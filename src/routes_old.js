import React from "react";

const ComingSoon = React.lazy(() => import("./views/pages/ComingSoon"));
const Search = React.lazy(() => import("./views/pages/Search"));
const EventList = React.lazy(() => import("./views/event/EventList"));
const EventNew = React.lazy(() => import("./views/event/EventNew"));
const EventEdit = React.lazy(() => import("./views/event/EventEdit"));
const EventDetail = React.lazy(() => import("./views/event/EventDetail"));
const Page404 = React.lazy(() => import("./views/pages/page404/Page404"));

const Home = React.lazy(() => import("./views/home/Home"));
const Profile = React.lazy(() => import("./views/profile/Profile"));
const User = React.lazy(() => import("./views/user/UserList"));
const EditRole = React.lazy(() => import("./views/user/EditRole"));
const NewRole = React.lazy(() => import("./views/user/NewRole"));
const EditProfile = React.lazy(() => import("./views/profile/EditProfile"));

const Submission = React.lazy(() => import("./views/submission/Submission"));
const SubmissionNew = React.lazy(() =>
  import("./views/submission/SubmissionEdit")
);
const SubmissionPreview = React.lazy(() =>
  import("./views/submission/SubmissionPreview")
);
const SubmissionProgress = React.lazy(() =>
  import("./views/submission/Progres")
);
const SubmissionReport = React.lazy(() => import("./views/submission/Laporan"));
const ReportPreview = React.lazy(() =>
  import("./views/submission/LaporanPreview")
);
const SubmissionList = React.lazy(() =>
  import("./views/submission/SubmissionList")
);
const SubmissionApproval = React.lazy(() =>
  import("./views/submission/SubmissionApproval")
);

const AppointmentBook = React.lazy(() =>
  import("./views/appointment/AppointmentBook")
);
const AppointmentHistory = React.lazy(() =>
  import("./views/appointment/AppointmentHistory")
);
const AppointmentApproval = React.lazy(() =>
  import("./views/appointment/AppointmentApproval")
);

const AnnouncementEdit = React.lazy(() =>
  import("./views/announcement/AnnouncementEdit")
);

const AnnouncementList = React.lazy(() =>
  import("./views/announcement/AnnouncementList")
);

const AnnouncementDetail = React.lazy(() =>
  import("./views/announcement/AnnouncementDetail")
);

const InnovationList = React.lazy(() =>
  import("./views/innovation/InnovationList")
);

const MyInnovation = React.lazy(() =>
  import("./views/innovation/MyInnovation")
);

const InnovationDetail = React.lazy(() =>
  import("./views/innovation/InnovationDetail")
);

const InnovationDetailBudget = React.lazy(() =>
  import("./views/innovation/InnovationDetailBudget")
);

const InnovationDetailOKR = React.lazy(() =>
  import("./views/innovation/InnovationDetailOKR")
);

const InnovationDetailRevenue = React.lazy(() =>
  import("./views/innovation/InnovationDetailRevenue")
);

//Innovator
const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));

// //ADMIN==========
// const DashboardAdmin = React.lazy(() => import("./views/admin/Dashboard"));

// //AMA============
// const DashboardAMA = React.lazy(() => import("./views/ama/Dashboard"));

const routes = [
  { path: "/404", name: "404", element: Page404 },

  //1. Sebagai user saya dapat melakukan registrasi, sign-in, dan mengakses home page pada platform MOKAP

  //1.2 & 1.6 DONE
  { path: "/profile/edit", name: "Edit Profile", element: EditProfile },

  //1.3
  { path: "/", name: "Home", element: Home },

  //1.4 DONE
  { path: "/search/:search", name: "Search", element: Search },

  //1.5 DONE
  { path: "/profile", name: "Profile", element: Profile },

  //2. Sebagai user saya dapat mengakses event pada platform MOKAP
  { path: "/event", name: "Event" },

  //2.1
  // { path: "/event/new", name: "New Event", element: EventNew },

  //2.2
  { path: "/event/list", name: "Event List", element: EventList },

  //2.3
  { path: "/event/detail/:id", name: "Event Detail", element: EventDetail },

  //3. Sebagai user saya dapat mengakses inovasi, proposal, progress, dan laporan

  {
    path: "/event/submission",
    name: "Submission",
  },

  //3.1, 3.2
  {
    path: "/event/submission/:page/:id_event",
    name: "Submission",
    element: Submission,
  },

  {
    path: "/event/submission/:page/:id_event/:id_phase",
    name: "Submission",
    element: SubmissionNew,
  },

  {
    path: "/event/submission/:page/:id_event/:id_phase/:id_innovation",
    name: "Submission",
    element: Submission,
  },

  // //3.2
  // {
  //   path: "/event/submission/preview/:id",
  //   name: "Submission Preview",
  //   element: SubmissionPreview,
  // },

  //3.3
  {
    path: "/event/submission/progress/:id",
    name: "Submission Progress",
    element: SubmissionProgress,
  },

  //3.4
  {
    path: "/event/submission/report/:id",
    name: "Submission Report",
    element: SubmissionReport,
  },
  {
    path: "/event/submission/report/:id/:id_phase",
    name: "Submission Report",
    element: SubmissionReport,
  },

  //3.5
  {
    path: "/event/submission/report/preview/:id",
    name: "Submission Report Preview",
    element: ReportPreview,
  },

  // {
  //   path: "/event/submission/list",
  //   name: "Submission List",
  //   element: ComingSoon,
  // },
  // {
  //   path: "/event/appointmen/approval",
  //   name: "Appointment Approval",
  //   element: ComingSoon,
  // },

  //5. Sebagai user saya dapat mengakses schedule management fitur pada platform MOKAP

  //5.1
  {
    path: "/event/appointment",
    name: "Appointment",
    element: AppointmentBook,
  },
  {
    path: "/event/appointment/history",
    name: "History",
    element: AppointmentHistory,
  },
  //6. Sebagai user saya dapat mengakses dashboard reporting

  //6.1
  { path: "/dashboard", name: "Dashboard", element: Dashboard },

  //7. Sebagai user saya dapat mengakses notifikasi penting pada platform MOKAP

  //INNOVATION
  { path: "/innovation", name: "Innovation" },
  {
    path: "/innovation/list",
    name: "Innovation List",
    element: InnovationList,
  },
  {
    path: "/innovation/myinnovation",
    name: "My Innovation",
    element: MyInnovation,
  },
  {
    path: "/innovation/detail/:id",
    name: "Innovation Detail",
    element: InnovationDetail,
  },
  {
    path: "/innovation/detail/budget/:id",
    name: "Innovation Detail Budget",
    element: InnovationDetailBudget,
  },

  {
    path: "/innovation/detail/okr/:id",
    name: "Innovation Detail OKR",
    element: InnovationDetailOKR,
  },

  {
    path: "/innovation/detail/revenue/:id",
    name: "Innovation Detail Revenue",
    element: InnovationDetailRevenue,
  },
  //ANNOUNCEMENT
  { path: "/announcement", name: "Announcement" },
  {
    path: "/announcement/list",
    name: "Announcement List",
    element: AnnouncementList,
  },
  {
    path: "/announcement/detail/:id",
    name: "Announcement Detail",
    element: AnnouncementDetail,
  },
  {
    path: "/announcement/detail/:id/:slug",
    name: "Announcement Detail",
    element: AnnouncementDetail,
  },
  // {
  //   path: "/announcement/new",
  //   name: "Create Announcement",
  //   element: ComingSoon,
  // },
];

const routesAdmin = [
  { path: "/404", name: "404", element: Page404 },

  //UserList
  { path: "/user", name: "User List", element: User },
  { path: "/user/role/:id", name: "User Role", element: EditRole },
  // { path: "/user/role", name: "User Role", element: NewRole },

  //1. Sebagai user saya dapat melakukan registrasi, sign-in, dan mengakses home page pada platform MOKAP

  //1.2 & 1.6 DONE
  { path: "/profile/edit", name: "Edit Profile", element: EditProfile },

  //1.3
  { path: "/", name: "Home", element: Home },

  //1.4 DONE
  { path: "/search/:search", name: "Search", element: Search },

  //1.5 DONE
  { path: "/profile", name: "Profile", element: Profile },

  //2. Sebagai user saya dapat mengakses event pada platform MOKAP
  { path: "/event", name: "Event" },

  //2.1
  { path: "/event/new", name: "New Event", element: EventNew },

  //2.2
  { path: "/event/list", name: "Event List", element: EventList },

  //2.3
  { path: "/event/detail/:id", name: "Event Detail", element: EventDetail },

  { path: "/event/edit/:id", name: "Edit Event", element: EventEdit },

  //3. Sebagai user saya dapat mengakses inovasi, proposal, progress, dan laporan

  {
    path: "/event/submission",
    name: "Submission",
  },

  //3.1, 3.2
  {
    path: "/event/submission/:page/:id_event",
    name: "Submission",
    element: Submission,
  },

  {
    path: "/event/submission/:page/:id_event/:id_phase",
    name: "Submission",
    element: SubmissionNew,
  },

  {
    path: "/event/submission/:page/:id_event/:id_phase/:id_innovation",
    name: "Submission",
    element: Submission,
  },

  //3.2
  // {
  //   path: "/event/submission/preview/:id",
  //   name: "Submission Preview",
  //   element: SubmissionPreview,
  // },

  //3.3
  {
    path: "/event/submission/progress/:id",
    name: "Submission Progress",
    element: SubmissionProgress,
  },

  //3.4
  {
    path: "/event/submission/report/:id",
    name: "Submission Report",
    element: SubmissionReport,
  },
  {
    path: "/event/submission/report/:id/:id_phase",
    name: "Submission Report",
    element: SubmissionReport,
  },
  //3.5
  {
    path: "/event/submission/report/preview/:id",
    name: "Submission Report Preview",
    element: ReportPreview,
  },

  //4.1
  {
    path: "/event/submission/list",
    name: "Submission List",
    element: SubmissionList,
  },

  //4.2
  {
    path: "/event/submission/approval/:page/:id_event/:id_phase/:id_innovation",
    name: "Submission Approval",
    element: SubmissionApproval,
  },

  //5. Sebagai user saya dapat mengakses schedule management fitur pada platform MOKAP

  //5.1
  {
    path: "/event/appointment",
    name: "Appointment",
    element: AppointmentBook,
  },
  {
    path: "/event/appointment/history",
    name: "History",
    element: AppointmentHistory,
  },
  {
    path: "/event/appointment/approval",
    name: "Approval",
    element: AppointmentApproval,
  },

  //6. Sebagai user saya dapat mengakses dashboard reporting

  //6.1
  { path: "/dashboard", name: "Dashboard", element: Dashboard },

  //7. Sebagai user saya dapat mengakses notifikasi penting pada platform MOKAP

  //INNOVATION
  { path: "/innovation", name: "Innovation" },
  {
    path: "/innovation/list",
    name: "Innovation List",
    element: InnovationList,
  },
  {
    path: "/innovation/myinnovation",
    name: "My Innovation",
    element: MyInnovation,
  },
  {
    path: "/innovation/detail/:id",
    name: "Innovation Detail",
    element: InnovationDetail,
  },
  {
    path: "/innovation/detail/budget/:id",
    name: "Innovation Detail Budget",
    element: InnovationDetailBudget,
  },

  {
    path: "/innovation/detail/okr/:id",
    name: "Innovation Detail OKR",
    element: InnovationDetailOKR,
  },

  {
    path: "/innovation/detail/revenue/:id",
    name: "Innovation Detail Revenue",
    element: InnovationDetailRevenue,
  },

  //ANNOUNCEMENT
  { path: "/announcement", name: "Announcement" },
  {
    path: "/announcement/list",
    name: "Announcement List",
    element: AnnouncementList,
  },
  {
    path: "/announcement/detail/:id",
    name: "Announcement Detail",
    element: AnnouncementDetail,
  },
  {
    path: "/announcement/detail/:id/:slug",
    name: "Announcement Detail",
    element: AnnouncementDetail,
  },
  {
    path: "/announcement/new",
    name: "Create Announcement",
    element: AnnouncementEdit,
  },
  {
    path: "/announcement/edit/:id",
    name: "Edit Announcement",
    element: AnnouncementEdit,
  },
];

const routesAMA = [
  { path: "/404", name: "404", element: Page404 },

  //1. Sebagai user saya dapat melakukan registrasi, sign-in, dan mengakses home page pada platform MOKAP

  //1.2 & 1.6 DONE
  { path: "/profile/edit", name: "Edit Profile", element: EditProfile },

  //1.3
  { path: "/", name: "Home", element: Home },

  //1.4 DONE
  { path: "/search/:search", name: "Search", element: Search },

  //1.5 DONE
  { path: "/profile", name: "Profile", element: Profile },

  //2. Sebagai user saya dapat mengakses event pada platform MOKAP
  { path: "/event", name: "Event" },

  //2.1
  // { path: "/event/new", name: "New Event", element: EventNew },

  //2.2
  { path: "/event/list", name: "Event List", element: EventList },

  //2.3
  { path: "/event/detail/:id", name: "Event Detail", element: EventDetail },

  // { path: "/event/edit/:id", name: "Edit Event", element: EventEdit },
  //3. Sebagai user saya dapat mengakses inovasi, proposal, progress, dan laporan

  {
    path: "/event/submission",
    name: "Submission",
  },

  //3.1, 3.2
  {
    path: "/event/submission/:page/:id_event",
    name: "Submission",
    element: Submission,
  },

  {
    path: "/event/submission/:page/:id_event/:id_phase",
    name: "Submission",
    element: SubmissionNew,
  },

  {
    path: "/event/submission/:page/:id_event/:id_phase/:id_innovation",
    name: "Submission",
    element: Submission,
  },

  // //3.2
  // {
  //   path: "/event/submission/preview/:id",
  //   name: "Submission Preview",
  //   element: SubmissionPreview,
  // },

  //3.3
  {
    path: "/event/submission/progress/:id",
    name: "Submission Progress",
    element: SubmissionProgress,
  },

  //3.4
  {
    path: "/event/submission/report/:id",
    name: "Submission Report",
    element: SubmissionReport,
  },
  {
    path: "/event/submission/report/:id/:id_phase",
    name: "Submission Report",
    element: SubmissionReport,
  },

  //3.5
  {
    path: "/event/submission/report/preview/:id",
    name: "Submission Report Preview",
    element: ReportPreview,
  },

  //4.1
  {
    path: "/event/submission/list",
    name: "Submission List",
    element: SubmissionList,
  },
  //4.2
  {
    path: "/event/submission/approval/:id/:id_phase",
    name: "Submission Approval",
    element: SubmissionApproval,
  },

  //5. Sebagai user saya dapat mengakses schedule management fitur pada platform MOKAP

  //5.1
  //5.1
  {
    path: "/event/appointment",
    name: "Appointment",
    element: AppointmentBook,
  },
  {
    path: "/event/appointment/history",
    name: "History",
    element: AppointmentHistory,
  },
  {
    path: "/event/appointment/approval",
    name: "Approval",
    element: AppointmentApproval,
  },
  //6. Sebagai user saya dapat mengakses dashboard reporting

  //6.1
  { path: "/dashboard", name: "Dashboard", element: Dashboard },

  //7. Sebagai user saya dapat mengakses notifikasi penting pada platform MOKAP

  //INNOVATION
  { path: "/innovation", name: "Innovation" },
  {
    path: "/innovation/list",
    name: "Innovation List",
    element: InnovationList,
  },
  {
    path: "/innovation/myinnovation",
    name: "My Innovation",
    element: MyInnovation,
  },
  {
    path: "/innovation/detail/:id",
    name: "Innovation Detail",
    element: InnovationDetail,
  },

  {
    path: "/innovation/detail/budget/:id",
    name: "Innovation Detail Budget",
    element: InnovationDetailBudget,
  },

  {
    path: "/innovation/detail/okr/:id",
    name: "Innovation Detail OKR",
    element: InnovationDetailOKR,
  },

  {
    path: "/innovation/detail/revenue/:id",
    name: "Innovation Detail Revenue",
    element: InnovationDetailRevenue,
  },

  //ANNOUNCEMENT
  { path: "/announcement", name: "Announcement" },
  {
    path: "/announcement/list",
    name: "Announcement List",
    element: AnnouncementList,
  },
  {
    path: "/announcement/detail/:id",
    name: "Announcement Detail",
    element: AnnouncementDetail,
  },
  {
    path: "/announcement/detail/:id/:slug",
    name: "Announcement Detail",
    element: AnnouncementDetail,
  },
  // {
  //   path: "/announcement/new",
  //   name: "Create Announcement",
  //   element: AnnouncementEdit,
  // },
  // {
  //   path: "/announcement/edit/:id",
  //   name: "Edit Announcement",
  //   element: AnnouncementEdit,
  // },
];

export { routes, routesAdmin, routesAMA };
