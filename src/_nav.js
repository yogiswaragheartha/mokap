import React, { useEffect, useState } from "react";
import CIcon from "@coreui/icons-react";
import {
  cilCursor,
  cilVoiceOverRecord,
  cilCalendarCheck,
  cilSpeedometer,
  cilStar,
} from "@coreui/icons";
import { CNavGroup, CNavItem, CNavTitle } from "@coreui/react";

const id = "";

const navigation = [
  {
    component: CNavItem,
    name: "Home",
    to: "/",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
    access: ["all"],
  },
  {
    component: CNavGroup,
    name: "Event",
    to: "/event",
    icon: <CIcon icon={cilCursor} customClassName="nav-icon" />,
    access: ["all"],
    items: [
      {
        component: CNavItem,
        name: "Event List",
        to: "/event/list",
        access: "all",
      },
      {
        component: CNavItem,
        name: "Create New Event",
        to: "/event/new",
        access: "createEvent",
      },
      {
        component: CNavItem,
        name: "Submission",
        to: "/event/submission/proposal",
        access: "submission",
      },
      {
        component: CNavItem,
        name: "List of Submission",
        to: "/event/submission/list",
        access: "listOfSubmission",
      },
      {
        component: CNavItem,
        name: "Appointment Approval",
        to: "/event/appointment/approval",
        access: "appointmentApproval",
      },
      {
        component: CNavItem,
        name: "Appointment",
        to: "/event/appointment",
        access: "appointment",
      },
    ],
  },
  {
    component: CNavGroup,
    name: "Innovation",
    to: "/innovation",
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
    access: ["innovationList", "myInnovation"],
    items: [
      {
        component: CNavItem,
        name: "Innovation List",
        to: "/innovation/list",
        access: "innovationList",
      },
      {
        component: CNavItem,
        name: "My Innovation",
        to: "/innovation/myinnovation",
        access: "myInnovation",
      },
    ],
  },

  {
    component: CNavGroup,
    name: "Announcement",
    to: "/announcement",
    icon: <CIcon icon={cilVoiceOverRecord} customClassName="nav-icon" />,
    access: ["createAnnouncement", "announcement"],
    items: [
      {
        component: CNavItem,
        name: "Create Announcement",
        to: "/announcement/new",
        access: "createAnnouncement",
      },
      {
        component: CNavItem,
        name: "Announcement",
        to: "/announcement/list",
        access: "announcement",
      },
    ],
  },

  {
    component: CNavItem,
    name: "Dashboard",
    to: "/dashboard",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
    access: ["all"],
  },
];

export { navigation };
