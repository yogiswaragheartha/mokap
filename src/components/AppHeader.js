import React, { useEffect, useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  CContainer,
  CHeader,
  CHeaderBrand,
  CHeaderDivider,
  CHeaderNav,
  CHeaderToggler,
  CNavLink,
  CNavItem,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CButton,
  CDropdown,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CModalFooter,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilBell, cilMagnifyingGlass, cilMenu } from "@coreui/icons";

import { AppBreadcrumb } from "./index";
import { AppHeaderDropdown } from "./header/index";
// import { navigation } from "../_nav";
import logo from "src/assets/images/logo_white.png";
import Notifikasi from "./Notifikasi";
import { getMyInnovation } from "src/api/submission";
import { getNotification } from "src/api/dashboard";
import ModalAlert from "./ModalAlert";

const AppHeader = (props) => {
  const dispatch = useDispatch();
  const sidebarShow = useSelector((state) => state.sidebar);
  const token = localStorage.getItem("token_jwt");
  const navigate = useNavigate();
  const [search, setSearch] = useState("");
  const [visible, setVisible] = useState(false);
  const [visibleNotif, setVisibleNotif] = useState(false);
  const [linkNavigasi, setLinkNavigasi] = useState(props.navigation);
  const [submission, setSubmission] = useState({});
  const [notifikasi, setNotifikasi] = useState([]);
  const [allNotifikasi, setAllNotifikasi] = useState([]);
  const [modal, setModal] = useState(false);

  const handleInputSearch = (event) => {
    setSearch(event.target.value);
    console.log(event.target.value);
    var input = document.getElementById("search");
    input.addEventListener("keypress", function (event) {
      if (event.key === "Enter") {
        // handleSearch();
        navigate("/search/" + event.target.value);
        setVisible(false);
      }
    });
  };

  const handleSearch = () => {
    navigate("/search/" + search);
    setVisible(false);
  };

  const get_MyInnovation = async (token) => {
    try {
      const response = await getMyInnovation(token);

      if (response.data.userInnovation) {
        setSubmission({
          id_event: response.data.userInnovation.eventId,
          id_phase: response.data.userInnovation.phaseId,
          id_innovation: response.data.userInnovation.id,
          status: response.data.status,
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  const get_notification = async (token) => {
    try {
      const response = await getNotification(token);
      // console.log("notif", response.data);

      const notif = response.data
        .filter(
          (c) =>
            c.receivedByUserId == null || c.receivedByUserId == props.userId
        )
        .sort((a, b) => (a.id > b.id ? -1 : 1));
      setNotifikasi(notif.slice(0, 5));
      setAllNotifikasi(notif);
      // console.log("notif", notif);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    get_MyInnovation(token);
    setLinkNavigasi(props.navigation);
  }, []);

  useEffect(() => {
    get_notification(token);
  }, [props.userId]);

  useEffect(() => {
    if (props.role == 1) {
      //admin
      //mengubah nilai array dari nav Submission (sesuai event id yg diperoleh)

      // props.navigation[1].items[2].to =
      //   "/event/submission/proposal/" +
      //   submission.id_event +
      //   "/" +
      //   submission.id_phase +
      //   "/" +
      //   submission.id_innovation;

      setLinkNavigasi(props.navigation);
    } else if (props.role == 2) {
      //ama
      // props.navigation[1].items[1].to =
      //   "/event/submission/proposal/" +
      //   submission.id_event +
      //   "/" +
      //   submission.id_phase +
      //   "/" +
      //   submission.id_innovation;
      setLinkNavigasi(props.navigation);
    } else {
      // props.navigation[1].items[1].to =
      //   "/event/submission/proposal/" +
      //   submission.id_event +
      //   "/" +
      //   submission.id_phase +
      //   "/" +
      //   submission.id_innovation;
      setLinkNavigasi(props.navigation);
    }
    // console.log(props.navigation);
  }, [props.role, submission, props.navigation]);

  return (
    <>
      <ModalAlert
        visible={modal}
        status={"warning"}
        message={"Join event first"}
        callBack={() => setModal(false)}
      />
      <CHeader position="sticky" className="mb-4 bg-primary">
        <CContainer>
          <CHeaderToggler
            className="ps-1 d-lg-none"
            onClick={() =>
              dispatch({ type: "set", sidebarShow: !sidebarShow.sidebarShow })
            }
          >
            <CIcon icon={cilMenu} size="lg" />
          </CHeaderToggler>
          <CNavLink to="/" className="d-none d-md-block" component={NavLink}>
            <img src={logo} />
          </CNavLink>
          <CHeaderBrand className="mx-auto d-md-none text-white" to="/">
            <CNavLink to="/" component={NavLink}>
              <img src={logo} />
            </CNavLink>
          </CHeaderBrand>
          <CHeaderNav className="d-none d-lg-flex me-auto">
            {linkNavigasi.map(function (item) {
              if (item.items == null) {
                return (
                  <CNavItem className="header-link">
                    <CNavLink to={item.to} component={NavLink}>
                      {item.name}
                    </CNavLink>
                  </CNavItem>
                );
              } else {
                //untuk dropdown
                return (
                  <CDropdown variant="nav-item">
                    <CDropdownToggle color="secondary" className="header-link">
                      {item.name}
                    </CDropdownToggle>
                    <CDropdownMenu>
                      {item.items.map(function (subItem) {
                        return (
                          <CNavLink to={subItem.to} component={NavLink}>
                            <CDropdownItem>{subItem.name}</CDropdownItem>
                          </CNavLink>
                        );
                      })}
                    </CDropdownMenu>
                  </CDropdown>
                );
              }
            })}
          </CHeaderNav>
          <CHeaderNav className="d-none d-md-block">
            <CInputGroup className="input-prepend ">
              <CFormInput
                type="text"
                placeholder="Search"
                id="search"
                onChange={handleInputSearch}
              />

              <CInputGroupText type="button" onClick={handleSearch}>
                <CIcon icon={cilMagnifyingGlass} />
              </CInputGroupText>
            </CInputGroup>
          </CHeaderNav>
          <CButton
            type="button"
            class="btn btn-primary d-md-none"
            onClick={() => setVisible(!visible)}
          >
            <CIcon icon={cilMagnifyingGlass} size="lg" />
          </CButton>
          <CModal visible={visible} onClose={() => setVisible(false)}>
            <CModalHeader>
              <CModalTitle>Search</CModalTitle>
            </CModalHeader>
            <div variant="nav-item">
              <CModalBody>
                <>
                  <>
                    <CInputGroup className="input-prepend ">
                      <CFormInput
                        type="text"
                        placeholder="Search"
                        id="search"
                        onChange={handleInputSearch}
                      />

                      <CInputGroupText type="button" onClick={handleSearch}>
                        <CIcon icon={cilMagnifyingGlass} />
                      </CInputGroupText>
                    </CInputGroup>
                  </>
                </>
              </CModalBody>
            </div>
          </CModal>
          <CHeaderNav>
            <CDropdown variant="">
              <CDropdownToggle
                caret={false}
                className="btn btn-primary position-relative"
                onMouseUp={() =>
                  (document.getElementById("jlh-notif").style.visibility =
                    "hidden")
                }
              >
                <CIcon icon={cilBell} size="xl" />
                <span
                  id="jlh-notif"
                  class="position-absolute top-0 -ms-2 translate-middle badge rounded-pill bg-danger"
                >
                  {allNotifikasi.length}
                  <span class="visually-hidden">unread messages</span>
                </span>
              </CDropdownToggle>
              <CDropdownMenu className="">
                <Notifikasi
                  data={notifikasi}
                  callBack={(e) => setVisibleNotif(e)}
                />
              </CDropdownMenu>
              <CModal
                visible={visibleNotif}
                onClose={() => setVisibleNotif(false)}
                size="sm"
              >
                <Notifikasi data={allNotifikasi} all={true} />
                <button
                  className="btn bg-primary text-white"
                  onClick={() => setVisibleNotif(false)}
                >
                  Close
                </button>
              </CModal>
            </CDropdown>
          </CHeaderNav>
          <CHeaderNav className="ms-3">
            <AppHeaderDropdown />
          </CHeaderNav>
        </CContainer>
        {/* <CHeaderDivider /> */}
      </CHeader>
      <CContainer>
        <AppBreadcrumb />
      </CContainer>
      <br />
    </>
  );
};

export default AppHeader;
