import { CSpinner } from "@coreui/react";
import React from "react";

const LoadingContent = (props) => {
  return (
    <>
      <CSpinner color="primary" /> Processing..
    </>
  );
};

export default LoadingContent;
