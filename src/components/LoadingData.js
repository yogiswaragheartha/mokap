import { CSpinner } from "@coreui/react";
import React from "react";
import logo from "src/assets/images/logo.png";

const LoadingData = (props) => {
  return (
    <div className="text-center">
      <img src={logo} className="mt-5 animasi-loadingData" />
    </div>
  );
};

export default LoadingData;
