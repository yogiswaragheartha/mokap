import React, { Suspense } from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { CContainer, CSpinner } from "@coreui/react";
// import { useSelector } from "react-redux";

const AppContent = (prop) => {
  // const { auth } = useSelector((state) => state);
  return (
    <CContainer fluid>
      <Suspense
        fallback={
          <>
            <CSpinner color="primary" />
            Loading..
          </>
        }
      >
        <Routes>
          {prop.routes.map((route, idx) => {
            // if (prop.role === "") {
            //   return (
            //     <>
            //       <Route
            //         path="/*"
            //         element={<Navigate to="profile/edit" replace />}
            //       />
            //       route.element && (
            //       <Route
            //         key={idx}
            //         path={route.path}
            //         exact={route.exact}
            //         name="Edit Profile"
            //         element={<route.element />}
            //       />
            //       )
            //     </>
            //   );
            // } else {
            return (
              route.element && (
                <Route
                  key={idx}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  element={<route.element />}
                />
              )
            );
            // }
          })}
          {/* <Route path="/" element={<Navigate to="dashboard" replace />} /> */}
        </Routes>
      </Suspense>
    </CContainer>
  );
};

export default React.memo(AppContent);
