import {
  CModal,
  CButton,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CModalTitle,
} from "@coreui/react";
import React, { useState, useEffect } from "react";
import Input from "./Input";
import { cilCheck, cilX, cilWarning } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import logo from "src/assets/images/logo_white_big.png";

const ModalAlert = (props) => {
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    setVisible(props.visible);
  }, [props.visible]);

  return (
    <>
      <CModal visible={visible} onClose={props.callBack}>
        {props.status == "loading" ? (
          <div
            className={`bg-primary d-flex justify-content-center py-4`}
            style={{ height: "220px" }}
          >
            <img src={logo} className="mt-5 animasi-loading" />
          </div>
        ) : (
          <div
            className={`bg-${props.status} d-flex justify-content-center py-4`}
            style={{ height: "220px" }}
          >
            <div
              className="border text-center text-light border-3 border-light rounded-circle"
              style={{ width: "150px", height: "150px" }}
            >
              <CIcon
                icon={
                  props.status == "success"
                    ? cilCheck
                    : props.status == "danger"
                    ? cilX
                    : cilWarning
                }
                width={100}
                className="mt-4"
              />
              <h4 className="mt-4">
                {props.status == "success"
                  ? "SUCCESS!"
                  : props.status == "danger"
                  ? "ERROR!"
                  : ""}
              </h4>
            </div>
          </div>
        )}

        <CModalBody className="text-center px-4">{props.message}</CModalBody>

        {props.input != true ? (
          ""
        ) : (
          <div className="mx-5">
            <Input
              id="comment"
              label="Comment"
              type="text-area"
              placeholder="Please comment here"
              value=""
              required
            />
          </div>
        )}

        <div className="text-center mb-4"></div>
      </CModal>
    </>
  );
};

export default ModalAlert;
