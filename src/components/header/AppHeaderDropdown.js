import React from "react";
import {
  CAvatar,
  CBadge,
  CNavLink,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from "@coreui/react";

import { cilLockLocked, cilSettings, cilUser, cilPeople } from "@coreui/icons";
import CIcon from "@coreui/icons-react";
import { useNavigate, Link } from "react-router-dom";
import { signOut } from "../../state-redux/actionCreators";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";

const AppHeaderDropdown = () => {
  const navigate = useNavigate();

  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const AC = bindActionCreators({ signOut }, dispatch);

  const logout = () => {
    localStorage.clear();

    AC.signOut();

    navigate("/login");
  };

  return (
    <CDropdown variant="nav-item">
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        {/* <CAvatar src={avatar8} size="md" /> */}
        <CAvatar color="secondary" size="md">
          {!auth.foto ? (
            ""
          ) : (
            <img
              class="avatar-img"
              src={auth.foto}
              style={{ objectFit: "cover", height: "45px", width: "45px" }}
            ></img>
          )}
        </CAvatar>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <Link to="/profile" className="text-decoration-none">
          <CDropdownItem>
            <CIcon icon={cilUser} className="me-2" />
            Profile
          </CDropdownItem>
        </Link>
        {auth.access.includes("userList") ||
        auth.access.includes("userRole") ? (
          <Link to="/user" className="text-decoration-none">
            <CDropdownItem>
              <CIcon icon={cilPeople} className="me-2" />
              User Management
            </CDropdownItem>
          </Link>
        ) : (
          ""
        )}

        {/* <CDropdownItem href="#">
          <CIcon icon={cilSettings} className="me-2" />
          Settings
        </CDropdownItem> */}

        <CDropdownItem href="" onClick={logout}>
          <CIcon icon={cilLockLocked} className="me-2" />
          Logout
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  );
};

export default AppHeaderDropdown;
