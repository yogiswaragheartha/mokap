import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import {
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarToggler,
} from "@coreui/react";

import { AppSidebarNav } from "./AppSidebarNav";

import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";

// sidebar nav config
// import { navigation } from "../_nav";

const AppSidebar = (props) => {
  const dispatch = useDispatch();
  const unfoldable = useSelector((state) => state.sidebarUnfoldable);
  const sidebarShow = useSelector((state) => state.sidebar);
  const [linkNavigasi, setLinkNavigasi] = useState([]);

  useEffect(() => {
    // console.log("props role nav", props.role);
    if (props.role == 1) {
      //admin
      setLinkNavigasi(props.navigation);
    } else if (props.role == 2) {
      //ama
      setLinkNavigasi(props.navigation);
    } else {
      setLinkNavigasi(props.navigation);
    }
  }, [props.role, props.navigation]);

  return (
    <CSidebar
      position="fixed"
      unfoldable={unfoldable}
      visible={sidebarShow.sidebarShow}
      onVisibleChange={(visible) => {
        dispatch({ type: "set", sidebarShow: visible });
        // console.log("Visible", sidebarShow);
      }}
      className="bg-primary"
    >
      <CSidebarBrand className="d-none d-md-flex" to="/">
        <h3>Mokap</h3>
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <AppSidebarNav items={linkNavigasi} />
        </SimpleBar>
      </CSidebarNav>
      {/* <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() => dispatch({ type: 'set', sidebarUnfoldable: !unfoldable })}
      /> */}
    </CSidebar>
  );
};

export default React.memo(AppSidebar);
