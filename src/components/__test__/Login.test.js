import React from "react";
import { render, screen } from "@testing-library/react";
import Login from "src/views/pages/login/Login";


test("render Login", () => {
  render(<Login />);
});

test("render logo Mokap", () => {
  render(<Login />);
  const logo = screen.getByRole('img');
  expect(logo).toHaveAttribute('src', 'logo.png')
});


test('render welcome message', () => {
  render(<Login />);
  const linkElement = screen.getByText(/More than just an innovation management tool/i)
  expect(linkElement).toBeInTheDocument();    
});

test("render sign in button", () => {
  render(<Login />);
  const signInButton = screen.getByRole('button');
  expect(signInButton).toHaveAttribute('id', 'signButton')
  expect(screen.getByText('Sign in with Amoeba Git')).toBeInTheDocument();    
});