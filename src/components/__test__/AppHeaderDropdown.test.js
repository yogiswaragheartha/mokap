import React from "react";
import { render } from "@testing-library/react";
import { CAvatar, CDropdown } from "@coreui/react";

test("render Avatar Header Dropdown", () => {
  render(<CAvatar />);
});

test("render dropdown", () => {
  render(<CDropdown variant="nav-item"></CDropdown>);
});
