import React from "react";
import { render } from "@testing-library/react";
import CIcon from "@coreui/icons-react";

test("render Icon", () => {
  render(<CIcon icon="cilMagnifyingGlass" />);
});

test("render error message", () => {
    render(<p className="text-medium-emphasis float-start">
    The page you are looking for was not found.
  </p>); 
});