import { CCol, CRow } from "@coreui/react";
import React, { useState, useEffect } from "react";

const Input = (props) => {
  const [value, setValueInput] = useState("");
  const [isShowPass, setIsShowPass] = useState(false);

  const handleChangeValue = (event) => {
    setValueInput(event.target.value);

    // console.log(props.id + " - " + event.target.value);
    if (props.callBack) {
      props.callBack(event);
    }
  };

  // const handleChangeCheckbox = (e) => {
  //   // let isChecked = e.target.checked;
  //   // setValueInput(isChecked);
  //   // if (e.target.checked) {
  //   setValueInput(e.target.value);
  //   // } else {
  //   //   setValueInput();
  //   // }
  //   console.log(props.id + " - " + value);
  //   if (props.callBack) {
  //     props.callBack(isChecked);
  //   }
  // };

  if (props.required) {
    var required = "*";
  } else {
    var required = "";
  }

  useEffect(() => {
    setValueInput(props.value);
  }, [props.value]);

  if (props.type == "text-area") {
    return (
      <div class="mb-3" style={props.style}>
        <label for={props.id} class="form-label">
          {props.label} <code>{required}</code>
        </label>
        <textarea
          class={`form-control ${
            props.required && !value ? "border-danger" : ""
          }`}
          id={props.id}
          rows="3"
          placeholder={props.placeholder}
          onChange={handleChangeValue}
          value={value}
          disabled={props.disabled ? true : false}
        ></textarea>
      </div>
    );
  } else if (props.type == "select") {
    // console.log("value", value);
    return (
      <div class="mb-3" style={props.style}>
        {props.nolabel ? (
          ""
        ) : (
          <label for={props.id} className="form-label">
            {props.label}
            <code>{required}</code>
          </label>
        )}
        <select
          size="sm"
          id={props.id}
          className={`form-select ${
            props.required && !value ? "border-danger" : ""
          }`}
          aria-label="Small select example"
          value={value}
          onChange={handleChangeValue}
          onClick={props.onClick}
          disabled={props.disabled ? true : false}
          style={props.style}
        >
          <option value="" disabled>
            {props.placeholder}
          </option>
          {props.opsi.map(function (item) {
            return <option value={item.value}>{item.text}</option>;
          })}
        </select>
      </div>
    );
  } else if (props.type == "range") {
    return (
      <CRow>
        <CCol>
          <input
            type="range"
            className="form-range"
            id={props.id}
            value={value}
            onChange={handleChangeValue}
            disabled={props.disabled ? true : false}
          />
        </CCol>
        <CCol className="text-mini text-primary" lg={3}>
          {value}%
        </CCol>
      </CRow>
    );
  } else if (props.type == "checkbox") {
    const [isChecked, setIsChecked] = useState(props.checked);
    return (
      <div className="form-check " style={{ marginLeft: "-23px" }}>
        <input
          className="clickable"
          type="checkbox"
          id={props.id}
          value={value}
          onChange={(e) => {
            handleChangeValue(e);
            setIsChecked(!isChecked);
          }}
          disabled={props.disabled ? true : false}
          checked={isChecked}
        />{" "}
        <label className="form-check-label clickable" for={props.id}>
          {props.label}
        </label>
      </div>
    );
  } else if (props.type == "file") {
    return (
      <div class={props.nolabel ? "" : "mb-3"} style={props.style}>
        {props.nolabel ? (
          ""
        ) : (
          <label for={props.id} className="form-label">
            {props.label}
            <code>{required}</code>
          </label>
        )}

        <input
          type={props.type}
          className={`form-control ${
            props.required && !value ? "border-danger" : ""
          }`}
          id={props.id}
          name={props.id}
          placeholder={props.placeholder}
          onChange={handleChangeValue}
          disabled={props.disabled ? true : false}
        />
        <input type="hidden" id={"cek-" + props.id} value={value} />
        <p className="text-mini text-muted">
          {value ? (
            <font
              className="btn btn-sm text-muted"
              onClick={() => window.open(value)}
            >
              {value.substring(0, 36) + (value.length > 36 ? ".." : "")}
            </font>
          ) : (
            ""
          )}
        </p>
        <input type="text" id={props.id + "input"} value={value} hidden />
      </div>
    );
  } else if (props.type == "password") {
    return (
      <div class={props.nolabel ? "" : "mb-3"} style={props.style}>
        {props.nolabel ? (
          ""
        ) : (
          <label for={props.id} className="form-label">
            {props.label}
            <code>{required}</code>
          </label>
        )}

        <div class="input-group mb-3">
          <input
            type={isShowPass ? "text" : "password"}
            className={`form-control ${
              props.required && !value ? "border-danger" : ""
            }`}
            id={props.id}
            name={props.id}
            placeholder={props.placeholder}
            onChange={handleChangeValue}
            value={value}
            disabled={props.disabled ? true : false}
            style={props.style}
          />
          <span
            className="clickable input-group-text bg-light border-0 "
            style={{ borderRadius: "0px 10px 10px 0px" }}
            onClick={() => setIsShowPass(!isShowPass)}
          >
            <i
              className={`${isShowPass ? "bi-eye-fill" : "bi-eye-slash-fill"}`}
            ></i>
          </span>
        </div>
      </div>
    );
  } else {
    return (
      <div class={props.nolabel ? "" : "mb-3"} style={props.style}>
        {props.nolabel ? (
          ""
        ) : (
          <label for={props.id} className="form-label">
            {props.label}
            <code>{required}</code>
          </label>
        )}

        <input
          type={props.type}
          className={`form-control ${
            props.required && !value ? "border-danger" : ""
          }`}
          id={props.id}
          name={props.id}
          placeholder={props.placeholder}
          onChange={handleChangeValue}
          value={value}
          disabled={props.disabled ? true : false}
          style={props.style}
        />
      </div>
    );
  }
};

export default Input;
