import React, { Component, useState, useEffect } from "react";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

const InputRichText = (props) => {
  const [value, setValueInput] = useState(props.value);

  const handleChangeValue = (event) => {
    setValueInput(event.target.value);

    if (props.callBack) {
      props.callBack();
    }
  };

  if (props.required) {
    var required = "*";
  } else {
    var required = "";
  }

  useEffect(() => {
    setValueInput(props.value);
  }, [props.value]);

  return (
    <div className="mb-3 App" style={props.style}>
      <label for={props.id} class="form-label">
        {props.label} <code>{required}</code>
      </label>

      <div
        className={` ${props.required && !value ? "border border-danger" : ""}`}
      >
        <CKEditor
          editor={ClassicEditor}
          data={props.value}
          id={props.id}
          onChange={(event, editor) => {
            setValueInput(editor.getData());

            // console.log(value);
            // console.log({ event, editor, data });
            if (props.callBack) {
              props.callBack(editor.getData());
            }
          }}
          placeholder={props.placeholder}
          disabled={props.disabled ? true : false}
        />
      </div>
    </div>
  );
};

export default InputRichText;
