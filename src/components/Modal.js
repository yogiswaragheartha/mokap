import {
  CModal,
  CButton,
  CModalHeader,
  CModalBody,
  CModalFooter,
  CModalTitle,
} from "@coreui/react";
import React, { useState, useEffect } from "react";
import Input from "./Input";
import { cilCheck } from "@coreui/icons";
import CIcon from "@coreui/icons-react";

const Modal = (props) => {
  const [visible, setVisible] = useState(false);
  const [comment, setComment] = useState();

  const handleChangeComment = (e) => {
    setComment(e.target.value);
  };

  useEffect(() => {
    setVisible(props.visible);
  }, [props.visible]);
  return (
    <>
      <CModal visible={visible} onClose={props.close}>
        <div
          className="bg-warning d-flex justify-content-center py-4"
          style={{ height: "200px" }}
        >
          <div
            className="border text-center border-3 border-light rounded-circle"
            style={{ width: "150px" }}
          >
            <h1 className="text-white" style={{ fontSize: "120px" }}>
              !
            </h1>
          </div>
        </div>
        <CModalBody className="text-center px-4">{props.pesan}</CModalBody>

        {props.input != true ? (
          ""
        ) : (
          <div className="mx-5">
            <Input
              id="comment"
              label="Comment"
              type="text-area"
              placeholder="Please comment here"
              value=""
              callBack={(e) => handleChangeComment(e)}
              required
            />
          </div>
        )}

        <div className="text-center mb-4">
          <CButton
            color="success"
            className="text-white me-4"
            style={{ width: "100px" }}
            onClick={(e) => props.callBack(e, comment)}
          >
            Yes
          </CButton>
          <CButton
            color="danger"
            className="text-white"
            style={{ width: "100px" }}
            onClick={() => setVisible(false)}
          >
            No
          </CButton>
        </div>
      </CModal>
    </>
  );
};

export default Modal;
