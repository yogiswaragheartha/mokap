import React, { useEffect, useState } from "react";
import { Link, NavLink, useNavigate } from "react-router-dom";
import {
  CHeaderDivider,
  CDropdownItem,
  CDropdownMenu,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CRow,
  CCol,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {
  cilBell,
  cilMagnifyingGlass,
  cilMenu,
  cilClock,
  cilCheck,
  cilBullhorn,
  cilStar,
  cilCursor,
} from "@coreui/icons";
import Moment from "moment";

const Notifikasi = (props) => {
  return (
    <>
      <div
        style={{ maxWidth: "100%", height: "35px", margin: "-6px" }}
        className="pt-1 ms-0 mb-2 bg-prpl rounded"
      >
        <CRow>
          <CCol xs={1} className="ps-4 text-primary">
            <CIcon icon={cilBell} size="xs" />
          </CCol>

          <CCol>
            <div className="text-primary ms-2">Notifications</div>
          </CCol>
        </CRow>
      </div>

      <div className="ms-0" style={{ maxWidth: "300px" }} />
      {props.data.map(function (item) {
        let icon = "";
        let color = "primary";
        if (item.topic == "Announcement") {
          icon = cilBullhorn;
          color = "warning";
        } else if (item.topic == "Appointment") {
          icon = cilClock;
          color = "danger";
        } else if (item.topic == "Innovation") {
          icon = cilStar;
          color = "success";
        } else {
          icon = cilCursor;
        }
        return (
          <>
            {item.status.toLowerCase() == "active" ? (
              // <Link to={"#"} className="text-decoration-none">
              <div className="text-mini px-1">
                <table style={{ maxWidth: "300px" }}>
                  <tr>
                    <td xs={1} className="text-primary">
                      <div
                        className={`border border-${color} text-${color} text-center pt-1 mt-1 ms-1 rounded-circle`}
                        style={{
                          width: "27px",
                          height: "27px",
                        }}
                      >
                        <CIcon icon={icon} size="xs" style={{}} />
                      </div>
                    </td>

                    <td>
                      <div className="mx-2">
                        <div>{item.message}</div>

                        <font className="text-muted">
                          {Moment(item.updatedAt).format("DD MMM YYYY")} at{" "}
                          {Moment(item.updatedAt).format("HH:mm")}
                        </font>
                      </div>
                    </td>
                  </tr>
                </table>
              </div>
            ) : (
              // </Link>
              <CDropdownItem className="text-mini text-muted">
                <CRow>
                  <CCol xs={1} className="text-primary">
                    <div
                      className={`border text-muted text-center pt-1 mt-1 rounded-circle`}
                      style={{
                        width: "27px",
                        height: "27px",
                        marginLeft: "-5px",
                      }}
                    >
                      <CIcon icon={icon} size="xs" style={{}} />
                    </div>
                  </CCol>

                  <CCol className="ms-2">
                    {item.message}
                    <br />
                    <font className="text-muted">
                      {Moment(item.updatedAt).format("DD MMM YYYY")};{" "}
                      {Moment(item.updatedAt).format("hh:mm")}
                    </font>
                  </CCol>
                </CRow>
              </CDropdownItem>
            )}
            <CHeaderDivider style={{ maxWidth: "100%" }} className="ms-0 " />
          </>
        );
      })}

      {props.all == true ? (
        ""
      ) : (
        <CDropdownItem
          className="clickable text-center bg-prpl text-mini"
          style={{ height: "25px", marginTop: "-7px", marginBottom: "-7px" }}
          onClick={() => props.callBack(true)}
        >
          View all
        </CDropdownItem>
      )}
    </>
  );
};

export default Notifikasi;
