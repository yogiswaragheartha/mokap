import React, { useEffect, useState } from "react";
import {
  AppContent,
  AppSidebar,
  AppFooter,
  AppHeader,
} from "../components/index";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { signIn } from "../state-redux/actionCreators";
import { bindActionCreators } from "redux";
// routes config
import { routes, routesAdmin, routesAMA } from "../routes";
import { getUserMokap } from "src/api/user";

const DefaultLayout = () => {
  const navigate = useNavigate();
  const token_jwt = localStorage.getItem("token_jwt");
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const AC = bindActionCreators({ signIn }, dispatch);

  const [rute, setRute] = useState([]);

  const getUser = async (token) => {
    try {
      const response = await getUserMokap(token);
      console.log("getUser", response);
      if (response.data.result) {
        AC.signIn({
          username: response.data.result.username,
          foto: response.data.result.foto,
          role: response.data.result.role,
        });
      } else {
        AC.signIn({
          username: response.data.username,
          foto: "",
          role: "",
        });
      }

      console.log("getUserMokap", response.data);

      // if (response.data.is_registered == "true") {
      //   navigate("/");
      // } else {
      //   navigate("/profile/edit");
      // }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (!token_jwt) {
      navigate("/login");
      return;
    }
    AC.signIn({
      username: "loading..",
      foto: "loading..",
      role: "loading..",
    });
    getUser(token_jwt);
  }, []);

  useEffect(() => {
    if (!auth.role) {
      setRute([
        {
          path: "/profile/edit",
          name: "Edit Profile",
          element: React.lazy(() => import("src/views/profile/EditProfile")),
        },
      ]);
    } else {
      if (auth.role == "Admin") {
        setRute(routesAdmin);
      } else if (auth.role == "AMA") {
        setRute(routesAMA);
      } else {
        setRute(routes);
      }
    }
    console.log("auth:", auth);
  }, [auth]);

  return (
    <div>
      <AppSidebar role={auth.role} />

      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeader role={auth.role} />
        <div className="body flex-grow-1 px-3">
          <AppContent routes={rute} role={auth.role} />
        </div>
        <AppFooter />
      </div>
    </div>
  );
};

export default DefaultLayout;
