import React, { useEffect, useState } from "react";
import {
  AppContent,
  AppSidebar,
  AppFooter,
  AppHeader,
} from "../components/index";
import {
  useNavigate,
  useLocation,
  Route,
  Routes,
  Navigate,
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { signIn, signOut } from "../state-redux/actionCreators";
import { bindActionCreators } from "redux";
// routes config
import { routes } from "../routes";
import { navigation } from "../_nav";
import { getUserMokap, getRoleById } from "src/api/user";
import EditProfile from "src/views/profile/EditProfile";

const DefaultLayout = () => {
  const navigate = useNavigate();
  const token_jwt = localStorage.getItem("token_jwt");
  const location = useLocation();
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const AC = bindActionCreators({ signIn, signOut }, dispatch);

  const [rute, setRute] = useState([]);
  const [nav, setNav] = useState([]);
  const [roleAccess, setRoleAccess] = useState({});

  const logout = () => {
    localStorage.clear();

    AC.signOut();

    navigate("/login");
  };

  const getUser = async (token) => {
    try {
      const response = await getUserMokap(token);
      // console.log("getUser", response.data);
      if (response.data) {
        getRoleAccess(response.data.roleId, response.data);
      } else {
        AC.signIn({
          username: response.data.username,
          id: response.data.id,
          foto: "",
          role: "",
          access: "",
        });
      }

      // console.log("getUserMokap", response.data);

      if (response.data.registered == true) {
        //navigate("/");
      } else {
        navigate("/profile/edit");
      }
    } catch (error) {
      if (error.response.statusText == "Unauthorized") {
        // console.log(error.response.statusText);
        logout();
      }
    }
  };

  const getRoleAccess = async (id, data) => {
    try {
      const response = await getRoleById(token_jwt, id);

      // console.log("respon akses", response.data.access);

      const access = response.data.access
        ? JSON.parse(response.data.access)
        : {};
      const eventEvent = access.eventEvent || [];
      const eventDocument = access.eventDocument || [];
      const eventBooking = access.eventBooking || [];
      const dashboard = access.dashboard || [];
      const announcement = access.announcement || [];
      const innovation = access.innovation || [];
      const userManagement = access.userManagement || [];

      const allAccess = eventEvent.concat(
        eventDocument.concat(
          eventBooking.concat(
            dashboard.concat(
              announcement.concat(innovation.concat(userManagement))
            )
          )
        )
      );
      allAccess.push("all");
      setRoleAccess(allAccess);
      // console.log("allAccess", allAccess);

      setRute(routes.filter((c) => allAccess.includes(c.access)));

      const filteredNav = navigation.filter(
        (c) =>
          allAccess.includes(c.access[0]) || allAccess.includes(c.access[1])
      );

      setNav(filteredNav);
      // console.log("nav", id, filteredNav);

      if (data) {
        AC.signIn({
          username: data.username,
          id: data.id,
          foto: data.avatar,
          role: data.roleId,
          access: allAccess,
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  const filterSubNav = () => {
    if (nav) {
      // console.log("FilteredNav Before", nav);
      nav.map((item, i) => {
        if (item.items) {
          nav[i]["items"] = item.items.filter((c) =>
            roleAccess.includes(c.access)
          );
        }
      });
      // console.log("FilteredNav After", nav);
    }
  };

  useEffect(() => {
    setTimeout(() => {
      filterSubNav();
    }, 0);
  }, [roleAccess]);

  useEffect(() => {
    if (!token_jwt) {
      navigate("/login");
      return;
    }

    if (!auth.role && auth.isLogin == true) {
      setRute([
        {
          path: "/profile/edit",
          name: "Edit Profile",
          element: React.lazy(() => import("src/views/profile/EditProfile")),
        },
      ]);
    }

    if (location.data != undefined) {
      //baru daftar
      AC.signIn({
        username: location.state.data.username,
        id: location.state.data.id,
        foto: location.state.data.avatar,
        role: location.state.data.role,
      });
      console.log("state", location.state);
    } else {
      if (!auth.role) {
        console.log("Mount");
        getUser(token_jwt);
      }
    }
  }, []);

  useEffect(() => {
    if (auth.role) {
      console.log("auth role");
      getRoleAccess(auth.role);
    }
  }, [auth.role]);

  useEffect(() => {
    // if (!auth.role) {
    console.log("auth foto");
    getUser(token_jwt);
    // }
  }, [auth.foto]);

  return (
    <div>
      {window.innerWidth >= 992 ? (
        ""
      ) : (
        <AppSidebar role={auth.role} navigation={nav} />
      )}

      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeader role={auth.role} userId={auth.id} navigation={nav} />
        <div className="body flex-grow-1 px-0">
          {auth.role ? (
            <AppContent routes={rute} role={auth.role} />
          ) : (
            <Routes>
              {rute.map((route, idx) => {
                return (
                  <>
                    {/* <Route
                      path="/*"
                      element={<Navigate to="profile/edit" replace />}
                    /> */}
                    {/* route.element && (
                    <Route
                      key={idx}
                      path={route.path}
                      exact={route.exact}
                      name="Edit Profile"
                      element={<route.element />}
                    />
                    ) */}
                    <Route
                      key={idx}
                      path={route.path}
                      exact={route.exact}
                      name="Edit Profile"
                      element={<route.element />}
                    />
                  </>
                );
              })}
            </Routes>
          )}
        </div>
        <AppFooter />
      </div>
    </div>
  );
};

export default DefaultLayout;
