import React, { useEffect, useState } from "react";
import CIcon from "@coreui/icons-react";
import {
  cilCursor,
  cilVoiceOverRecord,
  cilCalendarCheck,
  cilSpeedometer,
  cilStar,
} from "@coreui/icons";
import { CNavGroup, CNavItem, CNavTitle } from "@coreui/react";

const id = "";

const navigation = [
  {
    component: CNavItem,
    name: "Home",
    to: "/",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
  {
    component: CNavGroup,
    name: "Event",
    to: "/event",
    icon: <CIcon icon={cilCursor} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Event List",
        to: "/event/list",
      },
      // {
      //   component: CNavItem,
      //   name: "Create New Event",
      //   to: "/event/new",
      // },
      {
        component: CNavItem,
        name: "Submission",
        to: "/event/submission/proposal/" + id,
      },
      // {
      //   component: CNavItem,
      //   name: "List of Submission",
      //   to: "/event/submission/list",
      // },
      // {
      //   component: CNavItem,
      //   name: "Appointment Approval",
      //   to: "/event/appointmen/approval",
      // },
      {
        component: CNavItem,
        name: "Appointment",
        to: "/event/appointment",
      },
    ],
  },
  {
    component: CNavGroup,
    name: "Innovation",
    to: "/innovation",
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
    items: [
      {
        component: CNavItem,
        name: "Innovation List",
        to: "/innovation/list",
      },
      {
        component: CNavItem,
        name: "My Innovation",
        to: "/innovation/myinnovation",
      },
    ],
  },

  {
    component: CNavGroup,
    name: "Announcement",
    to: "/announcement",
    icon: <CIcon icon={cilVoiceOverRecord} customClassName="nav-icon" />,
    items: [
      // {
      //   component: CNavItem,
      //   name: "Create Announcement",
      //   to: "/announcement/new",
      // },
      {
        component: CNavItem,
        name: "Announcement",
        to: "/announcement/list",
      },
    ],
  },

  {
    component: CNavItem,
    name: "Dashboard",
    to: "/dashboard",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
];

const navigationAdmin = [
  {
    component: CNavItem,
    name: "Home",
    to: "/",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
  {
    component: CNavGroup,
    name: "Event",
    to: "/event",
    icon: <CIcon icon={cilCursor} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Event List",
        to: "/event/list",
      },
      {
        component: CNavItem,
        name: "Create New Event",
        to: "/event/new",
      },
      {
        component: CNavItem,
        name: "Submission",
        to: "/event/submission/proposal/" + id,
      },
      {
        component: CNavItem,
        name: "List of Submission",
        to: "/event/submission/list",
      },
      {
        component: CNavItem,
        name: "Appointment Approval",
        to: "/event/appointment/approval",
      },
      {
        component: CNavItem,
        name: "Appointment",
        to: "/event/appointment",
      },
    ],
  },
  {
    component: CNavGroup,
    name: "Innovation",
    to: "/innovation",
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
    items: [
      {
        component: CNavItem,
        name: "Innovation List",
        to: "/innovation/list",
      },
      {
        component: CNavItem,
        name: "My Innovation",
        to: "/innovation/myinnovation",
      },
    ],
  },

  {
    component: CNavGroup,
    name: "Announcement",
    to: "/announcement",
    icon: <CIcon icon={cilVoiceOverRecord} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Create Announcement",
        to: "/announcement/new",
      },
      {
        component: CNavItem,
        name: "Announcement",
        to: "/announcement/list",
      },
    ],
  },

  {
    component: CNavItem,
    name: "Dashboard",
    to: "/dashboard",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
];

const navigationAMA = [
  {
    component: CNavItem,
    name: "Home",
    to: "/",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
  {
    component: CNavGroup,
    name: "Event",
    to: "/event",
    icon: <CIcon icon={cilCursor} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: "Event List",
        to: "/event/list",
      },
      // {
      //   component: CNavItem,
      //   name: "Create New Event",
      //   to: "/event/new",
      // },
      {
        component: CNavItem,
        name: "Submission",
        to: "/event/submission/proposal/" + id,
      },
      {
        component: CNavItem,
        name: "List of Submission",
        to: "/event/submission/list",
      },
      {
        component: CNavItem,
        name: "Appointment Approval",
        to: "/event/appointment/approval",
      },
      {
        component: CNavItem,
        name: "Appointment",
        to: "/event/appointment",
      },
    ],
  },
  {
    component: CNavGroup,
    name: "Innovation",
    to: "/innovation",
    icon: <CIcon icon={cilStar} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
    items: [
      {
        component: CNavItem,
        name: "Innovation List",
        to: "/innovation/list",
      },
      {
        component: CNavItem,
        name: "My Innovation",
        to: "/innovation/myinnovation",
      },
    ],
  },

  {
    component: CNavGroup,
    name: "Announcement",
    to: "/announcement",
    icon: <CIcon icon={cilVoiceOverRecord} customClassName="nav-icon" />,
    items: [
      // {
      //   component: CNavItem,
      //   name: "Create Announcement",
      //   to: "/announcement/new",
      // },
      {
        component: CNavItem,
        name: "Announcement",
        to: "/announcement/list",
      },
    ],
  },

  {
    component: CNavItem,
    name: "Dashboard",
    to: "/dashboard",
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: "info",
      text: "",
    },
  },
];

export { navigation, navigationAMA, navigationAdmin };
