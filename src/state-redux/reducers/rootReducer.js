import { combineReducers } from "redux";
import authReducer from "./authReducer";
import sidebarReducer from "./sidebarReducer";

const rootReducer = combineReducers({
  auth: authReducer,
  sidebar: sidebarReducer,
});

export default rootReducer;
