const initialState = {
  isLogin: false,
  username: "",
  id: "",
  foto: "",
  role: "",
  access: "",
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "LOGIN": {
      return {
        isLogin: true,
        username: action.payload.username,
        id: action.payload.id,
        foto: action.payload.foto,
        role: action.payload.role,
        access: action.payload.access,
      };
    }
    case "LOGOUT": {
      return {
        isLogin: false,
        id: "",
        name: "",
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
