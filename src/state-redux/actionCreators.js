export const signIn = (profile) => {
  return (dispatch) => {
    dispatch({
      type: "LOGIN",
      payload: {
        username: profile.username,
        id: profile.id,
        foto: profile.foto,
        role: profile.role,
        access: profile.access,
      },
    });
  };
};

export const signOut = () => {
  return (dispatch) => {
    dispatch({
      type: "LOGOUT",
    });
  };
};
