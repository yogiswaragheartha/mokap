import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers/rootReducer";
import thunk from "redux-thunk";

const Store = createStore(rootReducer, {}, applyMiddleware(thunk));

// const initialState = {
//   sidebarShow: false,
// };

// const changeState = (state = initialState, { type, ...rest }) => {
//   switch (type) {
//     case "set":
//       return { ...state, ...rest };
//     default:
//       return state;
//   }
// };

// const StoreB = createStore(changeState);

export default Store;
