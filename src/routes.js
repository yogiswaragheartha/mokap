import React from "react";

const ComingSoon = React.lazy(() => import("./views/pages/ComingSoon"));
const Search = React.lazy(() => import("./views/pages/Search"));
const EventList = React.lazy(() => import("./views/event/EventList"));
const EventNew = React.lazy(() => import("./views/event/EventNew"));
const EventEdit = React.lazy(() => import("./views/event/EventEdit"));
const EventDetail = React.lazy(() => import("./views/event/EventDetail"));
const Page404 = React.lazy(() => import("./views/pages/page404/Page404"));

const Home = React.lazy(() => import("./views/home/Home"));
const Profile = React.lazy(() => import("./views/profile/Profile"));
const User = React.lazy(() => import("./views/user/UserManagement"));
const EditRole = React.lazy(() => import("./views/user/EditRole"));
const NewRole = React.lazy(() => import("./views/user/NewRole"));
const EditProfile = React.lazy(() => import("./views/profile/EditProfile"));

const Submission = React.lazy(() => import("./views/submission/Submission"));
// const SubmissionLatest= React.lazy(() => import("./views/submission/SubmissionLatest"));
const SubmissionNew = React.lazy(() =>
  import("./views/submission/SubmissionEdit")
);
const SubmissionPreview = React.lazy(() =>
  import("./views/submission/SubmissionPreview")
);
const SubmissionProgress = React.lazy(() =>
  import("./views/submission/Progres")
);
const SubmissionReport = React.lazy(() => import("./views/submission/Laporan"));
const ReportPreview = React.lazy(() =>
  import("./views/submission/LaporanPreview")
);
const SubmissionList = React.lazy(() =>
  import("./views/submission/SubmissionList")
);
const SubmissionApproval = React.lazy(() =>
  import("./views/submission/SubmissionApproval")
);

const AppointmentBook = React.lazy(() =>
  import("./views/appointment/AppointmentBook")
);
const AppointmentHistory = React.lazy(() =>
  import("./views/appointment/AppointmentHistory")
);
const AppointmentApproval = React.lazy(() =>
  import("./views/appointment/AppointmentApproval")
);

const AnnouncementEdit = React.lazy(() =>
  import("./views/announcement/AnnouncementEdit")
);

const AnnouncementList = React.lazy(() =>
  import("./views/announcement/AnnouncementList")
);

const AnnouncementDetail = React.lazy(() =>
  import("./views/announcement/AnnouncementDetail")
);

const InnovationList = React.lazy(() =>
  import("./views/innovation/InnovationList")
);

const MyInnovation = React.lazy(() =>
  import("./views/innovation/MyInnovation")
);

const InnovationDetail = React.lazy(() =>
  import("./views/innovation/InnovationDetail")
);

const InnovationDetailBudget = React.lazy(() =>
  import("./views/innovation/InnovationDetailBudget")
);

const InnovationDetailOKR = React.lazy(() =>
  import("./views/innovation/InnovationDetailOKR")
);

const InnovationDetailRevenue = React.lazy(() =>
  import("./views/innovation/InnovationDetailRevenue")
);

const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));

const routes = [
  { path: "/404", name: "404", element: Page404, access: "all" },

  //UserList
  {
    path: "/user",
    name: "User Management",
    element: User,
    access: "all",
  },
  {
    path: "/user/role/:id",
    name: "User Role",
    element: EditRole,
    access: "userRole",
  },
  // { path: "/user/role", name: "User Role", element: NewRole, access:"userRole" },

  //1. Sebagai user saya dapat melakukan registrasi, sign-in, dan mengakses home page pada platform MOKAP

  //1.2 & 1.6 DONE
  {
    path: "/profile/edit",
    name: "Edit Profile",
    element: EditProfile,
    access: "all",
  },

  //1.3
  { path: "/", name: "Home", element: Home, access: "all" },

  //1.4 DONE
  { path: "/search/:search", name: "Search", element: Search, access: "all" },

  //1.5 DONE
  { path: "/profile", name: "Profile", element: Profile, access: "all" },

  //2. Sebagai user saya dapat mengakses event pada platform MOKAP
  { path: "/event", name: "Event", access: "all" },

  //2.1
  {
    path: "/event/new",
    name: "New Event",
    element: EventNew,
    access: "createEvent",
  },

  //2.2
  {
    path: "/event/list",
    name: "Event List",
    element: EventList,
    access: "all",
  },

  //2.3
  {
    path: "/event/detail/:id",
    name: "Event Detail",
    element: EventDetail,
    access: "all",
  },
  {
    path: "/event/detail/:id/:slug",
    name: "Event Detail",
    element: EventDetail,
    access: "all",
  },

  {
    path: "/event/edit/:id",
    name: "Edit Event",
    element: EventEdit,
    access: "createEvent",
  },
  {
    path: "/event/edit/:id/:slug",
    name: "Edit Event",
    element: EventEdit,
    access: "createEvent",
  },

  //3. Sebagai user saya dapat mengakses inovasi, proposal, progress, dan laporan

  {
    path: "/event/submission",
    name: "Submission",
    access: "all",
  },

  //3.1, 3.2
  {
    path: "/event/submission/:page/:id_event/:id_phase/:id_innovation",
    name: "Submission",
    element: Submission,
    access: "submission",
  },
  {
    path: "/event/submission/:page/:id_event",
    name: "Submission",
    element: Submission,
    access: "submission",
  },

  {
    path: "/event/submission/:page/:id_event/:id_phase",
    name: "Submission",
    element: SubmissionNew,
    access: "submission",
  },

  {
    path: "/event/submission/:page",
    name: "Submission",
    element: Submission,
    access: "submission",
  },

  //3.2
  // {
  //   path: "/event/submission/preview/:id",
  //   name: "Submission Preview",
  //   element: SubmissionPreview, access:
  // },

  //3.3
  {
    path: "/event/submission/progress/:id",
    name: "Submission Progress",
    element: SubmissionProgress,
    access: "submission",
  },

  //3.4
  {
    path: "/event/submission/report/:id",
    name: "Submission Report",
    element: SubmissionReport,
    access: "submission",
  },
  {
    path: "/event/submission/report/:id/:id_phase",
    name: "Submission Report",
    element: SubmissionReport,
    access: "submission",
  },
  //3.5
  {
    path: "/event/submission/report/preview/:id",
    name: "Submission Report Preview",
    element: ReportPreview,
    access: "submission",
  },

  //4.1
  {
    path: "/event/submission/list",
    name: "Submission List",
    element: SubmissionList,
    access: "listOfSubmission",
  },

  //4.2
  {
    path: "/event/submission/approval/:page/:id_event/:id_phase/:id_innovation",
    name: "Submission Approval",
    element: SubmissionApproval,
    access: "reviewProposal",
  },

  //5. Sebagai user saya dapat mengakses schedule management fitur pada platform MOKAP

  //5.1
  {
    path: "/event/appointment",
    name: "Appointment",
    element: AppointmentBook,
    access: "appointment",
  },
  {
    path: "/event/appointment/history",
    name: "History",
    element: AppointmentHistory,
    access: "appointment",
  },
  {
    path: "/event/appointment/approval",
    name: "Approval",
    element: AppointmentApproval,
    access: "appointmentApproval",
  },

  //6. Sebagai user saya dapat mengakses dashboard reporting

  //6.1
  { path: "/dashboard", name: "Dashboard", element: Dashboard, access: "all" },

  //7. Sebagai user saya dapat mengakses notifikasi penting pada platform MOKAP

  //INNOVATION
  { path: "/innovation", name: "Innovation", access: "all" },
  {
    path: "/innovation/list",
    name: "Innovation List",
    element: InnovationList,
    access: "innovationList",
  },
  {
    path: "/innovation/myinnovation",
    name: "My Innovation",
    element: MyInnovation,
    access: "myInnovation",
  },
  {
    path: "/innovation/detail/:id",
    name: "Innovation Detail",
    element: InnovationDetail,
    access: "all",
  },
  {
    path: "/innovation/detail/budget/:id",
    name: "Innovation Detail Budget",
    element: InnovationDetailBudget,
    access: "all",
  },

  {
    path: "/innovation/detail/okr/:id",
    name: "Innovation Detail OKR",
    element: InnovationDetailOKR,
    access: "all",
  },

  {
    path: "/innovation/detail/revenue/:id",
    name: "Innovation Detail Revenue",
    element: InnovationDetailRevenue,
    access: "all",
  },

  //ANNOUNCEMENT
  { path: "/announcement", name: "Announcement", access: "all" },
  {
    path: "/announcement/list",
    name: "Announcement List",
    element: AnnouncementList,
    access: "announcement",
  },
  {
    path: "/announcement/detail/:id",
    name: "Announcement Detail",
    element: AnnouncementDetail,
    access: "announcement",
  },
  {
    path: "/announcement/detail/:id/:slug",
    name: "Announcement Detail",
    element: AnnouncementDetail,
    access: "announcement",
  },
  {
    path: "/announcement/new",
    name: "Create Announcement",
    element: AnnouncementEdit,
    access: "createAnnouncement",
  },
  {
    path: "/announcement/edit/:id",
    name: "Edit Announcement",
    element: AnnouncementEdit,
    access: "createAnnouncement",
  },
];

export { routes };
