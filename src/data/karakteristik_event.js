const karakteristik_event = [
  {
    mantap: "1",
    peserta: {
      cv: 45,
      pv1: 6,
      pv2: 15,
      bmv: 3,
      mv: 9,
    },
    tim: {
      cv: 15,
      pv1: 2,
      pv2: 5,
      bmv: 1,
      mv: 3,
    },
    idea: {
      cv: 18,
      pv1: 2,
      pv2: 5,
      bmv: 1,
      mv: 3,
    },
  },
];

export default karakteristik_event;
