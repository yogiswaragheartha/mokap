const performa_pendapatan_inovasi = [
  {
    mantap: "1",
    total_estimasi_pendapatan: {
      cv: 500000,
      pv1: 1500000,
      pv2: 1500000,
      bmv: 2500000,
      mv: 5000000,
    },
    total_pendapatan: {
      cv: 250000,
      pv1: 3000000,
      pv2: 2500000,
      bmv: 1000000,
      mv: 4000000,
    },
    realisasi: {
      cv: 50,
      pv1: 200,
      pv2: 166.6666667,
      bmv: 40,
      mv: 80,
    },
  },
];

export default performa_pendapatan_inovasi;
