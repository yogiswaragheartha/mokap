const data_banner = [
  {
    id: "1",
    banner: require("../assets/banner/banner1.png"),
  },
  {
    id: "2",
    banner: require("../assets/banner/banner2.png"),
  },
  {
    id: "3",
    banner: require("../assets/banner/banner3.png"),
  },
];

export default data_banner;
