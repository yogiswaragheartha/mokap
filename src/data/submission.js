const submission = [
  {
    title: "Mokap",
    tagline: "Bokap, Nyokap, Mokap",
    description:
      "Mokap (Monev Rekap), final project Telkom Athon of Team A. How Effective Your Company Handling Innovation?",
    category: "Digitalization",
    innovator: [
      { nama: "Zlatan", role: "CEO", area: "Area 1" },
      { nama: "Pep", role: "CTO", area: "Area 2" },
      { nama: "Kylian", role: "CMO", area: "Area 1" },
    ],
    estRevenue: 290000000,
    descriptionRev: "Angka tersebut hanya estimasi, realisasi pasti nol.",
    file: {
      pitch: "pitch.pptx",
      additional: "poster.jpg",
    },
    activity: [
      {
        nama: "Meeting",
        type: "Initial Meeting",
        output: "kumpulan problem & pain",
        pic: "Zlatan",
        progres: 30,
      },
      {
        nama: "Menentukan Solusi",
        type: "Disscussion",
        output: "Solusi",
        pic: "Pep",
        progres: 15,
      },
    ],
    budget: [
      {
        activity: "Meeting",
        need: "Sewa zoom",
        freq: 3,
        qty: 1,
        price: 50000,
        total: 150000,
        freqReal: 3,
        qtyReal: 1,
        priceReal: 45000,
        evidence: "struk zoom.jpg",
      },
      {
        activity: "Menentukan Solusi",
        need: "Konsumsi Meeting",
        freq: 3,
        qty: 1,
        price: 60000,
        total: 180000,
        freqReal: 3,
        qtyReal: 1,
        priceReal: 62000,
        evidence: "struk makan.jpg",
      },
    ],

    objective: "Solusi 100% valid",
    digitalization: "All solusi dalam 1 aplikasi",
    digitization: "proses dapat terakomodir secara virtual",
    kr: [
      {
        result: "Solusi fit dengan pain problem",
        target: 100,
        unit: "%",
        realization: 65,
      },
      { result: "Validasi 70 user", target: 70, unit: "user", realization: 55 },
    ],
    revenue: {
      start: "2022/03/12",
      end: "2022/04/12",
      revenue: 15000000,
    },
  },
];

export default submission;
