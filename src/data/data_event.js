const data_event = [
  {
    id: "1",
    Name: "HackIdea 1",
    image: require("../assets/event/HI1.png"),
    status: "inactive",
  },
  {
    id: "2",
    Name: "HackIdea 2",
    image: require("../assets/event/HI2.png"),
    status: "inactive",
  },
  {
    id: "3",
    Name: "HackIdea 3",
    image: require("../assets/event/HI3.png"),
    status: "inactive",
  },
  {
    id: "4",
    Name: "HackIdea 4",
    image: require("../assets/event/HI4.png"),
    status: "inactive",
  },
  {
    id: "5",
    Name: "HackIdea 5",
    image: require("../assets/event/HI5.png"),
    status: "inactive",
  },
  {
    id: "6",
    Name: "HackIdea 6",
    image: require("../assets/event/HI6.png"),
    status: "inactive",
  },
  {
    id: "7",
    Name: "HackIdea 7",
    image: require("../assets/event/HI7.png"),
    status: "active",
  },
];

export default data_event;
