const data_inovasi = [
  {
    id: "1",
    name: "Creativa",
    tagline: "unlimited creation",
    photo: require("../assets/logo/Ellipse 29.png"),
  },
  {
    id: "2",
    name: "Hi Tech",
    tagline: "a tech for life",
    photo: require("../assets/logo/Ellipse 30.png"),
  },
  {
    id: "3",
    name: "iTone",
    tagline: "push to the limit",
    photo: require("../assets/logo/Ellipse 31.png"),
  },
  {
    id: "4",
    name: "Glycon",
    tagline: "connect us to unlimited resource",
    photo: require("../assets/logo/Ellipse 32.png"),
  },
  {
    id: "5",
    name: "Superapp",
    tagline: "something for everything",
    photo: require("../assets/logo/Ellipse 33.png"),
  },
  {
    id: "6",
    name: "Ui Creator",
    tagline: "from art to experience",
    photo: require("../assets/logo/Ellipse 34.png"),
  },
  {
    id: "7",
    name: "ColorHex",
    tagline: "pallete everywhere",
    photo: require("../assets/logo/Ellipse 35.png"),
  },
  {
    id: "8",
    name: "k24 online",
    tagline: "no more worries",
    photo: require("../assets/logo/Ellipse 36.png"),
  },
  {
    id: "9",
    name: "Triangle.io",
    tagline: "no pain no gain",
    photo: require("../assets/logo/Ellipse 37.png"),
  },
  {
    id: "10",
    name: "Bobcats",
    tagline: "your pet platform",
    photo: require("../assets/logo/Ellipse 38.png"),
  },
];

export default data_inovasi;
