const performa_biaya_inovasi = [
  {
    mantap: "1",
    total_estimasi_pengeluaran: {
      cv: 45000000,
      pv1: 25000000,
      pv2: 2500000,
      bmv: 31500000,
      mv: 60000000,
    },
    total_pengeluaran: {
      cv: 35000000,
      pv1: 30000000,
      pv2: 2500000,
      bmv: 1000000,
      mv: 25000000,
    },
    realisasi: {
      cv: 77.777777,
      pv1: 120,
      pv2: 100,
      bmv: 66.6666667,
      mv: 41.66666666666667,
    },
  },
];

export default performa_biaya_inovasi;
