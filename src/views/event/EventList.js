import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CNavLink,
  CCardImage,
  CCardText,
  CCardTitle,
  CRow,
} from "@coreui/react";
import {
  useNavigate,
  NavLink,
  Link,
  useLocation,
  useParams,
} from "react-router-dom";
import { useSelector } from "react-redux";
import CIcon from "@coreui/icons-react";
import { cilMediaPlay, cilMediaStop, cilAddressBook } from "@coreui/icons";
import { getEvent } from "src/api/event";
import Moment from "moment";
import ModalAlert from "src/components/ModalAlert";
import LoadingData from "src/components/LoadingData";

const EventList = () => {
  const token = localStorage.getItem("token_jwt");
  const [event, setEvent] = useState([]);
  const [isLoadingData, setIsLoadingData] = useState(true);
  const d = new Date();
  const { auth } = useSelector((state) => state);
  let location = useLocation();
  const [modal, setModal] = useState({
    visible: location.state
      ? location.state.visible == true
        ? true
        : false
      : false,
    message: location.state ? location.state.message : "",
    status: location.state ? location.state.status : "",
  });

  const get_event = async (token) => {
    try {
      const response = await getEvent(token);

      const filteredEvent = response.data.filter(
        (c) =>
          (auth.access.includes("activeEvent") && c.status == true) ||
          (auth.access.includes("inactiveEvent") && c.status == false)
      );

      console.log("Event", filteredEvent);
      setEvent(filteredEvent.sort((a, b) => (a.id > b.id ? -1 : 1)) || []);
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  useEffect(() => {
    get_event(token);
  }, []);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row ">
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <CContainer>
        <CRow className="">
          <h3>
            Event List{" "}
            {auth.access.includes("createEvent") ? (
              <CNavLink to={"/event/new"} component={NavLink}>
                <CButton
                  color="primary"
                  className="text-white"
                  style={{ width: "200px", float: "right" }}
                >
                  + Create new event
                </CButton>
              </CNavLink>
            ) : (
              ""
            )}
          </h3>

          {isLoadingData ? (
            <LoadingData />
          ) : (
            <CCol md={12} lg={11} xl={10}>
              {event.length < 1 ? (
                <i>no event can be shown</i>
              ) : (
                event.map(function (item) {
                  if (item.status == false) {
                    //aktif
                    var cekMuted = "p-4 shadow opacity-50";
                  } else {
                    var cekMuted = "p-4 shadow  opacity-100";
                  }
                  return (
                    <Link
                      to={"/event/detail/" + item.id + "/" + item.slug}
                      className="text-decoration-none "
                      style={{ color: "rgba(44, 56, 74, 0.95)" }}
                    >
                      <CCard className="mt-2">
                        <CCardBody className={cekMuted}>
                          <CRow>
                            <CCol md={3}>
                              <img
                                orientation="left"
                                src={item.banner}
                                style={{ width: "100%" }}
                              />
                            </CCol>
                            <CCol xs={12} md={9}>
                              <CCardTitle className="text-secondary">
                                {item.name}
                              </CCardTitle>
                              <CCardText>
                                <p className="text-mini ">
                                  {item.description.length > 500
                                    ? item.description.substr(0, 500) + "..."
                                    : item.description}
                                </p>
                              </CCardText>
                              <div class="text-muted text-mini">
                                <CRow>
                                  <CCol xs={4} md={3}>
                                    <CIcon
                                      icon={cilMediaPlay}
                                      className="pt-1"
                                    />
                                    Start:{" "}
                                    {Moment(item.timelineStart).format(
                                      "DD MMM YYYY"
                                    )}
                                  </CCol>
                                  <CCol xs={4} md={3}>
                                    <CIcon
                                      icon={cilMediaStop}
                                      className="pt-1"
                                    />
                                    End:{" "}
                                    {Moment(item.timelineEnd).format(
                                      "DD MMM YYYY"
                                    )}
                                  </CCol>
                                  <CCol xs={4} md={3}>
                                    <CIcon
                                      icon={cilAddressBook}
                                      className="pt-1"
                                    />
                                    {item.totalInnovation} Innovations
                                  </CCol>
                                </CRow>
                              </div>
                            </CCol>
                          </CRow>
                        </CCardBody>
                      </CCard>
                    </Link>
                  );
                })
              )}
              {}
            </CCol>
          )}
        </CRow>
      </CContainer>
    </div>
  );
};

export default EventList;
