import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CSpinner,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import axios from "axios";
import { Link, useNavigate, useParams } from "react-router-dom";
// import { signIn } from "../../state-redux/actionCreators";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { getUserMokap, updateUserMokap } from "src/api/user";
import { updateEvent, getEvent } from "src/api/event";
import Phase from "./Phase";
import Category from "./Category";
import Input from "src/components/Input";
import Moment from "moment";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import LoadingContent from "src/components/LoadingContent";

const EventEdit = () => {
  const token = localStorage.getItem("token_jwt");
  const [loadingState, setLoadingState] = useState(true);
  const [phase, setPhase] = useState([]);
  const [category, setCategory] = useState([]);
  const { id } = useParams();
  const [countPhase, setCountPhase] = useState(2);
  const [countCategory, setCountCategory] = useState(2);
  const [status, setStatus] = useState();
  const [event, setEvent] = useState([]);
  const [mod, setMod] = useState(false);
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });
  const [submitText, setSubmitText] = useState("Submit");
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  //   const { auth } = useSelector((state) => state);
  //   const dispatch = useDispatch();
  //   const AC = bindActionCreators({ signIn }, dispatch);

  const handleAddPhase = () => {
    setCountPhase(countPhase + 1);
    const newPhase = phase;
    newPhase.push({});
    setPhase(newPhase);
  };

  const handleAddCategory = () => {
    setCountCategory(countCategory + 1);
    const newCategory = category;
    newCategory.push({});
    setCategory(newCategory);
  };

  const handleSetStatus = () => {
    if (document.getElementById("status").checked == true) {
      setStatus(true);
    } else {
      setStatus(false);
    }
  };

  const cekTanggal = (start, end) => {
    if (end < start) {
      return false;
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const name = document.getElementById("title").value;
    const tagline = document.getElementById("tagline").value;
    const description = document.getElementById("description").value;
    const timelineStart = document.getElementById("start").value;
    const timelineEnd = document.getElementById("end").value;
    const status = document.getElementById("status").checked;
    const banner = document.getElementById("banner").files[0];
    const cekBanner = document.getElementById("cek-banner").value;
    const other = document.getElementById("otherfile").files[0];
    console.log(name, tagline, description, timelineStart, timelineEnd, banner);
    if (
      !name ||
      !tagline ||
      !description ||
      !timelineStart ||
      !timelineEnd ||
      !cekBanner
    ) {
      setModal({
        visible: true,
        message: "Fill in the form corectly",
        status: "warning",
      });
      setSubmitText("Submit");
      setMod(false);
      return;
    }

    //cek tanggal
    if (cekTanggal(timelineStart, timelineEnd) == false) {
      setMod(false);
      setModal({
        visible: true,
        message: "End date of event must be higher than start date",
        status: "warning",
      });
      setSubmitText("Submit");
      return;
    }

    setMod(false);
    setSubmitText("Loading..");

    const data = {
      name,
      tagline,
      description,
      timelineStart,
      timelineEnd,
      status,
      banner,
      other,
    };

    //CATEGORY
    const categoryName = document.querySelectorAll("#category-name");
    for (let i = 0; i < categoryName.length; i++) {
      const name = categoryName[i].value;
      const id = document.querySelectorAll("#category-id")[i].value;

      if (!name) {
        setModal({
          visible: true,
          message: "Category " + (i + 1) + ": Fill in the form corectly",
          status: "warning",
        });
        setSubmitText("Submit");
        return;
      }

      if (id) {
        data[`categories[${i}][id]`] = id;
        data[`categories[${i}][name]`] = name;
      } else {
        data[`categories[${i}]`] = name;
      }
    }

    //PHASE
    for (let i = 0; i < document.querySelectorAll("#phase-name")?.length; i++) {
      const id = document.querySelectorAll("#phase-id")[i].value;
      const name = document.querySelectorAll("#phase-name")[i].value;
      const initial = document.querySelectorAll("#phase-initial")[i].value;
      const start = document.querySelectorAll("#phase-start")[i].value;
      const end = document.querySelectorAll("#phase-end")[i].value;
      const description =
        document.querySelectorAll("#phase-description")[i].value;

      if (cekTanggal(start, end) == false) {
        setMod(false);
        setSubmitText("Submit");
        setModal({
          visible: true,
          message:
            "End date of Phase " + (i + 1) + " must be higher than start date",
          status: "warning",
        });
        return;
      }
      if (!name || !initial || !description) {
        setModal({
          visible: true,
          message: "Phase " + (i + 1) + ": Fill in the form corectly",
          status: "warning",
        });
        setSubmitText("Submit");
        return;
      }

      if (id) {
        data[`phases[${i}][id]`] = id;
      }
      data[`phases[${i}][name]`] = name;
      data[`phases[${i}][initial]`] = initial;
      data[`phases[${i}][description]`] = description;
      data[`phases[${i}][timelineStart]`] = timelineStart;
      data[`phases[${i}][timelineEnd]`] = timelineEnd;
      data[`phases[${i}][status]`] = true;
    }
    console.log("SUBMIT", data);

    setIsLoading(true);

    try {
      const response = await updateEvent(token, data, id);
      console.log("Edit Event:", response.data.message);

      if (response.data.message) {
        navigate("/event/list", {
          state: {
            visible: true,
            status: "success",
            message: "",
          },
        });
      }
      setSubmitText("Submit");
      setIsLoading(false);
    } catch (err) {
      alert(err.message);
      setSubmitText("Submit");
      setIsLoading(false);
    }
  };

  const get_event = async (token, id) => {
    try {
      const response = await getEvent(token, id);
      console.log(response.data);
      setEvent(response.data.event);

      // setCountPhase(countPhase + 1);
      setPhase(response.data.event.Phases);

      setCategory(response.data.event.Categories);

      setStatus(response.data.event.status);
      if (response.data.event.status == true) {
        document.getElementById("status").checked = true;
      } else {
        document.getElementById("status").checked = false;
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    get_event(token, id);
  }, []);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row align-items-center">
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <CContainer>
        <h3>Edit Event {event.nama}</h3>
        <div className=" justify-content-center">
          <div md={9} lg={7} xl={6} sm={12} className="mt-4    ">
            <div className="">
              <div
                className="p-4 shadow "
                style={{ border: "1px solid #ccc", borderRadius: "10px" }}
              >
                <CForm>
                  <Input
                    id="title"
                    label="Event Title"
                    type="text"
                    placeholder="Name of the Event"
                    value={event.name}
                    required
                  />

                  <Input
                    id="tagline"
                    label="Tagline"
                    type="text"
                    placeholder="Tagline of the Event"
                    value={event.tagline}
                    required
                  />

                  <Input
                    id="description"
                    label="Event Description"
                    type="text-area"
                    placeholder="Describe the event"
                    value={event.description}
                    required
                  />

                  <div class="mb-3">
                    <CRow className="">
                      <CCol md={5}>
                        <Input
                          id="start"
                          label="Start"
                          type="date"
                          placeholder=""
                          value={Moment(event.timelineStart).format(
                            "YYYY-MM-DD"
                          )}
                          required
                        />
                      </CCol>
                      <CCol md={5}>
                        <Input
                          id="end"
                          label="End"
                          type="date"
                          placeholder=""
                          value={Moment(event.timelineEnd).format("YYYY-MM-DD")}
                          required
                        />
                      </CCol>
                      <CCol md={2}></CCol>
                    </CRow>
                  </div>

                  <div class="mb-3">
                    <CRow className="">
                      <CCol md={6}>
                        <Input
                          id="banner"
                          label="Event Banner"
                          type="file"
                          value={event.banner}
                          placeholder=""
                          required
                        />
                      </CCol>
                      <CCol md={6}>
                        <Input
                          id="otherfile"
                          label="Additional File"
                          type="file"
                          value={event.otherFile}
                          placeholder=""
                        />
                      </CCol>
                    </CRow>
                  </div>

                  <div class="mb-3">
                    <CRow className="">
                      <CCol md={4}>
                        <label for="category" class="form-label">
                          Category for Innovation <code>*</code>
                        </label>
                        {category.map(function (item, i) {
                          return (
                            <>
                              <Category i={i + 1} value={item} />
                            </>
                          );
                        })}
                      </CCol>

                      <CCol md={8}></CCol>
                    </CRow>
                  </div>

                  <div class="mb-3">
                    <CButton
                      color="secondary"
                      className="text-white btn-sm"
                      onClick={handleAddCategory}
                    >
                      + More Category
                    </CButton>
                  </div>

                  <div class="mb-3">
                    <CRow className="">
                      <CCol md={4}>
                        <label for="status" class="form-label">
                          Status
                        </label>
                        <div class="form-check form-switch">
                          <input
                            class="form-check-input"
                            type="checkbox"
                            role="switch"
                            id="status"
                            value={status}
                            onClick={handleSetStatus}
                          />
                          <label class="form-check-label" for="status">
                            {status ? "Active" : "Inactive"}
                          </label>
                        </div>
                      </CCol>

                      <CCol md={8}></CCol>
                    </CRow>
                  </div>

                  {phase.map(function (item, i) {
                    return (
                      <>
                        <Phase i={i + 1} value={item} />
                      </>
                    );
                  })}

                  <div class="mb-3">
                    <CButton
                      color="secondary"
                      className="text-white btn-sm"
                      onClick={handleAddPhase}
                    >
                      + More Phase
                    </CButton>
                  </div>

                  <div className="" style={{ textAlign: "center" }}>
                    <CButton
                      onClick={() => setMod(!mod)}
                      style={{ width: "90px" }}
                      className={
                        "bg-warning text-dark border-0 mt-4 btn-md me-2"
                      }
                    >
                      {submitText}
                    </CButton>
                    <Modal
                      pesan="Are you sure to Submit?"
                      input={false}
                      callBack={handleSubmit}
                      close={() => setMod(false)}
                      visible={mod}
                    />
                  </div>
                </CForm>
              </div>
            </div>
          </div>
        </div>
      </CContainer>
    </div>
  );
};

export default EventEdit;
