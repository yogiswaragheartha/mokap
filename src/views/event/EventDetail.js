import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CNavLink,
  CCardImage,
  CCardText,
  CCardTitle,
  CRow,
  CAccordion,
} from "@coreui/react";
import axios from "axios";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import CIcon from "@coreui/icons-react";
import { cilMediaPlay, cilMediaStop, cilAddressBook } from "@coreui/icons";
import PhaseList from "./PhaseList";
import { getEvent } from "src/api/event";
import { getInnovation, getSubmissionRecap } from "src/api/submission";
import { useSelector } from "react-redux";
import Moment from "moment";
import LoadingData from "src/components/LoadingData";

const EventDetail = () => {
  const token = localStorage.getItem("token_jwt");
  const [event, setEvent] = useState([]);
  const [phase, setPhase] = useState([]);
  const { id } = useParams();
  const [jlhInovasi, setJlhInovasi] = useState(0);
  const [isLoadingData, setIsLoadingData] = useState(true);
  const navigate = useNavigate();
  const [isNewSubmission, setIsNewSubmission] = useState(true);
  const [innovations, setInnovations] = useState({});
  const [log, setLog] = useState();
  const { auth } = useSelector((state) => state);

  const logs = [
    {
      dateTime: "22/05/2022; 15:30",
      log: "You submit a submission",
      note: "waiting for approval",
    },
    {
      dateTime: "22/05/2022; 15:33",
      log: "Your submission is returned by AMA",
      note: "Tolong di cek kembali untuk OKR nya, sepertinya salah penulisan",
    },
  ];

  const openSubmission = (id_phase, id_innovation) => {
    if (innovations.stage == "PROPOSAL" || !innovations.stage) {
      navigate(
        "/event/submission/proposal/" +
          id +
          "/" +
          id_phase +
          "/" +
          id_innovation
      );
    } else if (innovations.stage == "PROGRESS") {
      navigate(
        "/event/submission/progress/" +
          id +
          "/" +
          id_phase +
          "/" +
          id_innovation
      );
    } else if (innovations.stage == "REPORT") {
      navigate(
        "/event/submission/report/" + id + "/" + id_phase + "/" + id_innovation
      );
    }
  };

  const get_event = async (token, idevent) => {
    try {
      const response = await getEvent(token, idevent);

      console.log("Event", response.data);
      setEvent(response.data.event);
      setPhase(response.data.event.Phases);

      if (response.data.status == false && auth.role != 1) {
        navigate("/event/list", {
          state: {
            visible: true,
            status: "danger",
            message: "Event sudah tidak aktif, silakan menghubungi Admin",
          },
        });
      }

      if (response.data.userInnovation) {
        const innovationId = response.data.userInnovation.id;
        const phaseId = response.data.userInnovation.phaseId;
        get_innovation(token, innovationId, phaseId);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  const get_innovation = async (token, innovationId, phaseId) => {
    try {
      // const response = await getInnovation(token);
      const response = await getSubmissionRecap(token, innovationId, phaseId);

      // const innovation = response.data.filter(
      //   (c) => c.eventId == id_event && c.createdByUserId == id_user
      // );

      console.log("Innovation", response.data);

      if (!innovationId) {
        // if ga ada, artinya submission baru
        setIsNewSubmission(true);
      } else {
        setIsNewSubmission(false);
        setInnovations(response.data);
        setLog(response.data.approvals);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const get_total_inovasi = async (token) => {
    try {
      const response = await getInnovation(token);
      const totalInovasi = response.data.filter((c) => c.eventId == id).length;

      setJlhInovasi(totalInovasi);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    get_event(token, id);
  }, []);

  // useEffect(() => {
  //   if (auth.id) {
  //     get_innovation(token, id, auth.id);
  //   }
  // }, [auth.id]);

  useEffect(() => {
    get_total_inovasi(token);
  }, [innovations]);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row ">
      <CContainer>
        <CRow className="">
          <CRow>
            <CCol md={6}>
              <h3>{event.name}</h3>
            </CCol>
            <CCol md={6}>
              {auth.access.includes("createEvent") ? (
                <CNavLink
                  to={"/event/edit/" + event.id + "/" + event.slug}
                  component={NavLink}
                >
                  <CButton
                    color="primary"
                    className="text-white"
                    style={{ width: "180px", float: "right" }}
                  >
                    Edit this event
                  </CButton>
                </CNavLink>
              ) : (
                ""
              )}
            </CCol>
          </CRow>
          <CCol md={12} lg={12} xl={12}>
            {isLoadingData ? (
              <LoadingData />
            ) : (
              <CCard className="mt-2 border-0">
                <>
                  <CRow>
                    <CCol md={12}>
                      <img
                        orientation="left"
                        src={event.banner}
                        onClick={() => window.open(event.banner)}
                        className=""
                        style={{
                          width: "100%",
                          maxHeight: "340px",
                          objectFit: "cover",
                        }}
                      />
                    </CCol>
                    <CCol xs={12} md={12}>
                      <CRow>
                        <CCol md={12} lg={8}>
                          <CCardTitle className="text-secondary  mt-4">
                            Description
                          </CCardTitle>
                          <CCardText>
                            <p className="" style={{ textAlign: "justify" }}>
                              {event.description}
                            </p>
                          </CCardText>

                          <CCardTitle className="text-secondary mt-4">
                            Tagline
                          </CCardTitle>
                          <CCardText>
                            <p className=" ">{event.tagline}</p>
                          </CCardText>

                          <CCardTitle className="text-secondary mt-4">
                            File
                          </CCardTitle>
                          <CCardText>
                            <p>
                              {!event.otherFile ? (
                                "-"
                              ) : (
                                <button
                                  className="btn btn-md bg-dark text-white"
                                  onClick={() => window.open(event.otherFile)}
                                >
                                  Open file
                                </button>
                              )}
                            </p>
                          </CCardText>

                          <CCardTitle className="text-secondary mt-4">
                            Phase
                          </CCardTitle>

                          <CAccordion activeItemKey={2}>
                            {!phase
                              ? ""
                              : phase.map(function (item) {
                                  {
                                    return <PhaseList data={item} />;
                                  }
                                })}
                          </CAccordion>
                        </CCol>
                        <CCol lg={4}>
                          <CCard className="mt-4 shadow">
                            <CCardBody className="">
                              <CRow>
                                <CCol xs={8}>
                                  <CCardTitle className="text-secondary ">
                                    Event Summary
                                  </CCardTitle>
                                </CCol>
                                <CCol xs={4}>
                                  <CButton
                                    color={
                                      event.status == true
                                        ? "success"
                                        : "danger"
                                    }
                                    variant="outline"
                                    className=" btn-sm"
                                    style={{
                                      marginTop: "-5px",
                                    }}
                                  >
                                    {event.status == true
                                      ? "Active"
                                      : "Inactive"}
                                  </CButton>
                                </CCol>
                              </CRow>

                              <table className="" style={{ width: "100%" }}>
                                <tr>
                                  <td>
                                    <CIcon icon={cilMediaPlay} /> Start
                                  </td>
                                  <td>:</td>
                                  <td>
                                    {Moment(event.timelineStart).format(
                                      "DD MMM YYYY"
                                    )}
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <CIcon icon={cilMediaStop} /> End
                                  </td>
                                  <td>:</td>
                                  <td>
                                    {Moment(event.timelineEnd).format(
                                      "DD MMM YYYY"
                                    )}
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <CIcon icon={cilAddressBook} /> Innovation
                                  </td>
                                  <td>:</td>
                                  <td>{jlhInovasi} </td>
                                </tr>
                              </table>
                              {isNewSubmission && event.status ? (
                                <CNavLink
                                  to={"/event/submission/proposal/" + event.id}
                                  component={NavLink}
                                  className="text-center mt-2"
                                >
                                  <CButton
                                    color="primary"
                                    className="text-white btn-sm"
                                    style={{ float: "center" }}
                                  >
                                    Join this event
                                  </CButton>
                                </CNavLink>
                              ) : event.status ? (
                                <div className="text-center mt-2">
                                  <CButton
                                    color="primary"
                                    className="text-white btn-sm "
                                    style={{ float: "center", opacity: "0.5" }}
                                  >
                                    Already Joined
                                  </CButton>
                                </div>
                              ) : (
                                ""
                              )}
                            </CCardBody>
                          </CCard>

                          {isNewSubmission || !innovations.submission ? (
                            ""
                          ) : (
                            <CCard className="mt-4 shadow">
                              <CCardBody className="">
                                <CRow>
                                  <CCol xs={8}>
                                    <CCardTitle className="text-secondary ">
                                      Submission
                                    </CCardTitle>
                                  </CCol>
                                  <CCol xs={4}>
                                    <CButton
                                      color={
                                        innovations.submission.status == "DRAFT"
                                          ? "primary"
                                          : innovations.submission.status ==
                                            "SUBMITTED"
                                          ? "warning"
                                          : innovations.submission.status ==
                                              "APPROVED" ||
                                            innovations.submission.status ==
                                              "REPORTED" ||
                                            !innovations.submission.status
                                          ? "success"
                                          : "danger"
                                      }
                                      className=" btn-sm text-white"
                                      style={{
                                        marginTop: "-5px",
                                      }}
                                    >
                                      {innovations.submission.status == "DRAFT"
                                        ? "Draft"
                                        : innovations.submission.status ==
                                          "SUBMITTED"
                                        ? "Waiting Approval"
                                        : innovations.submission.status == null
                                        ? "New Phase"
                                        : innovations.submission.status}
                                    </CButton>
                                  </CCol>
                                </CRow>

                                <table className="" style={{ width: "100%" }}>
                                  <tr>
                                    <td>Created</td>
                                    <td>:</td>
                                    <td>
                                      {Moment(
                                        innovations.submission.createdAt
                                      ).format("DD MMM YYYY")}
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Innovation</td>
                                    <td>:</td>
                                    <td>{innovations.submission.title}</td>
                                  </tr>
                                  <tr>
                                    <td>Phase</td>
                                    <td>:</td>
                                    <td>
                                      {innovations.phase
                                        ? innovations.phase.name
                                        : ""}
                                    </td>
                                  </tr>
                                </table>
                                <div
                                  onClick={() =>
                                    openSubmission(
                                      innovations.submission.phaseId,
                                      innovations.submission.id
                                    )
                                  }
                                  className="text-center mt-2"
                                >
                                  <CButton
                                    color="warning"
                                    className="text-white btn-sm"
                                    style={{ float: "center" }}
                                  >
                                    Open
                                  </CButton>
                                </div>
                                <CCardTitle className="text-secondary ">
                                  Log
                                </CCardTitle>
                                <div
                                  style={{
                                    borderLeft: "2px solid #bbe",
                                  }}
                                  className="ps-2 text-mini"
                                >
                                  <table>
                                    <tr>
                                      <td className="py-2">
                                        <div
                                          style={{
                                            borderRadius: "50%",
                                            width: "10px",
                                            height: "10px",
                                            marginLeft: "-15px",
                                          }}
                                          className="bg-primary"
                                        ></div>
                                      </td>
                                      <td className="py-1">
                                        <div className="text-muted">
                                          {Moment(
                                            innovations.submission.createdAt
                                          ).format("DD MMM YYYY; HH:mm")}
                                        </div>
                                        <div>You create a submission</div>
                                        <div className="text-muted">
                                          <i>Complete and submit the data</i>
                                        </div>
                                      </td>
                                    </tr>
                                  </table>
                                </div>
                                {!log
                                  ? ""
                                  : log.map(function (item) {
                                      {
                                        return (
                                          <div
                                            style={{
                                              borderLeft: "2px solid #bbe",
                                            }}
                                            className="ps-2 text-mini"
                                          >
                                            <table>
                                              <tr>
                                                <td className="py-2">
                                                  <div
                                                    style={{
                                                      borderRadius: "50%",
                                                      width: "10px",
                                                      height: "10px",
                                                      marginLeft: "-15px",
                                                    }}
                                                    className="bg-primary"
                                                  ></div>
                                                </td>
                                                <td className="py-1">
                                                  <div className="text-muted">
                                                    {Moment(
                                                      item.createdAt
                                                    ).format(
                                                      "DD MMM YYYY; HH:mm"
                                                    )}
                                                  </div>
                                                  <div>
                                                    Your submission is{" "}
                                                    {item.status} by AMA
                                                  </div>
                                                  <div className="text-muted">
                                                    <i>{item.comment}</i>
                                                  </div>
                                                </td>
                                              </tr>
                                            </table>
                                          </div>
                                        );
                                      }
                                    })}
                              </CCardBody>
                            </CCard>
                          )}
                        </CCol>
                      </CRow>
                    </CCol>
                  </CRow>
                </>
              </CCard>
            )}
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default EventDetail;
