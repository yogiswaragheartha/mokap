import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";

import {
  CAccordion,
  CAccordionBody,
  CAccordionItem,
  CAccordionHeader,
} from "@coreui/react";

const PhaseList = (props) => {
  return (
    <CAccordionItem itemKey={props.i + 1} style={{}}>
      <CAccordionHeader className="">{props.data.name}</CAccordionHeader>
      <CAccordionBody>{props.data.description}</CAccordionBody>
    </CAccordionItem>
  );
};

export default PhaseList;
