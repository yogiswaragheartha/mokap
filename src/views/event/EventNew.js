import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CSpinner,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import axios from "axios";
import { Link, useNavigate } from "react-router-dom";
// import { signIn } from "../../state-redux/actionCreators";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { getUserMokap, updateUserMokap } from "src/api/user";
import { createEvent } from "src/api/event";
import Phase from "./Phase";
import Category from "./Category";
import Input from "src/components/Input";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import LoadingContent from "src/components/LoadingContent";

const EventNew = () => {
  const token = localStorage.getItem("token_jwt");
  const [phase, setPhase] = useState([
    {
      i: 1,
      name: "phase1",
    },
  ]);
  const [category, setCategory] = useState([
    {
      i: 1,
      name: "category1",
    },
  ]);
  const [countPhase, setCountPhase] = useState(2);
  const [countCategory, setCountCategory] = useState(2);
  const [status, setStatus] = useState("Inactive");
  const [tglEnd, setTglEnd] = useState("");
  const [mod, setMod] = useState(false);
  const [submitText, setSubmitText] = useState("Submit");
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });
  const [isLoading, setIsLoading] = useState(false);
  const navigate = useNavigate();
  const [demo, setDemo] = useState({});

  //   const { auth } = useSelector((state) => state);
  //   const dispatch = useDispatch();
  //   const AC = bindActionCreators({ signIn }, dispatch);

  const handleAddPhase = () => {
    setCountPhase(countPhase + 1);
    const newPhase = phase;
    newPhase.push({
      i: countPhase,
      name: "phase" + countPhase,
    });
    setPhase(newPhase);
    console.log("Phase", countPhase, phase);
  };

  const handleAddCategory = () => {
    setCountCategory(countCategory + 1);
    const newCategory = category;
    newCategory.push({
      i: countCategory,
      name: "category" + countCategory,
    });
    setCategory(newCategory);
    console.log("Category", countCategory, category);
  };

  const handleSetStatus = () => {
    if (document.getElementById("status").checked == true) {
      setStatus("Active");
    } else {
      setStatus("Inactive");
    }
  };

  // const handlePhaseName = (i) => {
  //   setPhaseName();
  // };
  const cekTanggal = (start, end) => {
    if (end < start) {
      return false;
    }
  };

  const handleSubmit = async (event) => {
    const name = document.getElementById("title").value;
    const tagline = document.getElementById("tagline").value;
    const description = document.getElementById("description").value;
    const timelineStart = document.getElementById("start").value;
    const timelineEnd = document.getElementById("end").value;
    const status = document.getElementById("status").checked;
    const banner = document.getElementById("banner").files[0];
    const cekBanner = document.getElementById("cek-banner").value;
    const other = document.getElementById("otherfile").files[0];
    console.log(name, tagline, description, timelineStart, timelineEnd, banner);
    if (
      !name ||
      !tagline ||
      !description ||
      !timelineStart ||
      !timelineEnd ||
      !cekBanner
    ) {
      setModal({
        visible: true,
        message: "Fill in the form corectly",
        status: "warning",
      });
      setSubmitText("Submit");
      setMod(false);
      return;
    }

    //cek tanggal
    if (cekTanggal(timelineStart, timelineEnd) == false) {
      setMod(false);
      setModal({
        visible: true,
        message: "End date of event must be higher than start date",
        status: "warning",
      });
      setSubmitText("Submit");
      return;
    }

    setMod(false);
    setSubmitText("Loading..");

    const data = {
      name,
      tagline,
      description,
      timelineStart,
      timelineEnd,
      status,
      banner,
      other,
    };

    //CATEGORY
    const categoryName = document.querySelectorAll("#category-name");
    for (let i = 0; i < categoryName.length; i++) {
      const name = categoryName[i].value;
      const id = document.querySelectorAll("#category-id")[i].value;

      if (!name) {
        setModal({
          visible: true,
          message: "Category " + (i + 1) + ": Fill in the form corectly",
          status: "warning",
        });
        setSubmitText("Submit");
        return;
      }

      data[`categories[${i}][name]`] = name;
    }

    //PHASE
    for (let i = 0; i < document.querySelectorAll("#phase-name")?.length; i++) {
      const id = document.querySelectorAll("#phase-id")[i].value;
      const name = document.querySelectorAll("#phase-name")[i].value;
      const initial = document.querySelectorAll("#phase-initial")[i].value;
      const start = document.querySelectorAll("#phase-start")[i].value;
      const end = document.querySelectorAll("#phase-end")[i].value;
      const description =
        document.querySelectorAll("#phase-description")[i].value;

      if (cekTanggal(start, end) == false) {
        setMod(false);
        setSubmitText("Submit");
        setModal({
          visible: true,
          message:
            "End date of Phase " + (i + 1) + " must be higher than start date",
          status: "warning",
        });
        return;
      }
      if (!name || !initial || !description) {
        setModal({
          visible: true,
          message: "Phase " + (i + 1) + ": Fill in the form corectly",
          status: "warning",
        });
        setSubmitText("Submit");
        return;
      }

      if (id) {
        data[`phases[${i}][id]`] = id;
      }
      data[`phases[${i}][name]`] = name;
      data[`phases[${i}][initial]`] = initial;
      data[`phases[${i}][description]`] = description;
      data[`phases[${i}][timelineStart]`] = timelineStart;
      data[`phases[${i}][timelineEnd]`] = timelineEnd;
      data[`phases[${i}][status]`] = true;
    }
    console.log("SUBMIT", data);

    setIsLoading(true);

    try {
      const response = await createEvent(token, data);
      console.log("Edit Event:", response.data);

      if (response.data.message) {
        navigate("/event/list", {
          state: {
            visible: true,
            status: "success",
            message: "",
          },
        });
      }
      setSubmitText("Submit");
      setIsLoading(false);
    } catch (err) {
      alert(err.message);
      setSubmitText("Submit");
      setIsLoading(false);
    }
  };

  const getUser = async (token) => {
    try {
      const response = await getUserMokap(token);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDemoMode = () => {
    setDemo({
      title: "HackIdea 2022",
      tagline: "Be the real game changer",
      description:
        "The Amoeba management provide administrative, human capital, finance shared services for the amoebas, as well as policy protection and advocation, problem solving medition and telkom group synergy mediation, up to balancing capabilities either the personal of the amoeba team itselft.",
      start: "2022-06-01",
      end: "2022-10-30",
    });
    setCategory([
      { i: 1, name: "New Business" },
      { i: 2, name: "Value Creation" },
      { i: 3, name: "Others" },
    ]);
    setPhase([
      {
        name: "Customer Validation",
        initial: "CV",
        timelineStart: "2022-06-01",
        timelineEnd: "2022-06-15",
        description: "Ini adalah fase pertama yaitu customer validation.",
      },
      {
        name: "Product Validation",
        initial: "PV",
        timelineStart: "2022-06-16",
        timelineEnd: "2022-07-30",
        description: "Ini adalah fase kedua yaitu product validation.",
      },
    ]);
  };

  useEffect(() => {}, []);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row align-items-center">
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <CContainer>
        <h3>Create New Event</h3>
        <div className=" justify-content-center">
          <div md={9} lg={7} xl={6} sm={12} className="mt-4    ">
            <div className="">
              <div
                className="p-4 shadow "
                style={{ border: "1px solid #ccc", borderRadius: "10px" }}
              >
                <CForm>
                  <Input
                    id="title"
                    label="Event Title"
                    type="text"
                    placeholder="Name of the Event"
                    value={demo.title}
                    callBack={(e) => {
                      if (e.target.value == "demomode") {
                        handleDemoMode();
                      }
                    }}
                    required
                  />

                  <Input
                    id="tagline"
                    label="Tagline"
                    type="text"
                    placeholder="Tagline of the Event"
                    value={demo.tagline}
                    required
                  />

                  <Input
                    id="description"
                    label="Event Description"
                    type="text-area"
                    placeholder="Describe the Event"
                    value={demo.description}
                    required
                  />

                  <div class="mb-3">
                    <CRow className="">
                      <CCol md={5}>
                        <Input
                          id="start"
                          label="Start"
                          type="date"
                          placeholder=""
                          value={demo.start}
                          required
                        />
                      </CCol>
                      <CCol md={5}>
                        <Input
                          id="end"
                          label="End"
                          type="date"
                          placeholder=""
                          value={demo.end}
                          required
                        />
                      </CCol>
                      <CCol md={2}></CCol>
                    </CRow>
                  </div>

                  <div class="mb-3">
                    <CRow className="">
                      <CCol md={6}>
                        <Input
                          id="banner"
                          label="Event Banner"
                          type="file"
                          placeholder=""
                          value=""
                          required
                        />
                      </CCol>
                      <CCol md={6}>
                        <Input
                          id="otherfile"
                          label="Additional File"
                          type="file"
                          placeholder=""
                          value=""
                        />
                      </CCol>
                    </CRow>
                  </div>

                  <div class="mb-3">
                    <CRow className="">
                      <CCol md={4}>
                        <label for="category" class="form-label">
                          Category for Innovation <code>*</code>
                        </label>
                        {category.map(function (item) {
                          return (
                            <>
                              <Category
                                i={item.i}
                                value={demo.title ? item : {}}
                              />
                            </>
                          );
                        })}
                      </CCol>

                      <CCol md={8}></CCol>
                    </CRow>
                  </div>

                  <div class="mb-3">
                    <CButton
                      color="secondary"
                      className="text-white btn-sm"
                      onClick={handleAddCategory}
                    >
                      + More Category
                    </CButton>
                  </div>

                  <div class="mb-3">
                    <CRow className="">
                      <CCol md={4}>
                        <label for="status" class="form-label">
                          Status
                        </label>
                        <div class="form-check form-switch">
                          <input
                            class="form-check-input"
                            type="checkbox"
                            role="switch"
                            id="status"
                            onClick={handleSetStatus}
                          />
                          <label class="form-check-label" for="status">
                            {status}
                          </label>
                        </div>
                      </CCol>

                      <CCol md={8}></CCol>
                    </CRow>
                  </div>

                  {phase.map(function (item) {
                    return (
                      <>
                        <Phase i={item.i} value={demo.title ? item : {}} />
                      </>
                    );
                  })}

                  <div class="mb-3">
                    <CButton
                      color="secondary"
                      className="text-white btn-sm"
                      onClick={handleAddPhase}
                    >
                      + More Phase
                    </CButton>
                  </div>

                  <div className="" style={{ textAlign: "center" }}>
                    <CButton
                      onClick={() => setMod(!mod)}
                      style={{ width: "90px" }}
                      className={
                        "bg-warning text-dark border-0 mt-4 btn-md me-2"
                      }
                    >
                      {submitText}
                    </CButton>
                    <Modal
                      pesan="Are you sure to Submit?"
                      input={false}
                      callBack={handleSubmit}
                      close={() => setMod(false)}
                      visible={mod}
                    />
                  </div>
                </CForm>
              </div>
            </div>
          </div>
        </div>
      </CContainer>
    </div>
  );
};

export default EventNew;
