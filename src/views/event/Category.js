import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import Input from "src/components/Input";

const Category = (props) => {
  const catId = "category" + props.i;
  const placeholder = "Category " + props.i;

  const handleChangePhaseName = (event) => {
    // setPhaseName(event.target[1].value);
    console.log("Phase " + props.i + " - " + event.target.value);
  };

  return (
    <div className="mb-2">
      <Input
        id="category-name"
        label=""
        type="text"
        placeholder={"Category " + props.i}
        value={props.value ? props.value.name : ""}
        nolabel
      />
      <input
        type="text"
        id="category-id"
        value={props.value ? props.value.id : ""}
        hidden
      />
    </div>
  );
};

export default Category;
