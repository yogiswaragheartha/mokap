import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import Input from "src/components/Input";
import Moment from "moment";

const Phase = (props) => {
  return (
    <div class="mb-3 p-3" style={{ backgroundColor: "#F6F5FF" }}>
      <h6 className="text-center mb-3">Phase {props.i}</h6>
      <div class="mb-3">
        <CRow className="">
          <CCol md={4}>
            <Input
              id={"phase-name"}
              label="Phase Name"
              type="text"
              placeholder={"Name of Phase " + props.i}
              value={props.value.name}
              required
            />
            <input id={"phase-id"} type="text" value={props.value.id} hidden />
          </CCol>
          <CCol md={2}>
            <Input
              id={"phase-initial"}
              label="Initial"
              type="text"
              placeholder={"Initial of Phase"}
              value={props.value.initial}
              required
            />
          </CCol>
          <CCol md={3}>
            <Input
              id={"phase-start"}
              label="Start"
              type="date"
              placeholder=""
              value={Moment(props.value.timelineStart).format("YYYY-MM-DD")}
              required
            />
          </CCol>
          <CCol md={3}>
            <Input
              id={"phase-end"}
              label="End"
              type="date"
              placeholder=""
              value={Moment(props.value.timelineEnd).format("YYYY-MM-DD")}
              required
            />
          </CCol>
        </CRow>
      </div>
      <div class="mb-3">
        <Input
          id={"phase-description"}
          label="Phase Description"
          type="text-area"
          placeholder={"Describe the phase " + props.i}
          value={props.value.description}
          required
        />
      </div>
    </div>
  );
};

export default Phase;
