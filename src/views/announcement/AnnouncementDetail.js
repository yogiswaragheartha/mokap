import React, { useState, useEffect } from "react";
import { CButton, CCol, CContainer, CRow } from "@coreui/react";
import { Link, useParams } from "react-router-dom";
import { cilSave, cilDescription } from "@coreui/icons";
import { getAnnouncement } from "src/api/event";
import { useSelector } from "react-redux";
import CIcon from "@coreui/icons-react";
import Banner from "../home/Banner";
import Moment from "moment";
import LoadingData from "src/components/LoadingData";

const AnnouncementDetail = () => {
  const token = localStorage.getItem("token_jwt");
  const { id } = useParams();
  const [announcement, setAnnouncement] = useState([]);
  const [isLoadingData, setIsLoadingData] = useState(true);

  const { auth } = useSelector((state) => state);

  const get_announcement = async (token, id) => {
    try {
      const response = await getAnnouncement(token, id);

      console.log("Announcement", response.data);
      setAnnouncement(response.data || []);
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  useEffect(() => {
    get_announcement(token, id);
  }, []);

  return (
    <div className="bg-light mt-4 align-items-center">
      <CContainer>
        <CRow>
          <CCol md={6}>
            <h3>Announcement</h3>
          </CCol>
          <CCol md={6}>
            {auth.access.includes("createAnnouncement") ? (
              <Link to={"/announcement/edit/" + id}>
                <CButton
                  color="primary"
                  className="text-white"
                  style={{ width: "180px", float: "right" }}
                >
                  Edit announcement
                </CButton>
              </Link>
            ) : (
              ""
            )}
          </CCol>
        </CRow>
        {isLoadingData ? (
          <LoadingData />
        ) : (
          <>
            <div>
              <img
                orientation="left"
                src={announcement.banner}
                onClick={() => window.open(announcement.banner)}
                className="rounded-lg my-4 "
                style={{
                  width: "100%",
                  maxHeight: "340px",
                  objectFit: "cover",
                  borderRadius: "20px",
                }}
              />
            </div>
            <div className="my-4">
              <h4 className="mb-2">{announcement.title}</h4>
              <br />
              <div
                dangerouslySetInnerHTML={{ __html: announcement.description }}
              />
            </div>
            <div className="my-4">
              <h4>Timeline</h4>
              <p>
                {Moment(announcement.periodStart).format("DD MMM YYYY")} to{" "}
                {Moment(announcement.periodEnd).format("DD MMM YYYY")}
              </p>
            </div>
            <div className="my-4">
              <h4>File</h4>
              <p>
                {!announcement.otherFile ? (
                  "-"
                ) : (
                  <button
                    className="btn btn-md bg-dark text-white"
                    onClick={() => window.open(announcement.otherFile)}
                  >
                    Open file
                  </button>
                )}
              </p>
            </div>
          </>
        )}
      </CContainer>
    </div>
  );
};

export default AnnouncementDetail;
