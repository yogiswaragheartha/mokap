import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import { Link } from "react-router-dom";
import { cilSave, cilDescription } from "@coreui/icons";
import { useSelector } from "react-redux";
import { getAnnouncement } from "src/api/event";
import CIcon from "@coreui/icons-react";
import Banner from "../home/Banner";
import LoadingData from "src/components/LoadingData";

const AnnouncementList = () => {
  const token = localStorage.getItem("token_jwt");

  const [announcement, setAnnouncement] = useState([]);
  const { auth } = useSelector((state) => state);
  const [isLoadingData, setIsLoadingData] = useState(true);

  const get_announcement = async (token) => {
    try {
      const response = await getAnnouncement(token);

      console.log("Announcement", response.data);
      setAnnouncement(response.data || []);
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  useEffect(() => {
    get_announcement(token);
  }, []);

  return (
    <div className="bg-light mt-4 align-items-center">
      <CContainer>
        <h3>Announcement List</h3>
        <CRow className="my-4">
          <CCol></CCol>
          {isLoadingData ? (
            <LoadingData />
          ) : (
            <CCol md={6}>
              {!announcement
                ? ""
                : announcement.map(function (item, i) {
                    return (
                      <div className="mb-4">
                        <Banner
                          item={item.banner}
                          link={
                            "/announcement/detail/" + item.id + "/" + item.slug
                          }
                          title={item.title}
                        />
                      </div>
                    );
                  })}

              {auth.access.includes("createAnnouncement") ? (
                <Link
                  to={"/announcement/new"}
                  className="d-flex justify-content-center"
                >
                  <CButton color="primary" className="text-white">
                    + Create Announcement
                  </CButton>
                </Link>
              ) : (
                ""
              )}
            </CCol>
          )}

          <CCol></CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default AnnouncementList;
