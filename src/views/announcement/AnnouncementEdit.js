import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import axios from "axios";
import { Link, useNavigate, useParams, useLocation } from "react-router-dom";
import Input from "src/components/Input";
import {
  createAnnouncement,
  getAnnouncement,
  updateAnnouncement,
  deleteAnnouncement,
} from "src/api/event";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import Moment from "moment";
import LoadingContent from "src/components/LoadingContent";
import InputRichText from "src/components/InputRichText";

const AnnouncementEdit = () => {
  const token = localStorage.getItem("token_jwt");
  const { id } = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  const [desc, setDesc] = useState("");
  const [announcement, setAnnouncement] = useState([]);
  const [edit, setEdit] = useState([]);
  const [mod, setMod] = useState(false);
  const [modDelete, setModDelete] = useState(false);
  const [submitText, setSubmitText] = useState("Post");
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  const cekTanggal = (start, end) => {
    if (end < start) {
      return false;
    }
  };

  const handleChangeDescription = (e) => {
    setDesc(e);
  };

  const handleDelete = async (event) => {
    event.preventDefault();
    setIsLoading(true);
    setModDelete(false);
    try {
      const response = await deleteAnnouncement(token, id);
      if (response.data.message) {
        navigate("/announcement/list", {
          state: { visible: true },
        });
      }
      setIsLoading(false);
    } catch (err) {
      console.log("ERROR: ", err);
      setIsLoading(false);
    }
  };

  const handleSubmit = async () => {
    const title = document.getElementById("title").value;
    // const description = document.getElementById("description").value;
    const periodStart = document.getElementById("start").value;
    const periodEnd = document.getElementById("end").value;
    const banner = document.getElementById("banner").files[0];
    const other = document.getElementById("additional").files[0];
    const cekBanner = document.getElementById("cek-banner").value;
    const description = desc;

    setMod(false);

    if (
      !title ||
      !description ||
      !description ||
      !periodStart ||
      !periodEnd ||
      !cekBanner
    ) {
      setModal({
        visible: true,
        message: "Fill in the form corectly",
        status: "warning",
      });
      setSubmitText("Post");
      setMod(false);
      return;
    }

    //cek tanggal
    if (cekTanggal(periodStart, periodEnd) == false) {
      setMod(false);
      setModal({
        visible: true,
        message: "End date of announcement must be higher than start date",
        status: "warning",
      });
      setSubmitText("Post");
      return;
    }

    const data = {
      title,
      description,
      periodStart,
      periodEnd,
      banner,
      other,
    };
    console.log("SUBMIT", data);
    setIsLoading(true);
    try {
      if (!id) {
        const response = await createAnnouncement(token, data);
        console.log("Create Announcement:", response.data.message);
        if (response.data.message) {
          navigate("/announcement/list", {
            state: { visible: true },
          });
        }
      } else {
        const response = await updateAnnouncement(token, data, id);
        console.log("Update Announcement:", response.data.message);
        if (response.data.message) {
          navigate("/announcement/list", {
            state: { visible: true },
          });
        }
      }

      setSubmitText("Post");
      setIsLoading(false);
    } catch (err) {
      console.log("ERROR: ", err);
      setSubmitText("Post");
      setIsLoading(false);
    }
  };

  const get_announcement = async (token, id) => {
    try {
      if (!id) {
        const response = await getAnnouncement(token);
        setAnnouncement(response.data);
      } else {
        const response = await getAnnouncement(token, id);
        setEdit(response.data);
        setDesc(response.data.description);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (!id) {
      get_announcement(token);
    } else {
      get_announcement(token, id);
    }
  }, []);

  return (
    <div className="bg-light my-4 align-items-center">
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <CContainer>
        <h3 className="mb-5">{id ? "Edit" : "Create"} Announcement</h3>
        <CRow>
          <CCol md={1}></CCol>

          <CCol md={10}>
            <CCard className="border-0 shadow p-4">
              <>
                <Input
                  id="title"
                  label="Title"
                  type="text"
                  placeholder="Name of the announcement"
                  value={edit.title}
                  required
                />
                <InputRichText
                  id="description"
                  label="Announcement Description"
                  type="RichTextEditor"
                  placeholder="Describe the announcement"
                  value={edit.description}
                  callBack={handleChangeDescription}
                  required
                />
                <CRow>
                  <CCol>
                    <Input
                      id="start"
                      label="Start"
                      type="date"
                      placeholder=""
                      value={Moment(edit.periodStart).format("YYYY-MM-DD")}
                      required
                    />
                  </CCol>
                  <CCol>
                    <Input
                      id="end"
                      label="End"
                      type="date"
                      placeholder=""
                      value={Moment(edit.periodEnd).format("YYYY-MM-DD")}
                      required
                    />
                  </CCol>
                </CRow>
                <CRow>
                  <CCol md={6}>
                    <Input
                      id="banner"
                      label="Announcement Banner"
                      type="file"
                      placeholder="Image of your announcement"
                      value={edit.banner}
                      required
                    />
                    <p className="text-muted">
                      <i>
                        (set banner resolution to 1320x450 pixels for best
                        results)
                      </i>
                    </p>
                    <Input
                      id="additional"
                      label="Additional File"
                      type="file"
                      placeholder="Pitchdeck template or any template"
                      value={edit.otherFile}
                    />
                  </CCol>
                  <CCol></CCol>
                </CRow>

                <div className="text-center ">
                  <CButton
                    onClick={() => setMod(!mod)}
                    style={{ width: "130px" }}
                    className={"bg-primary border-0 mt-4 btn-md mx-2"}
                  >
                    {submitText}
                  </CButton>
                  <Modal
                    pesan="Are you sure to Submit?"
                    input={false}
                    callBack={handleSubmit}
                    close={() => setMod(false)}
                    visible={mod}
                  />

                  {!id ? (
                    ""
                  ) : (
                    <>
                      <CButton
                        onClick={() => setModDelete(!modDelete)}
                        style={{ width: "130px" }}
                        className={"bg-danger border-0 mt-4 btn-md mx-2"}
                      >
                        Delete
                      </CButton>
                      <Modal
                        pesan="Are you sure to Delete?"
                        input={false}
                        callBack={handleDelete}
                        close={() => setModDelete(false)}
                        visible={modDelete}
                      />
                    </>
                  )}
                </div>
              </>
            </CCard>
          </CCol>

          <CCol md={1}></CCol>
        </CRow>

        {id ? (
          ""
        ) : (
          <>
            <h3 className="text-center my-5">Announcement History</h3>
            <div className="table-responsive">
              <table class="table table-striped ">
                <thead className="text-center">
                  <tr>
                    <th scope="col">Created</th>
                    <th scope="col">Announcement Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Schedule</th>
                  </tr>
                </thead>
                <tbody>
                  {!announcement
                    ? ""
                    : announcement.map(function (item, i) {
                        return (
                          <tr>
                            <td>
                              {Moment(item.createdAt).format("DD MMM YYYY")}
                            </td>
                            <td>{item.title}</td>
                            <td>
                              {item.description.replace(/<[^>]+>/g, "")
                                .length <= 100
                                ? item.description.replace(/<[^>]+>/g, "")
                                : item.description
                                    .replace(/<[^>]+>/g, "")
                                    .substr(0, 120) + "..."}
                            </td>
                            <td>
                              {Moment(item.periodStart).format("DD MMM YYYY")} -{" "}
                              {Moment(item.periodEnd).format("DD MMM YYYY")}
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </div>
          </>
        )}
      </CContainer>
    </div>
  );
};

export default AnnouncementEdit;
