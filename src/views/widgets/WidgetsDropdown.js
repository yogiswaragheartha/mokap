import React from "react";
import { CRow, CCol, CWidgetStatsA } from "@coreui/react";
import { useNavigate } from "react-router-dom";

const WidgetsDropdown = (props) => {
  const navigate = useNavigate();

  return (
    <CRow sm={{ cols: 2 }} md={{ cols: 3 }} xl={{ cols: 5 }}>
      <CCol>
        <CWidgetStatsA
          className="mb-4"
          color="primary"
          style={{ minHeight: "116px" }}
          value={
            <>
              <h5>Total Innovation</h5>
              <p class="my-1" style={{ fontSize: "30px" }}>
                {props?.totInovasi || "-"}
              </p>
            </>
          }
          onClick={props.onClick}
        />
      </CCol>
      <CCol>
        <CWidgetStatsA
          className="mb-4"
          color="info"
          style={{ minHeight: "116px" }}
          value={
            <>
              <h5>Average Progress</h5>
              <p class="my-1" style={{ fontSize: "30px" }}>
                {props?.progInovasi || "-"}%
              </p>
            </>
          }
        />
      </CCol>
      <CCol>
        <CWidgetStatsA
          className="mb-4"
          color="danger"
          style={{ minHeight: "116px" }}
          value={
            <>
              <h5>Total Expenses</h5>
              <p class="my-3" style={{ fontSize: "19px" }}>
                IDR {Intl.NumberFormat().format(props.totPengeluaran)}
              </p>
            </>
          }
        />
      </CCol>
      <CCol>
        <CWidgetStatsA
          className="mb-4"
          color="warning"
          style={{ minHeight: "116px" }}
          value={
            <>
              <h5>Total Revenue</h5>
              <p class="my-3" style={{ fontSize: "19px" }}>
                IDR {Intl.NumberFormat().format(props.totPendapatan)}
              </p>
            </>
          }
        />
      </CCol>
      <CCol>
        <CWidgetStatsA
          className="mb-4"
          color="success"
          style={{ minHeight: "116px" }}
          value={
            <>
              <h5>Cost Effectiveness</h5>
              <p class="my-3" style={{ fontSize: "19px" }}>
                IDR {Intl.NumberFormat().format(props.costEffectiveness)}
              </p>
            </>
          }
        />
      </CCol>
    </CRow>
  );
};

export default WidgetsDropdown;
