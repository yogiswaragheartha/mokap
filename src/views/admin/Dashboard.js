import React from "react";

import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from "@coreui/react";
import { CChartBar, CChartPie } from "@coreui/react-chartjs";

import WidgetsDropdown from "../widgets/WidgetsDropdown";
import overview_fase from "src/data/overview_fase";
import sebaran_inovasi from "src/data/sebaran_inovasi";
import top5 from "src/data/top5";

const Dashboard = () => {
  const totInovasi = 1352;
  const growth = 20;
  const progInovasi = 75;
  const totPengeluaran = 400000000;
  const szPengeluaran = 80;
  const totPendapatan = 200000000;
  const szPendapatan = 85;

  return (
    <>
      <h1>(Konten Admin)</h1>
      <br />
      <WidgetsDropdown
        totInovasi={totInovasi.toLocaleString("en-US")}
        growth={growth}
        progInovasi={progInovasi}
        totPengeluaran={totPengeluaran.toLocaleString("en-US")}
        szPengeluaran={szPengeluaran}
        totPendapatan={totPendapatan.toLocaleString("en-US")}
        szPendapatan={szPendapatan}
      />
      <CRow>
        <CCol xs={6}>
          <CCard className="mb-4">
            <CCardHeader>Trend Pengeluaran</CCardHeader>
            <CCardBody>
              <CChartBar
                data={{
                  labels: overview_fase.map(function (item) {
                    return item.fase;
                  }),
                  datasets: [
                    {
                      label: "Total Estimasi Pengeluaran",
                      backgroundColor: "#7979f8",
                      data: overview_fase.map(function (item) {
                        return item.pengeluaran.estimasi;
                      }),
                    },
                    {
                      label: "Total Pengeluaran",
                      backgroundColor: "#f87979",
                      data: overview_fase.map(function (item) {
                        return item.pengeluaran.real;
                      }),
                    },
                  ],
                }}
                labels="months"
              />
            </CCardBody>
          </CCard>
        </CCol>
        <CCol xs={6}>
          <CCard className="mb-4">
            <CCardHeader>Trend Pendapatan</CCardHeader>
            <CCardBody>
              <CChartBar
                data={{
                  labels: overview_fase.map(function (item) {
                    return item.fase;
                  }),
                  datasets: [
                    {
                      label: "Total Estimas Pendapatan",
                      backgroundColor: "#7979f8",
                      data: overview_fase.map(function (item) {
                        return item.pendapatan.estimasi;
                      }),
                    },
                    {
                      label: "Total Pendapatan",
                      backgroundColor: "#f87979",
                      data: overview_fase.map(function (item) {
                        return item.pendapatan.real;
                      }),
                    },
                  ],
                }}
                labels="months"
              />
            </CCardBody>
          </CCard>
        </CCol>
        <CCol xs={6}>
          <CCard className="mb-4">
            <CCardHeader>Sebaran inovasi</CCardHeader>
            <CCardBody>
              <CChartPie
                data={{
                  labels: sebaran_inovasi.map(function (item) {
                    return item.kelompok;
                  }),
                  datasets: [
                    {
                      data: sebaran_inovasi.map(function (item) {
                        return item.jlh_inovasi;
                      }),
                      backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        "#8463FF",
                        "#52489C",
                        "#00798C",
                        "#E63946",
                      ],
                      hoverBackgroundColor: ["#222"],
                    },
                  ],
                }}
              />
            </CCardBody>
          </CCard>
        </CCol>
        <CCol xs={6}>
          <CCard className="mb-4">
            <CCardHeader>Top 5 progres inovasi</CCardHeader>
            <CTable hover>
              <CTableHead>
                <CTableRow>
                  <CTableHeaderCell scope="col">No</CTableHeaderCell>
                  <CTableHeaderCell scope="col">Team Name</CTableHeaderCell>
                  <CTableHeaderCell scope="col">% Progress</CTableHeaderCell>
                </CTableRow>
              </CTableHead>
              <CTableBody>
                {top5.map(function (item, i) {
                  return (
                    <CTableRow>
                      <CTableHeaderCell scope="row">{i + 1}</CTableHeaderCell>
                      <CTableDataCell>{item.name}</CTableDataCell>
                      <CTableDataCell>{item.progress}</CTableDataCell>
                    </CTableRow>
                  );
                })}
              </CTableBody>
            </CTable>
          </CCard>
        </CCol>
      </CRow>
    </>
  );
};

export default Dashboard;
