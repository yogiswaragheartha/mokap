import React, { useState, useEffect } from "react";
import { Navigate, useLocation, useNavigate } from "react-router-dom";
import { CCol, CContainer, CRow } from "@coreui/react";
import { cilSave } from "@coreui/icons";
import { getAllUser, getAreas, getRoles, updateUserData } from "src/api/user";
import { useSelector } from "react-redux";
import CIcon from "@coreui/icons-react";
import Input from "src/components/Input";
import LoadingData from "src/components/LoadingData";
import LoadingContent from "src/components/LoadingContent";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import UserRole from "./UserRole";
import NewRole from "./NewRole";

const UserList = () => {
  const token = localStorage.getItem("token_jwt");
  const { auth } = useSelector((state) => state);
  const navigate = useNavigate();
  const location = useLocation();
  const [user, setUser] = useState([]);
  const [listUser, setListUser] = useState([]);
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [mod, setMod] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [itemRow, setItemRow] = useState();
  const [modSubmit, setModSubmit] = useState(false);
  const [userId, setUserId] = useState();
  const [roles, setRoles] = useState([]);
  const [kontenRole, setKontenRole] = useState();
  const [listRole, setListRole] = useState([]);
  const [listArea, setListArea] = useState([]);
  const [activeTab, setActiveTab] = useState();

  const get_user = async (token) => {
    try {
      const response = await getAllUser(token);

      // console.log("Users", response.data);
      setUser(response.data.sort((a, b) => (a.id > b.id ? -1 : 1)) || []);
      setListUser(response.data.sort((a, b) => (a.id > b.id ? -1 : 1)) || []);
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };
  const getRole = async () => {
    try {
      const response = await getRoles();

      const arrRole = [];
      response.data.map(function (item, i) {
        arrRole.push({ text: item.name, value: item.id });
      });
      setListRole(arrRole);
      setRoles(response.data);
      setKontenRole(<UserRole role={response.data} />);
    } catch (error) {
      console.log(error);
    }
  };

  const getArea = async () => {
    try {
      const response = await getAreas();

      const arrArea = [];
      response.data.map(function (item, i) {
        arrArea.push({ text: item.name, value: item.id });
      });
      setListArea(arrArea);
    } catch (error) {
      console.log(error);
    }
  };

  const changeTab = (num) => {
    setActiveTab(num);
  };

  const updateKontenSearch = () => {
    const search = document.getElementById("filter-search").value;

    const filtered = listUser.filter(function (obj) {
      if (obj["fullName"]) {
        return obj["fullName"].toLowerCase().includes(search.toLowerCase());
      }
    });

    setUser(filtered);
  };

  const handleSave = (id, item) => {
    setModSubmit(false);
    const role = document.getElementById("role" + id).value;
    const area = document.getElementById("area" + id).value;
    console.log(role, area);
    update_user(token, id, role, area, item);
  };

  const update_user = async (token, id, role, area, item) => {
    setIsLoading(true);
    const email = item.email;
    const name = item.fullName;
    try {
      const response = await updateUserData(token, id, role, area);
      console.log("Update User:", response.data);
      if (response.data.message == "Successfully update profile") {
        setMod(true);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    get_user(token);
    getArea();
    getRole();

    const userList = auth.access.includes("userList");
    const userRole = auth.access.includes("userRole");
    if (userList || (!userRole && userList)) {
      setActiveTab(1);
    } else if ((!userList && userRole) || userRole) {
      setActiveTab(2);
    }

    if (location.state) {
      setActiveTab(location.state.activeTab);
      if (location.state.status == "success") {
        setMod(true);
      }
    }
  }, []);

  return (
    <div className="bg-light mt-4 align-items-center">
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <ModalAlert
        visible={mod}
        status="success"
        message=""
        callBack={() => setMod(false)}
      />

      <CContainer>
        <h3>List of User</h3>
        <CRow>
          <CCol>
            <div className="btn-group mt-4">
              <font
                class="clickable btn btn-primary"
                onClick={() => changeTab(1)}
                style={{
                  borderTopLeftRadius: "50px",
                  borderTopRightRadius: "50px",
                  width: "120px",
                  opacity: activeTab == 1 ? "1" : "0.5",
                }}
                hidden={auth.access.includes("userList") ? false : true}
              >
                User List
              </font>

              <font
                class="clickable btn btn-primary"
                onClick={() => changeTab(2)}
                style={{
                  borderTopLeftRadius: "50px",
                  borderTopRightRadius: "50px",
                  width: "120px",
                  opacity: activeTab == 2 || activeTab == 3 ? "1" : "0.5",
                }}
                hidden={auth.access.includes("userRole") ? false : true}
              >
                User Role
              </font>
            </div>
            {activeTab == 2 ? (
              <button
                className="btn btn-sm bg-primary text-white mt-4 ms-5"
                onClick={() => navigate("/user/role/new")}
              >
                + Create New Role
              </button>
            ) : (
              ""
            )}
          </CCol>
          <CCol md={6} className="d-flex justify-content-end">
            {activeTab == 1 ? (
              <Input
                type="text"
                id="filter-search"
                placeholder="Search"
                value=""
                callBack={updateKontenSearch}
              />
            ) : (
              ""
            )}
          </CCol>
        </CRow>
        {isLoadingData ? (
          <LoadingData />
        ) : activeTab == 2 && auth.access.includes("userRole") ? (
          kontenRole
        ) : activeTab == 1 && auth.access.includes("userList") ? (
          <div className="my-4 table-responsive">
            <table class="table table-striped ">
              <thead className="text-center">
                <tr>
                  <th scope="col">Photo</th>
                  <th scope="col">Name</th>
                  <th scope="col">Role</th>
                  <th scope="col">Area</th>
                  <th scope="col">Email</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                {!user
                  ? ""
                  : user.map(function (item, i) {
                      return (
                        <tr className="align-middle">
                          <td>
                            {item.avatar ? (
                              <img
                                className="inovasi"
                                src={item.avatar}
                                style={{ objectFit: "cover" }}
                              />
                            ) : (
                              <div
                                className="inovasi bg-primary text-center pt-2 text-white "
                                style={{
                                  objectFit: "cover",
                                  fontSize: "28px",
                                }}
                              >
                                {item.title ? item.title.substr(0, 1) : ""}
                              </div>
                            )}
                          </td>
                          <td>{item.fullName}</td>
                          <td>
                            <Input
                              type="select"
                              id={"role" + item.id}
                              style={{ maxWidth: "200px" }}
                              value={item.roleId ? item.roleId : ""}
                              opsi={listRole}
                            />
                          </td>
                          <td className="text-center">
                            <Input
                              type="select"
                              id={"area" + item.id}
                              style={{ maxWidth: "200px" }}
                              value={item.areaId ? item.areaId : ""}
                              opsi={listArea}
                            />
                          </td>
                          <td className="text-center">{item.email}</td>
                          <td>
                            <div
                              className="btn"
                              onClick={() => {
                                setUserId(item.id);
                                setItemRow(item);
                                setModSubmit(true);
                              }}
                            >
                              <CIcon icon={cilSave} size="md" /> Save Change
                            </div>
                          </td>
                        </tr>
                      );
                    })}
                <Modal
                  pesan={"Are you sure to change the data?"}
                  input={false}
                  callBack={() => handleSave(userId, itemRow)}
                  close={() => setModSubmit(false)}
                  visible={modSubmit}
                />
              </tbody>
            </table>
          </div>
        ) : (
          ""
        )}
      </CContainer>
    </div>
  );
};

export default UserList;
