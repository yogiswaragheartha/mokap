import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { CCol, CContainer, CRow, CButton } from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilSave } from "@coreui/icons";
import Moment from "moment";
import { createRole } from "src/api/user";
import Modal from "src/components/Modal";
import Input from "src/components/Input";

const NewRole = (props) => {
  const token = localStorage.getItem("token_jwt");
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [mod, setMod] = useState(false);
  const navigate = useNavigate();

  const handleSubmit = () => {
    // console.log(name, description);
    if (!name) {
      setMod(false);
      alert("Fill the form correctly");

      return;
    }
    newRole({ name, description });
  };

  const newRole = async (data) => {
    props.callModal(true);
    try {
      const response = await createRole(token, data);

      console.log("New Role", response.data);
      setMod(false);
      props.callBack(2);
      props.callModal(false);
      props.callRole();
    } catch (error) {
      console.log(error);
    }
    setMod(false);
    props.callBack(2);
    props.callModal(false);
  };

  useEffect(() => {}, []);

  return (
    <div className="my-4 mb-5 ">
      <CRow>
        <CCol lg={6}>
          <div className="p-4 border" style={{ borderRadius: "10px" }}>
            <h4>New Role</h4>
            <Input
              type="text"
              label="Role Name"
              placeholder="Name of the Role"
              callBack={(e) => setName(e.target.value)}
              required
            />
            <Input
              type="text-area"
              label="Role Descripton"
              placeholder="Describe the Role"
              callBack={(e) => setDescription(e.target.value)}
            />
            <div className="" style={{ textAlign: "center" }}>
              <CButton
                onClick={() => setMod(!mod)}
                style={{ width: "90px" }}
                className={"bg-primary border-0 mt-4 btn-md me-2"}
              >
                Submit
              </CButton>
              <Modal
                pesan="Are you sure to Submit?"
                input={false}
                callBack={handleSubmit}
                close={() => setMod(false)}
                visible={mod}
              />

              <CButton
                onClick={() => props.callBack(2)}
                style={{ width: "90px" }}
                className={"bg-warning  border-0 mt-4 btn-md me-2"}
              >
                Cancel
              </CButton>
            </div>
          </div>
        </CCol>
      </CRow>
    </div>
  );
};

export default NewRole;
