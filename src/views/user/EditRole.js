import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { CCol, CContainer, CRow, CButton } from "@coreui/react";
import { cilSave } from "@coreui/icons";
import { getRoles, editRole, createRole } from "src/api/user";
import CIcon from "@coreui/icons-react";
import Input from "src/components/Input";
import LoadingData from "src/components/LoadingData";
import LoadingContent from "src/components/LoadingContent";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import UserRole from "./UserRole";
import NewRole from "./NewRole";
import {
  eventEventArr,
  eventDocumentArr,
  eventBookingArr,
  innovationArr,
  announcementArr,
  dashboardArr,
  userManagementArr,
} from "./dataAkses";

const EditRole = () => {
  const token = localStorage.getItem("token_jwt");
  const navigate = useNavigate();
  const { id } = useParams();
  const [renameForm, setRenameForm] = useState(false);
  const [name, setName] = useState();
  const [description, setDescription] = useState();
  const [user, setUser] = useState([]);
  const [listRole, setListRole] = useState([]);
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [mod, setMod] = useState(false);
  const [modAlert, setModAlert] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [modSubmit, setModSubmit] = useState(false);
  const [roles, setRoles] = useState([]);

  const getRole = async () => {
    setIsLoadingData(true);
    try {
      const response = await getRoles();
      console.log(response.data);
      const arrRole = [];
      response.data.map(function (item, i) {
        arrRole.push({ text: item.name, value: item.id });
      });
      setListRole(arrRole);

      if (id != "new") {
        const selectedRole = response.data.filter((c) => c.id == id)[0];

        if (selectedRole.access != null) {
          selectedRole["access"] = JSON.parse(selectedRole.access);
        }
        setRoles(selectedRole);
        console.log(selectedRole);
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  // const updateKontenSearch = () => {
  //   const search = document.getElementById("filter-search").value;

  //   const filtered = listUser.filter(function (obj) {
  //     if (obj["fullName"]) {
  //       return obj["fullName"].toLowerCase().includes(search.toLowerCase());
  //     }
  //   });

  //   setUser(filtered);
  // };
  const getRoleAccess = () => {
    const eventEvent = [];
    for (let i = 0; i < eventEventArr.length; i++) {
      if (document.getElementById("event" + i).checked) {
        eventEvent.push(document.getElementById("event" + i).value);
      }
    }

    const eventDocument = [];
    for (let i = 0; i < eventDocumentArr.length; i++) {
      if (document.getElementById("document" + i).checked) {
        eventDocument.push(document.getElementById("document" + i).value);
      }
    }

    const eventBooking = [];
    for (let i = 0; i < eventBookingArr.length; i++) {
      if (document.getElementById("booking" + i).checked) {
        eventBooking.push(document.getElementById("booking" + i).value);
      }
    }

    const innovation = [];
    for (let i = 0; i < announcementArr.length; i++) {
      if (document.getElementById("innovation" + i).checked) {
        innovation.push(document.getElementById("innovation" + i).value);
      }
    }

    const announcement = [];
    for (let i = 0; i < announcementArr.length; i++) {
      if (document.getElementById("announcement" + i).checked) {
        announcement.push(document.getElementById("announcement" + i).value);
      }
    }

    const dashboard = [];
    for (let i = 0; i < dashboardArr.length; i++) {
      if (document.getElementById("dashboard" + i).checked) {
        dashboard.push(document.getElementById("dashboard" + i).value);
      }
    }

    const userManagement = [];
    for (let i = 0; i < userManagementArr.length; i++) {
      if (document.getElementById("user" + i).checked) {
        userManagement.push(document.getElementById("user" + i).value);
      }
    }

    const data = {
      eventEvent,
      eventDocument,
      eventBooking,
      innovation,
      announcement,
      dashboard,
      userManagement,
    };
    // console.log("User Access", data);
    return data;
  };

  const handleSubmit = () => {
    setMod(false);
    const access = getRoleAccess();
    const data = { name, description, access };
    console.log("Edit Role", data);
    if (id == "new") {
      newRole(data);
    } else {
      updateRole(data);
    }
  };

  const newRole = async (data) => {
    setIsLoadingData(true);
    try {
      const response = await createRole(token, data);
      console.log(response.data);
      setRenameForm(false);
      navigate("/user", {
        state: { activeTab: 2, status: "success" },
      });
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  const updateRole = async (data) => {
    setIsLoadingData(true);
    try {
      const response = await editRole(token, id, data);
      console.log(response.data);
      setRenameForm(false);
      navigate("/user", {
        state: { activeTab: 2, status: "success" },
      });
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  useEffect(() => {
    getRole();
  }, [id]);

  return (
    <div className="bg-light mt-4 align-items-center">
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <ModalAlert
        visible={modAlert}
        status="success"
        message=""
        callBack={() => setModAlert(false)}
      />

      <CContainer>
        <h3>User Role</h3>

        {isLoadingData ? (
          <LoadingData />
        ) : (
          <div className="my-4 p-4 shadow ">
            {!renameForm && id != "new" ? (
              <div className="d-flex">
                <Input
                  type="select"
                  label={<b>Role Name</b>}
                  id="role"
                  style={{ maxWidth: "250px" }}
                  value={id}
                  opsi={listRole}
                  callBack={(e) => {
                    navigate("/user/role/" + e.target.value);
                  }}
                  required
                />
                <div className="mt-2 pt-1">
                  <button
                    className="btn btn-sm bg-primary text-white mt-4 ms-3 pt-1"
                    onClick={() => setRenameForm(!renameForm)}
                  >
                    <i className="bi-pencil-fill"></i> Rename Role
                  </button>
                </div>
              </div>
            ) : (
              <CRow>
                <CCol lg={6}>
                  <Input
                    type="text"
                    label={<b>Role Name</b>}
                    placeholder="Name of the Role"
                    value={roles.name}
                    callBack={(e) => setName(e.target.value)}
                    required
                  />
                  <Input
                    type="text-area"
                    label={<b>Description</b>}
                    placeholder="Describe the Role"
                    value={roles.description}
                    callBack={(e) => setDescription(e.target.value)}
                  />
                </CCol>
              </CRow>
            )}

            <div className="border-bottom my-4">
              <label className="form-label">
                <b>List of Access Menu</b>
                <code>*</code>
              </label>
            </div>

            <CRow className="my-2">
              <CCol sm={6} md={4} lg={3} className="my-2">
                <b>Event - Event</b>
                {eventEventArr.map(function (item, i) {
                  let isChecked = false;
                  if (roles.access) {
                    if (roles.access.eventEvent) {
                      isChecked = roles.access.eventEvent.find(
                        (c) => c == item.id
                      );
                    }
                  }

                  return (
                    <>
                      <Input
                        type="checkbox"
                        id={"event" + i}
                        label={item.name}
                        value={item.id}
                        checked={isChecked}
                      />
                    </>
                  );
                })}
              </CCol>
              <CCol sm={6} md={4} lg={3} className="my-2">
                <b>Event - Document</b>
                {eventDocumentArr.map(function (item, i) {
                  let isChecked = false;
                  if (roles.access) {
                    if (roles.access.eventDocument) {
                      isChecked = roles.access.eventDocument.find(
                        (c) => c == item.id
                      );
                    }
                  }
                  return (
                    <>
                      <Input
                        type="checkbox"
                        id={"document" + i}
                        label={item.name}
                        value={item.id}
                        checked={isChecked}
                      />
                    </>
                  );
                })}
              </CCol>
              <CCol sm={6} md={4} lg={3} className="my-2">
                <b>Event - Booking</b>

                {eventBookingArr.map(function (item, i) {
                  let isChecked = false;
                  if (roles.access) {
                    if (roles.access.eventBooking) {
                      isChecked = roles.access.eventBooking.find(
                        (c) => c == item.id
                      );
                    }
                  }
                  return (
                    <>
                      <Input
                        type="checkbox"
                        id={"booking" + i}
                        label={item.name}
                        value={item.id}
                        checked={isChecked}
                      />
                    </>
                  );
                })}
              </CCol>

              <CCol sm={6} md={4} lg={3} className="my-2">
                <b>Innovation</b>
                {innovationArr.map(function (item, i) {
                  let isChecked = false;
                  if (roles.access) {
                    if (roles.access.innovation) {
                      isChecked = roles.access.innovation.find(
                        (c) => c == item.id
                      );
                    }
                  }
                  return (
                    <>
                      <Input
                        type="checkbox"
                        id={"innovation" + i}
                        label={item.name}
                        value={item.id}
                        checked={isChecked}
                      />
                    </>
                  );
                })}
              </CCol>

              <CCol sm={6} md={4} lg={3} className="my-2">
                <b>Announcement</b>
                {announcementArr.map(function (item, i) {
                  let isChecked = false;
                  if (roles.access) {
                    if (roles.access.announcement) {
                      isChecked = roles.access.announcement.find(
                        (c) => c == item.id
                      );
                    }
                  }
                  return (
                    <>
                      <Input
                        type="checkbox"
                        id={"announcement" + i}
                        label={item.name}
                        value={item.id}
                        checked={isChecked}
                      />
                    </>
                  );
                })}
              </CCol>

              <CCol sm={6} md={4} lg={3} className="my-2">
                <b>Dashboard</b>
                {dashboardArr.map(function (item, i) {
                  let isChecked = false;
                  if (roles.access) {
                    if (roles.access.dashboard) {
                      isChecked = roles.access.dashboard.find(
                        (c) => c == item.id
                      );
                    }
                  }
                  return (
                    <>
                      <Input
                        type="checkbox"
                        id={"dashboard" + i}
                        label={item.name}
                        value={item.id}
                        checked={isChecked}
                      />
                    </>
                  );
                })}
              </CCol>
              <CCol sm={6} md={4} lg={3} className="my-2">
                <b>User Management</b>
                {userManagementArr.map(function (item, i) {
                  let isChecked = false;
                  if (roles.access) {
                    if (roles.access.userManagement) {
                      isChecked = roles.access.userManagement.find(
                        (c) => c == item.id
                      );
                    }
                  }
                  return (
                    <>
                      <Input
                        type="checkbox"
                        id={"user" + i}
                        label={item.name}
                        value={item.id}
                        checked={isChecked}
                      />
                    </>
                  );
                })}
              </CCol>
            </CRow>

            <div className="" style={{ textAlign: "center" }}>
              <CButton
                onClick={() => setMod(!mod)}
                style={{ width: "90px" }}
                className={"bg-primary border-0 mt-4 btn-md me-2"}
              >
                Submit
              </CButton>
              <Modal
                pesan={"Are you sure to Submit?"}
                input={false}
                callBack={handleSubmit}
                close={() => setMod(false)}
                visible={mod}
              />
            </div>
          </div>
        )}
      </CContainer>
    </div>
  );
};

export default EditRole;
