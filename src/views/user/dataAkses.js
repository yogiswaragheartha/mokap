const eventEventArr = [
  { id: "createEvent", name: "Create Event" },
  { id: "activeEvent", name: "Active Event" },
  { id: "inactiveEvent", name: "Inactive Event" },
];

const eventDocumentArr = [
  { id: "listOfSubmission", name: "List of Submission" },
  { id: "submission", name: "Submission" },
  { id: "changePhase", name: "Change Phase" },
  { id: "reviewProposal", name: "Review Proposal" },
];

const eventBookingArr = [
  { id: "appointment", name: "Appointment" },
  { id: "appointmentApproval", name: "Appointment Approval" },
];

const innovationArr = [
  { id: "innovationList", name: "Innovation List" },
  { id: "myInnovation", name: "My Innovation" },
];

const announcementArr = [
  { id: "createAnnouncement", name: "Create Announcement" },
  { id: "announcement", name: "Announcement" },
];

const dashboardArr = [
  { id: "filterFunction", name: "Filter Function" },
  { id: "performanceSummary", name: "Performance Summary" },
  { id: "ringkasanEvent", name: "Ringkasan Event" },
  { id: "expenseTrend", name: "Expense Trend" },
  { id: "revenueTrend", name: "Revenue Trend" },
  { id: "spreadOfInnovation", name: "Spread of Innovation" },
  { id: "top5Progress", name: "Top 5 Progress" },
];

const userManagementArr = [
  { id: "userList", name: "User List" },
  { id: "userRole", name: "User Role" },
];

export {
  eventEventArr,
  eventDocumentArr,
  eventBookingArr,
  innovationArr,
  announcementArr,
  dashboardArr,
  userManagementArr,
};
