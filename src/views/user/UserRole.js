import React, { useState, useEffect } from "react";
import { CCol, CContainer, CRow } from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilSave } from "@coreui/icons";
import Moment from "moment";
import { useNavigate } from "react-router-dom";

const UserRole = (props) => {
  const token = localStorage.getItem("token_jwt");
  const [role, setRole] = useState([]);
  const navigate = useNavigate();

  const handleEdit = (id) => {
    navigate("/user/role/" + id);
  };

  useEffect(() => {
    setRole(props.role);
  }, []);

  return (
    <div className="my-4 table-responsive">
      <table class="table table-striped ">
        <thead className="text-center">
          <tr>
            <th scope="col">Role Name</th>
            <th scope="col">Created Date</th>
            <th scope="col">Last Update</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {!role
            ? ""
            : role.map(function (item, i) {
                return (
                  <tr className="align-middle">
                    <td>{item.name}</td>

                    <td className="text-center">
                      {Moment(item.createdAt).format("DD MMM YYYY HH:mm")}
                    </td>
                    <td className="text-center">
                      {Moment(item.updatedAt).format("DD MMM YYYY HH:mm")}
                    </td>
                    <td>
                      <div
                        className="btn"
                        onClick={() => {
                          handleEdit(item.id);
                        }}
                      >
                        <i className="bi-pencil-fill"></i> Edit
                      </div>
                    </td>
                  </tr>
                );
              })}
        </tbody>
      </table>
    </div>
  );
};

export default UserRole;
