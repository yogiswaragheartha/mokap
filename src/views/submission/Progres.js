import React, { useState, useEffect } from "react";
import { CButton, CCard, CCol, CRow } from "@coreui/react";
import { useSelector } from "react-redux";
import { Link, useNavigate, useParams, useLocation } from "react-router-dom";
import { getEvent } from "src/api/event";
import Input from "src/components/Input";

import {
  getInnovationByID,
  getInnovation,
  getSubmissionRecap,
} from "src/api/submission";
import CButtonSaveProgress from "./CButtonSaveProgress";
import Moment from "moment";

const Progres = (props) => {
  const token = localStorage.getItem("token_jwt");
  const { id } = useParams();
  const location = useLocation();
  const [event, setEvent] = useState([]);
  const [fase, setFase] = useState([]);
  const [sub, setSub] = useState({});
  const [innovator, setInnovator] = useState([{}]);
  const [activity, setActivity] = useState([{}]);
  const [innovations, setInnovations] = useState({});
  const [budget, setBudget] = useState([{}]);
  const [keyResult, setKeyResult] = useState([{}]);
  const [countBudget, setCountBudget] = useState(2);
  const [transaction, setTransaction] = useState([{}]);
  const [countTransaction, setCountTransaction] = useState(2);
  const { auth } = useSelector((state) => state);
  const [total, setTotal] = useState([]);
  const [grandTotal, setGrandTotal] = useState([]);
  // var total = [];

  const handleAddBudget = () => {
    setCountBudget(countBudget + 1);
    const newBudget = budget;
    newBudget.push({
      activity: "Others",
      need: "",
      freq: 0,
      qty: 0,
      price: 0,
      total: 0,
    });
    setBudget(newBudget);
  };

  const handleAddTransaction = () => {
    setCountTransaction(countTransaction + 1);
    const newTrans = transaction;
    newTrans.push({});
    setTransaction(newTrans);
  };

  const hitungTotal = () => {
    const tot = [];
    let grandTotal = 0;
    for (let i = 0; i < document.querySelectorAll("#budget-id").length; i++) {
      const subtotal =
        document.querySelectorAll("#budget-freq")[i].value *
        document.querySelectorAll("#budget-qty")[i].value *
        document.querySelectorAll("#budget-price")[i].value;
      tot.push(subtotal);
      grandTotal += subtotal;
    }
    setTotal(tot);
    setGrandTotal(grandTotal);
  };

  useEffect(() => {
    setInnovations(props.data);
    setSub(props.data);
    setInnovator(props.data.members);
    setActivity(props.data.activities);
    setBudget(props.data.budgets);
    setKeyResult(props.data.keyResults);
    if (props.data.revenues != "") {
      setTransaction(props.data.revenues);
    } else {
      setTransaction([{}]);
    }
    const tot = [];
    let grandTotal = 0;

    if (props.data.budgets) {
      const budget = props.data.budgets;

      for (let i = 0; i < budget.length; i++) {
        const subtotal =
          budget[i].frequencyActual *
          budget[i].quantityActual *
          budget[i].unitPriceActual;

        tot.push(subtotal);
        grandTotal += subtotal;
      }
    }
    setTotal(tot);
    setGrandTotal(grandTotal);
  }, []);

  return (
    <CCard className="shadow border-0 p-4 mb-4">
      <div className="my-4">
        <h5>ACTIVITY</h5>
        <div className="table-responsive">
          <table className="table mt-4">
            <thead style={{ backgroundColor: "#F6F5FF" }}>
              <tr>
                <th scope="col">Activity Name</th>
                <th scope="col">Activity Type</th>
                <th scope="col">Output</th>
                <th scope="col">PIC</th>
                <th scope="col">Progress</th>
              </tr>
            </thead>
            <tbody>
              {console.log(sub)}
              {activity
                ? activity.map(function (item, i) {
                    return (
                      <tr>
                        <td>{item.name}</td>
                        <td>{item.type}</td>
                        <td>{item.output}</td>
                        <td>{item.picName}</td>
                        <td>
                          <Input
                            type="range"
                            id="activity-progress"
                            value={item.progress ? item.progress : 0}
                          />
                          <input
                            type="hidden"
                            value={item.id}
                            id="activity-id"
                          />
                        </td>
                      </tr>
                    );
                  })
                : ""}
            </tbody>
          </table>
        </div>
      </div>
      <div>
        <h5>ACTUAL BUDGET</h5>
        <div className="table-responsive">
          <table className="table my-4">
            <thead style={{ backgroundColor: "#F6F5FF" }}>
              <tr>
                <th scope="col" rowspan={2}>
                  Activity Name
                </th>
                <th scope="col" rowspan={2}>
                  Need
                </th>
                <th
                  scope="col"
                  colspan={4}
                  className="text-center bg-secondary"
                >
                  Plan
                </th>

                <th
                  scope="col"
                  colspan={4}
                  className="text-center bg-light border"
                >
                  Update / Actual
                </th>
                {/* <th
                                scope="col"
                                rowspan={2}
                                className="text-center  border"
                              >
                                Evidence
                              </th> */}
              </tr>
              <tr>
                <th className="bg-secondary border">Freq</th>
                <th className="bg-secondary border">Qty</th>
                <th className="bg-secondary border">Unit Price</th>
                <th className="bg-secondary border">Sub Total</th>
                <th className="bg-light border">Freq</th>
                <th className="bg-light border">Qty</th>
                <th className="bg-light border">Unit Price</th>
                <th className="bg-light border">Sub Total</th>
              </tr>
            </thead>
            <tbody>
              {budget
                ? budget.map(function (item, i) {
                    return (
                      <tr>
                        <td>{item.activity ? item.activity : "Others"}</td>
                        <td>{item.need}</td>
                        <td>{item.frequency ? item.frequency : 0}</td>
                        <td>{item.quantity ? item.quantity : 0}</td>
                        <td>
                          {item.unitPrice
                            ? Intl.NumberFormat().format(item.unitPrice)
                            : 0}
                        </td>
                        <td>
                          {item.unitPrice * item.quantity * item.frequency
                            ? Intl.NumberFormat().format(
                                item.unitPrice * item.quantity * item.frequency
                              )
                            : 0}
                        </td>
                        <td>
                          <input type="hidden" value={item.id} id="budget-id" />
                          <Input
                            type="number"
                            id="budget-freq"
                            value={item.frequencyActual}
                            callBack={hitungTotal}
                            style={{ maxWidth: "100px" }}
                            nolabel
                          />
                        </td>
                        <td>
                          <Input
                            type="number"
                            id="budget-qty"
                            value={item.quantityActual}
                            callBack={hitungTotal}
                            style={{ maxWidth: "100px" }}
                            nolabel
                          />
                        </td>
                        <td>
                          <Input
                            type="number"
                            id="budget-price"
                            value={item.unitPriceActual}
                            callBack={hitungTotal}
                            nolabel
                          />
                        </td>
                        <td className="text-end">
                          {total[i] ? Intl.NumberFormat().format(total[i]) : 0}
                        </td>
                        {/* <td>
                                        <Input
                                          type="file"
                                          id="budget-evidence"
                                          // value={item.evidence}
                                          nolabel
                                        />
                                      </td> */}
                      </tr>
                    );
                  })
                : ""}
              <tr>
                <td colspan={9} className="text-center">
                  <b>Grand Total</b>
                </td>
                <td colspan={2} className="text-end">
                  <b>{Intl.NumberFormat().format(grandTotal)}</b>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="mb-3">
          <CButton
            color="secondary"
            className="text-white btn-sm"
            onClick={handleAddBudget}
          >
            + Others
          </CButton>
        </div>
      </div>

      <div className="my-4">
        <h5>OKR</h5>
        <CRow>
          <CCol>
            <i>Objective</i>
            <p>{sub.objective}</p>
          </CCol>
          <CCol>
            <i>Digitalization</i>
            <p>{sub.digitalization}</p>
          </CCol>
          <CCol>
            <i>Digitization</i>
            <p>{sub.digitization}</p>
          </CCol>
        </CRow>
        <div className="table-responsive">
          <table class="table">
            <thead style={{ backgroundColor: "#F6F5FF" }}>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Key Result</th>
                <th scope="col">Target</th>
                <th scope="col">Unit</th>
                <th scope="col">Realization</th>
              </tr>
            </thead>
            <tbody>
              {!keyResult
                ? ""
                : keyResult.map(function (item, i) {
                    return (
                      <tr>
                        <th scope="row">{i + 1}</th>
                        <td>{item.key}</td>
                        <td>{item.target}</td>
                        <td>{item.unit}</td>
                        <td>
                          <input type="hidden" value={item.id} id="kr-id" />
                          <Input
                            type="text"
                            id="realization"
                            value={item.realization}
                            nolabel
                          />
                        </td>
                      </tr>
                    );
                  })}
            </tbody>
          </table>
        </div>
      </div>
      <div className="my-4">
        <h5>REVENUE</h5>
        <CRow>
          <CCol>
            {!transaction
              ? ""
              : transaction.map(function (item, i) {
                  return (
                    <div
                      class="mb-3 p-3"
                      style={{ backgroundColor: "#F6F5FF" }}
                    >
                      <CRow>
                        <CCol md={3}>
                          <input
                            type="hidden"
                            value={item.id}
                            id="revenue-id"
                          />
                          <Input
                            id={"revenue-start"}
                            label="Periode Start:"
                            type="date"
                            placeholder=""
                            value={Moment(item.periodStart).format(
                              "YYYY-MM-DD"
                            )}
                          />
                        </CCol>
                        <CCol md={3}>
                          <Input
                            id={"revenue-end"}
                            label="Periode End:"
                            type="date"
                            placeholder=""
                            value={Moment(item.periodEnd).format("YYYY-MM-DD")}
                          />
                        </CCol>
                        <CCol md={6}>
                          <Input
                            id={"revenue-total"}
                            label="Current Revenue / Cost Saving:"
                            type="number"
                            placeholder="IDR"
                            value={item.amount}
                          />
                        </CCol>
                      </CRow>
                    </div>
                  );
                })}
          </CCol>
          <CCol md={4}>
            Revenue/Cost Saving Plan: IDR{" "}
            {Intl.NumberFormat().format(sub.revenue)}
          </CCol>
        </CRow>
        <div class="mb-3">
          <CButton
            color="secondary"
            className="text-white btn-sm"
            onClick={handleAddTransaction}
          >
            + Transaction
          </CButton>
        </div>
      </div>
    </CCard>
  );
};

export default Progres;
