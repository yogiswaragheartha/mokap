import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import Input from "src/components/Input";
import { useNavigate } from "react-router-dom";

const CButtonProposal = (props) => {
  const navigate = useNavigate();

  const handleProposal = async (event) => {
    navigate(
      "/event/submission/proposal/" +
        props.data.eventId +
        "/" +
        props.cekPhase +
        "/" +
        props.data.id
    );
    event.preventDefault();
  };
  const handleProgres = async (event) => {
    navigate(
      "/event/submission/progress/" +
        props.data.eventId +
        "/" +
        props.cekPhase +
        "/" +
        props.data.id
    );
    event.preventDefault();
  };
  const handleLaporan = async (event) => {
    navigate(
      "/event/submission/report/" +
        props.data.eventId +
        "/" +
        props.cekPhase +
        "/" +
        props.data.id
    );
    event.preventDefault();
  };

  return (
    <div class="btn-group">
      <font
        class="clickable btn btn-primary"
        aria-current="page"
        onClick={handleProposal}
        style={{
          borderTopLeftRadius: "50px",
          borderTopRightRadius: "50px",
          width: "120px",
          opacity:
            props.active == "proposal" || props.active == "preview"
              ? "1"
              : "0.5",
        }}
      >
        Proposal
      </font>
      {(props.stage == "PROPOSAL" && props.currentPhase == props.cekPhase) ||
      props.stage == null ? (
        <>
          <font
            class="clickable btn btn-primary"
            style={{
              borderTopLeftRadius: "50px",
              borderTopRightRadius: "50px",
              width: "120px",
              opacity: props.active == "progress" ? "1" : "0.5",
            }}
          >
            Progres
          </font>

          <font
            class="clickable btn btn-primary"
            style={{
              borderTopLeftRadius: "50px",
              borderTopRightRadius: "50px",
              width: "120px",
              opacity:
                props.active == "report" || props.active == "reportpreview"
                  ? "1"
                  : "0.5",
            }}
          >
            Laporan
          </font>
        </>
      ) : (
        <>
          <font
            class="clickable btn btn-primary"
            onClick={
              props.stage == "REPORT" || props.currentPhase != props.cekPhase
                ? null
                : handleProgres
            }
            style={{
              borderTopLeftRadius: "50px",
              borderTopRightRadius: "50px",
              width: "120px",
              opacity: props.active == "progress" ? "1" : "0.5",
            }}
          >
            Progres
          </font>

          <font
            class="clickable btn btn-primary"
            onClick={handleLaporan}
            style={{
              borderTopLeftRadius: "50px",
              borderTopRightRadius: "50px",
              width: "120px",
              opacity:
                props.active == "report" || props.active == "reportpreview"
                  ? "1"
                  : "0.5",
            }}
          >
            Laporan
          </font>
        </>
      )}
    </div>
  );
};

export default CButtonProposal;
