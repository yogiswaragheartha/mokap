import React, { useState, useEffect } from "react";
import { CCol, CRow, CButton } from "@coreui/react";
import Input from "src/components/Input";
import { useNavigate } from "react-router-dom";
import submission from "src/data/submission";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import { updateProgress } from "src/api/submission";
import axios from "axios";
import LoadingContent from "src/components/LoadingContent";

const CButtonSaveProgress = (props) => {
  const token = localStorage.getItem("token_jwt");
  const [mod, setMod] = useState(false);
  const [modSubmit, setModSubmit] = useState(false);
  const navigate = useNavigate();
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  const cekTanggal = (start, end) => {
    if (end < start) {
      return false;
    }
  };

  const getContent = () => {
    const data = {};

    for (let i = 0; i < document.querySelectorAll("#activity-id").length; i++) {
      data[`activities[${i}][id]`] =
        document.querySelectorAll("#activity-id")[i].value;
      data[`activities[${i}][progress]`] =
        document.querySelectorAll("#activity-progress")[i].value;
    }

    for (let i = 0; i < document.querySelectorAll("#budget-id").length; i++) {
      if (document.querySelectorAll("#budget-id")[i].value) {
        data[`budgets[${i}][id]`] =
          document.querySelectorAll("#budget-id")[i].value;
      }

      data[`budgets[${i}][frequencyActual]`] =
        document.querySelectorAll("#budget-freq")[i].value;
      data[`budgets[${i}][quantityActual]`] =
        document.querySelectorAll("#budget-qty")[i].value;
      data[`budgets[${i}][unitPriceActual]`] =
        document.querySelectorAll("#budget-price")[i].value;
    }

    for (let i = 0; i < document.querySelectorAll("#kr-id").length; i++) {
      data[`keyResults[${i}][id]`] =
        document.querySelectorAll("#kr-id")[i].value;
      data[`keyResults[${i}][realization]`] =
        document.querySelectorAll("#realization")[i].value;
    }

    for (
      let i = 0;
      i < document.querySelectorAll("#revenue-start").length;
      i++
    ) {
      if (document.querySelectorAll("#revenue-id")[i].value) {
        data[`revenues[${i}][id]`] =
          document.querySelectorAll("#revenue-id")[i].value;
      }

      data[`revenues[${i}][periodStart]`] =
        document.querySelectorAll("#revenue-start")[i].value;
      data[`revenues[${i}][periodEnd]`] =
        document.querySelectorAll("#revenue-end")[i].value;
      data[`revenues[${i}][amount]`] =
        document.querySelectorAll("#revenue-total")[i].value;
    }

    console.log("KONTEN", data);

    return data;
  };

  const handleSave = (event) => {
    const data = getContent();
    save_progress(token, data, props.id);
    event.preventDefault();
  };

  const save_progress = async (token, data, id) => {
    setIsLoading(true);
    try {
      const response = await updateProgress(token, data, id);
      console.log("Create Proposal:", response.data);
      if (response.data) {
        setModal({
          visible: true,
          status: "success",
          message: "",
        });
      }
    } catch (err) {
      setModal({
        visible: true,
        status: "danger",
        message: err.message,
      });

      // alert(err.message);
    }
    setIsLoading(false);
  };

  return (
    <>
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <div style={{ textAlign: "right" }} className={props.className}>
        <CButton
          color="secondary text-white"
          onClick={handleSave}
          style={{ width: "130px" }}
        >
          Save
        </CButton>
      </div>
    </>
  );
};

export default CButtonSaveProgress;
