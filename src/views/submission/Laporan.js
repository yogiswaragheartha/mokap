import React, { useState, useEffect } from "react";
import { CCard, CCol, CContainer, CRow } from "@coreui/react";
import Input from "src/components/Input";
import Moment from "moment";
const Laporan = (props) => {
  const [innovations, setInnovations] = useState({});
  const [pitch, setPitch] = useState();
  const [otherFile, setOtherFile] = useState();
  var totalRealisasi = 0;

  useEffect(() => {
    setInnovations(props.data || [{}]);
    if (props.data.pitchDecks) {
      if (props.data.pitchDecks.length > 0) {
        setPitch(props.data.pitchDecks[0].pitchDeck);
        setOtherFile(props.data.pitchDecks[0].otherFile);
      }
      if (props.data.pitchDecks[0]) {
        props.data.pitchDecks[0].pitchDeckUrl
          ? setPitch(props.data.pitchDecks[0].pitchDeckUrl)
          : "";
      }
      if (props.data.pitchDecks[0]) {
        props.data.pitchDecks[0].otherFileUrl
          ? setOtherFile(props.data.pitchDecks[0].otherFileUrl)
          : "";
      }
    }
  }, []);

  return (
    <>
      <CRow className="mb-4">
        <CCol>
          <CCard className="shadow border-0 p-4">
            <h5>PITCHDECK</h5>
            <Input
              id="pitchdeck"
              label="Pitch Deck File"
              type="file"
              placeholder=".ppt format"
              value={pitch ? pitch : ""}
            />
          </CCard>
        </CCol>
        <CCol>
          <CCard className="shadow border-0 p-4">
            <h5>ADDITIONAL</h5>
            <Input
              id="additional-file"
              label="Additional File"
              type="file"
              placeholder="any format"
              value={otherFile ? otherFile : ""}
            />
          </CCard>
        </CCol>
      </CRow>
      <CCard className="shadow border-0 p-4 mb-4">
        <div className="my-4">
          <h5>ACTIVITY</h5>
          <div className="table-responsive">
            <table className="table mt-4">
              <thead style={{ backgroundColor: "#F6F5FF" }}>
                <tr>
                  <th scope="col">Activity Name</th>
                  <th scope="col">Activity Type</th>
                  <th scope="col">Output</th>
                  <th scope="col">PIC</th>
                  <th scope="col">Progress</th>
                </tr>
              </thead>
              <tbody>
                {innovations.activities
                  ? innovations.activities.map(function (item, i) {
                      return (
                        <tr>
                          <td>{item.name}</td>
                          <td>{item.type}</td>
                          <td>{item.output}</td>
                          <td>{item.picName}</td>
                          <td>
                            <Input
                              type="range"
                              id="activity-progress"
                              value={item.progress}
                              disabled
                            />
                          </td>
                        </tr>
                      );
                    })
                  : ""}
              </tbody>
            </table>
          </div>
        </div>
        <div>
          <h5>ACTUAL BUDGET</h5>
          <div className="table-responsive">
            <table className="table my-4">
              <thead style={{ backgroundColor: "#F6F5FF" }}>
                <tr>
                  <th scope="col" rowspan={2}>
                    Activity Name
                  </th>
                  <th scope="col" rowspan={2}>
                    Need
                  </th>
                  <th
                    scope="col"
                    colspan={4}
                    className="text-center bg-secondary"
                  >
                    Plan
                  </th>

                  <th
                    scope="col"
                    colspan={4}
                    className="text-center bg-light border"
                  >
                    Update / Actual
                  </th>
                </tr>
                <tr>
                  <th className="bg-secondary border">Freq</th>
                  <th className="bg-secondary border">Qty</th>
                  <th className="bg-secondary border">Unit Price</th>
                  <th className="bg-secondary border">Sub Total</th>
                  <th className="bg-light border">Freq</th>
                  <th className="bg-light border">Qty</th>
                  <th className="bg-light border">Unit Price</th>
                  <th className="bg-light border">Sub Total</th>
                </tr>
              </thead>
              <tbody>
                {innovations.budgets
                  ? innovations.budgets.map(function (item, i) {
                      totalRealisasi =
                        totalRealisasi +
                        item.unitPriceActual *
                          item.quantityActual *
                          item.frequencyActual;
                      return (
                        <tr>
                          <td>{item.activity}</td>
                          <td>{item.need}</td>
                          <td>{item.frequency}</td>
                          <td>{item.quantity}</td>
                          <td>{Intl.NumberFormat().format(item.unitPrice)}</td>
                          <td>
                            {Intl.NumberFormat().format(
                              item.unitPrice * item.quantity * item.frequency
                            )}
                          </td>
                          <td>
                            <Input
                              type="number"
                              id="budget-freq"
                              value={item.frequencyActual}
                              style={{ maxWidth: "60px" }}
                              nolabel
                              disabled
                            />
                          </td>
                          <td>
                            <Input
                              type="text"
                              id="budget-qty"
                              value={item.quantityActual}
                              style={{ maxWidth: "60px" }}
                              nolabel
                              disabled
                            />
                          </td>
                          <td>
                            <Input
                              type="text"
                              id="budget-price"
                              value={Intl.NumberFormat().format(
                                item.unitPriceActual
                              )}
                              nolabel
                              disabled
                            />
                          </td>
                          <td>
                            <Input
                              type="text"
                              id="budget-total"
                              value={Intl.NumberFormat().format(
                                item.unitPriceActual *
                                  item.quantityActual *
                                  item.frequencyActual
                              )}
                              nolabel
                              disabled
                            />
                          </td>
                        </tr>
                      );
                    })
                  : ""}
                <tr>
                  <th colspan={6}></th>
                  <th>Total</th>
                  <th colspan={2}></th>
                  <th>{Intl.NumberFormat().format(totalRealisasi)}</th>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <div className="my-4">
          <h5>OKR</h5>
          <CRow>
            <CCol>
              <i>Objective</i>
              <p>{innovations.objective}</p>
            </CCol>
            <CCol>
              <i>Digitalization</i>
              <p>{innovations.digitalization}</p>
            </CCol>
            <CCol>
              <i>Digitization</i>
              <p>{innovations.digitization}</p>
            </CCol>
          </CRow>
          <div className="table-responsive">
            <table class="table">
              <thead style={{ backgroundColor: "#F6F5FF" }}>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Key Result</th>
                  <th scope="col">Target</th>
                  <th scope="col">Unit</th>
                  <th scope="col">Realization</th>
                </tr>
              </thead>
              <tbody>
                {!innovations.keyResults
                  ? ""
                  : innovations.keyResults.map(function (item, i) {
                      return (
                        <tr>
                          <th scope="row">{i + 1}</th>
                          <td>{item.key}</td>
                          <td>{item.target}</td>
                          <td>{item.unit}</td>
                          <td>
                            <Input
                              type="text"
                              id="realization"
                              value={item.realization}
                              nolabel
                              disabled
                            />
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </div>
        </div>
        <div className="my-4">
          <h5>REVENUE</h5>
          <CRow>
            <CCol>
              {!innovations.revenues
                ? ""
                : innovations.revenues.map(function (item, i) {
                    return (
                      <div>
                        <CRow className="border-bottom">
                          <CCol>
                            <table>
                              <tr>
                                <td>Periode Start</td>
                                <td>:</td>
                                <td>
                                  {Moment(item.periodStart).format(
                                    "DD MMM YYYY"
                                  )}
                                </td>
                              </tr>
                              <tr>
                                <td>Periode End</td>
                                <td>:</td>
                                <td>
                                  {Moment(item.periodEnd).format("DD MMM YYYY")}
                                </td>
                              </tr>
                            </table>
                          </CCol>
                          <CCol>
                            Current Revenue/Cost Saving: IDR{" "}
                            {Intl.NumberFormat().format(
                              item.amount ? item.amount : 0
                            )}
                          </CCol>
                        </CRow>
                      </div>
                    );
                  })}
            </CCol>
            <CCol md={4}>
              Revenue/Cost Saving Plan: IDR{" "}
              {Intl.NumberFormat().format(innovations.revenue)}
            </CCol>
          </CRow>
        </div>
      </CCard>
    </>
  );
};

export default Laporan;
