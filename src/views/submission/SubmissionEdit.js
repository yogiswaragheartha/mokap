import React, { useState, useEffect } from "react";
import { CButton, CCard, CCol, CContainer, CRow } from "@coreui/react";
import Input from "src/components/Input";
import Innovator from "./Innovator";
import ActivityPlan from "./ActivityPlan";
import BudgetPlan from "./BudgetPlan";
import KeyResult from "./KeyResult";
import CIcon from "@coreui/icons-react";
import { cilCheckCircle, cilXCircle, cilWarning } from "@coreui/icons";

const SubmissionEdit = (props) => {
  const [sub, setSub] = useState({});
  const [category, setCategory] = useState([]);
  const [area, setArea] = useState([]);
  const [innovator, setInnovator] = useState([{}]);
  const [activity, setActivity] = useState([{}]);
  const [budget, setBudget] = useState([{}]);
  const [keyResult, setKeyResult] = useState([{}]);
  const [countInnovator, setCountInnovator] = useState(2);
  const [countActivity, setCountActivity] = useState(2);
  const [countBudget, setCountBudget] = useState(2);
  const [countKeyResult, setCountKeyResult] = useState(2);
  const [innovations, setInnovations] = useState({});
  const [pitch, setPitch] = useState();
  const [otherFile, setOtherFile] = useState();
  const [activeForm, setActiveForm] = useState(1);
  const [isInnovationDone, setIsInnovationDone] = useState();
  const [isPitchDeckDone, setIsPitchDeckDone] = useState();
  const [isActivityDone, setIsActivityDone] = useState();
  const [isBudgetDone, setIsBudgetDone] = useState();
  const [isOkrDone, setIsOkrDone] = useState();

  const handleAddInnovator = () => {
    setCountInnovator(countInnovator + 1);
    const newInnovator = innovator;
    newInnovator.push({});
    setInnovator(newInnovator);
    setIsInnovationDone(false);
  };

  const handleAddActivity = () => {
    setCountActivity(countActivity + 1);
    const newActivity = activity;
    newActivity.push({});
    setActivity(newActivity);
    setIsActivityDone(false);
  };

  const handleAddBudget = () => {
    setCountBudget(countBudget + 1);
    const newBudget = budget;
    newBudget.push({});
    setBudget(newBudget);
    setIsBudgetDone(false);
  };

  const handleAddKeyResult = () => {
    setCountKeyResult(countKeyResult + 1);
    const newKeyResult = keyResult;
    newKeyResult.push({});
    setKeyResult(newKeyResult);
    setIsOkrDone(false);
  };

  const get_innovation = (data) => {
    // console.log("Data", data);

    setInnovations(data.submission);
    setSub(data.submission);

    if (data.members.length > 0) {
      setInnovator(data.members);
    } else {
      setInnovator([{}]);
    }

    if (data.activities.length > 0) {
      setActivity(data.activities);
    } else {
      setActivity([{}]);
    }

    if (data.budgets.length > 0) {
      setBudget(data.budgets);
    } else {
      setBudget([{}]);
    }

    if (data.keyResults.length > 0) {
      setKeyResult(data.keyResults);
    } else {
      setKeyResult([{}]);
    }

    if (data.pitchDecks) {
      if (data.pitchDecks.length > 0) {
        setPitch(data.pitchDecks[0].pitchDeck || "");
        setOtherFile(data.pitchDecks[0].otherFile || "");
      }
    }
  };

  const changeForm = (num) => {
    setActiveForm(num);
  };

  const validasiInnovation = () => {
    let title, tagline, description, categoryId, revenue, revenueDescription;
    if (document.getElementById("title")) {
      title = document.getElementById("title").value || "";
      tagline = document.getElementById("tagline").value;
      description = document.getElementById("description").value;
      categoryId = document.getElementById("category").value;
      revenue = document.getElementById("estRevenue").value;
      revenueDescription = document.getElementById("descriptionRev").value;
    }

    if (
      !title ||
      !tagline ||
      !description ||
      !categoryId ||
      !revenue ||
      !revenueDescription
    ) {
      setIsInnovationDone(false);
    } else {
      setIsInnovationDone(true);
      const name = document.querySelectorAll("#innovator-name");
      const role = document.querySelectorAll("#innovator-role");
      const area = document.querySelectorAll("#innovator-area");
      for (let i = 0; i < name.length; i++) {
        if (!name[i].value || !role[i].value || !area[i].value) {
          setIsInnovationDone(false);
        }
      }
    }
  };

  const validasiPitch = () => {
    let pitchDeck;
    if (document.getElementById("pitchdeck")) {
      pitchDeck = document.getElementById("pitchdeck").files[0];
    }

    if (!pitchDeck && !pitch) {
      setIsPitchDeckDone(false);
    } else {
      setIsPitchDeckDone(true);
    }
  };

  const validasiActivity = () => {
    setIsActivityDone(true);
    let name = [];
    let type, output, picName, activityStart, activityEnd;
    if (document.querySelectorAll("#activity-name")[0]) {
      name = document.querySelectorAll("#activity-name");
      type = document.querySelectorAll("#activity-type");
      output = document.querySelectorAll("#activity-output");
      picName = document.querySelectorAll("#activity-pic");
      activityStart = document.querySelectorAll("#activity-start");
      activityEnd = document.querySelectorAll("#activity-end");
    }

    for (let i = 0; i < name.length; i++) {
      if (
        !name[i].value ||
        !type[i].value ||
        !output[i].value ||
        !picName[i].value ||
        !activityStart[i].value ||
        !activityEnd[i].value
      ) {
        setIsActivityDone(false);
      }
    }
  };

  const validasiBudget = () => {
    setIsBudgetDone(true);
    let activity = [];
    let need, frequency, quantity, unit, unitPrice, paymentFrom, paymentTo;
    if (document.querySelectorAll("#budget-activity")[0]) {
      activity = document.querySelectorAll("#budget-activity");
      need = document.querySelectorAll("#budget-need");
      frequency = document.querySelectorAll("#budget-frequency");
      quantity = document.querySelectorAll("#budget-quantity");
      unit = document.querySelectorAll("#budget-unit");
      unitPrice = document.querySelectorAll("#budget-price");
      paymentFrom = document.querySelectorAll("#budget-payment-from");
      paymentTo = document.querySelectorAll("#budget-payment-to");
    }

    for (let i = 0; i < activity.length; i++) {
      if (
        !activity[i].value ||
        !need[i].value ||
        !frequency[i].value ||
        !quantity[i].value ||
        !unit[i].value ||
        !unitPrice[i].value ||
        !paymentFrom[i].value ||
        !paymentTo[i].value
      ) {
        setIsBudgetDone(false);
      }
    }
  };

  const validasiOkr = () => {
    let objective, digitalization, digitization;
    if (document.getElementById("objective")) {
      objective = document.getElementById("objective").value;
      digitalization = document.getElementById("digitalization").value;
      digitization = document.getElementById("digitization").value;
    }

    if (!objective || !digitalization || !digitization) {
      setIsOkrDone(false);
    } else {
      setIsOkrDone(true);
      let key = [];
      let target, unit;
      if (document.querySelectorAll("#kr-key")[0]) {
        key = document.querySelectorAll("#kr-key");
        target = document.querySelectorAll("#kr-target");
        unit = document.querySelectorAll("#kr-unit");
      }
      for (let i = 0; i < key.length; i++) {
        if (!key[i].value || !target[i].value || !unit[i].value) {
          setIsOkrDone(false);
        }
      }
    }
  };

  useEffect(() => {
    if (props.category) {
      setCategory(props.category);
    }
    if (props.area) {
      setArea(props.area);
    }
  });

  useEffect(() => {
    if (props.data) {
      if (props.data.submission) {
        get_innovation(props.data);
      }
    }
    setCategory(props.category);
    setArea(props.area);
  }, [props.data]);

  useEffect(() => {
    if (sub) {
      setTimeout(() => {
        validasiInnovation();
        validasiPitch();
        validasiActivity();
        validasiBudget();
        validasiOkr();
      }, 1000);
    }
  }, [sub]);

  return (
    <CRow>
      <CCol md={3} className="mb-4">
        <h5 className="text-center">Section</h5>
        <div
          className={` click-div shadow d-flex justify-content-between ${
            activeForm == 1 ? "text-primary bg-prpl h6" : ""
          }`}
          onClick={() => changeForm(1)}
        >
          Innovation
          <p>
            {isInnovationDone ? (
              <CIcon icon={cilCheckCircle} size="lg" className="text-success" />
            ) : (
              <CIcon icon={cilXCircle} size="lg" className="text-danger" />
            )}
          </p>
        </div>
        <div
          className={`click-div shadow d-flex justify-content-between ${
            activeForm == 2 ? "text-primary bg-prpl h6" : ""
          }`}
          onClick={() => changeForm(2)}
        >
          Pitchdeck
          <p>
            {isPitchDeckDone ? (
              <CIcon icon={cilCheckCircle} size="lg" className="text-success" />
            ) : (
              <CIcon icon={cilWarning} size="lg" className="text-warning" />
            )}
          </p>
        </div>
        <div
          className={`click-div shadow d-flex justify-content-between ${
            activeForm == 3 ? "text-primary bg-prpl h6" : ""
          }`}
          onClick={() => changeForm(3)}
        >
          Activity Plan
          <p>
            {isActivityDone ? (
              <CIcon icon={cilCheckCircle} size="lg" className="text-success" />
            ) : (
              <CIcon icon={cilXCircle} size="lg" className="text-danger" />
            )}
          </p>
        </div>
        <div
          className={`click-div shadow d-flex justify-content-between ${
            activeForm == 4 ? "text-primary bg-prpl h6" : ""
          }`}
          onClick={() => changeForm(4)}
        >
          Budget Plan
          <p>
            {isBudgetDone ? (
              <CIcon icon={cilCheckCircle} size="lg" className="text-success" />
            ) : (
              <CIcon icon={cilXCircle} size="lg" className="text-danger" />
            )}
          </p>
        </div>
        <div
          className={`click-div shadow d-flex justify-content-between ${
            activeForm == 5 ? "text-primary bg-prpl h6" : ""
          }`}
          onClick={() => changeForm(5)}
        >
          OKR
          <p>
            {isOkrDone ? (
              <CIcon icon={cilCheckCircle} size="lg" className="text-success" />
            ) : (
              <CIcon icon={cilXCircle} size="lg" className="text-danger" />
            )}
          </p>
        </div>
      </CCol>
      <CCol>
        <CCard
          className={`shadow border-0 p-4 mb-4 ${
            activeForm == 1 ? "" : "d-none"
          }`}
        >
          <CRow>
            <Input
              id="title"
              label="Title"
              type="text"
              placeholder="Name of your innovation"
              value={sub.title}
              callBack={validasiInnovation}
              required
            />
            <Input
              id="tagline"
              label="Tagline"
              type="text"
              placeholder="Tagline of your innovation"
              value={sub.tagline}
              callBack={validasiInnovation}
              required
            />
            <Input
              id="description"
              label="Description"
              type="text-area"
              placeholder="Describe your innovation"
              value={sub.description}
              callBack={validasiInnovation}
              required
            />
            <CRow>
              <CCol md={6}>
                <Input
                  id="logo"
                  label="Innovation Logo"
                  type="file"
                  value={sub.logo ? sub.logo.name : ""}
                />
                <Input
                  id="category"
                  label="Category"
                  type="select"
                  placeholder="Select Category"
                  value={sub.categoryId}
                  opsi={category}
                  callBack={validasiInnovation}
                  required
                />
                <Input
                  id="estRevenue"
                  label="Est Revenue / Saving Cost"
                  type="number"
                  placeholder="Estimation amount"
                  value={sub.revenue}
                  callBack={validasiInnovation}
                  required
                />
              </CCol>
              <CCol></CCol>
            </CRow>

            <Input
              id="descriptionRev"
              label="Revenue Description"
              type="text-area"
              placeholder="Describe the calculation"
              value={sub.revenueDescription}
              callBack={validasiInnovation}
              required
            />
            <p>Innovators</p>
            {!innovator
              ? ""
              : innovator.map(function (item, i) {
                  return (
                    <>
                      <Innovator
                        i={i + 1}
                        value={item}
                        area={area}
                        callBack={validasiInnovation}
                      />
                    </>
                  );
                })}
            <div class="mb-3">
              <CButton
                color="secondary"
                className="text-white btn-sm"
                onClick={handleAddInnovator}
              >
                + More Innovator
              </CButton>
            </div>
          </CRow>
        </CCard>

        <CRow className={`mb-4 ${activeForm == 2 ? "" : "d-none"}`}>
          <CCol>
            <CCard className="shadow border-0 p-4">
              <h5>PITCHDECK</h5>
              <Input
                id="pitchdeck"
                label="Pitch Deck File"
                type="file"
                placeholder=".ppt format"
                callBack={validasiPitch}
                value={pitch}
              />
            </CCard>
          </CCol>
          <CCol>
            <CCard className="shadow border-0 p-4">
              <h5>ADDITIONAL</h5>
              <Input
                id="additional-file"
                label="Additional File"
                type="file"
                placeholder="any format"
                value={otherFile}
              />
            </CCard>
          </CCol>
        </CRow>

        <CCard
          className={`shadow border-0 p-4 mb-4 ${
            activeForm == 3 ? "" : "d-none"
          }`}
        >
          {!activity
            ? ""
            : activity.map(function (item, i) {
                return (
                  <>
                    Activity {i + 1}
                    <ActivityPlan
                      i={i + 1}
                      value={item}
                      callBack={validasiActivity}
                    />
                  </>
                );
              })}
          <div class="mb-3">
            <CButton
              color="secondary"
              className="text-white btn-sm"
              onClick={handleAddActivity}
            >
              + More Activity
            </CButton>
          </div>
        </CCard>

        <CCard
          className={`shadow border-0 p-4 mb-4 ${
            activeForm == 4 ? "" : "d-none"
          }`}
        >
          {!budget
            ? ""
            : budget.map(function (item, i) {
                return (
                  <>
                    Budget {i + 1}
                    <BudgetPlan
                      key={i}
                      i={i + 1}
                      value={item}
                      callBack={validasiBudget}
                    />
                  </>
                );
              })}
          <div class="mb-3">
            <CButton
              color="secondary"
              className="text-white btn-sm"
              onClick={handleAddBudget}
            >
              + More Budget
            </CButton>
          </div>
        </CCard>

        <CCard
          className={`shadow border-0 p-4 mb-4 ${
            activeForm == 5 ? "" : "d-none"
          }`}
        >
          <Input
            id="objective"
            label="Objective"
            type="text-area"
            placeholder="Objective utama pada fase berjalan"
            value={sub.objective}
            callBack={validasiOkr}
            required
          />

          <Input
            id="digitalization"
            label="Digitalization"
            type="text"
            placeholder="tujuan sisi digitalisasi"
            value={sub.digitalization}
            callBack={validasiOkr}
            required
          />
          <Input
            id="digitization"
            label="Digitization"
            type="text"
            placeholder="tujuan sisi digitisasi"
            value={sub.digitization}
            callBack={validasiOkr}
            required
          />

          {!keyResult
            ? ""
            : keyResult.map(function (item, i) {
                return (
                  <>
                    Key Result {i + 1}
                    <KeyResult i={i + 1} value={item} callBack={validasiOkr} />
                  </>
                );
              })}
          <div class="mb-3">
            <CButton
              color="secondary"
              className="text-white btn-sm"
              onClick={handleAddKeyResult}
            >
              + More Key Result
            </CButton>
          </div>
        </CCard>
      </CCol>
    </CRow>
  );
};

export default SubmissionEdit;
