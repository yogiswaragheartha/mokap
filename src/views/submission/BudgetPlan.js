import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import Input from "src/components/Input";
// import { array } from "prop-types";

const BudgetPlan = (props) => {
  // const [activity, setActivity] = useState([
  //   {
  //     text: props.value.activity,
  //     value: props.value.activity,
  //   },
  // ]);
  const [actValue, setActValue] = useState();

  const [isMounted, setIsMounted] = useState(false);

  const [activityList, setActivityList] = useState([]);

  const setListActivity = () => {
    const act = [];
    for (
      let i = 0;
      i < document.querySelectorAll("#activity-name").length;
      i++
    ) {
      act.push({
        text: document.querySelectorAll("#activity-name")[i].value,
        value: document.querySelectorAll("#activity-name")[i].value,
      });
    }

    setActivityList(act);
  };

  useEffect(() => {
    setActValue(props.value.activity);
  }, [props.value.activity]);

  useEffect(() => {
    if (isMounted == false) {
      const act = [];
      for (
        let i = 0;
        i < document.querySelectorAll("#activity-name").length;
        i++
      ) {
        act.push({
          text: document.querySelectorAll("#activity-name")[i].value,
          value: document.querySelectorAll("#activity-name")[i].value,
        });
      }
      // console.log("ACT", act);
      const isValid = act.every((item) => item.value != "");
      // console.log("isValid", isValid);
      if (isValid) {
        setIsMounted(true);
        setActivityList(act);
      }
    }
  });

  return (
    <div class="mb-3 p-3" style={{ backgroundColor: "#F6F5FF" }}>
      <div class="mb-3">
        <CRow className="">
          <CCol md={6}>
            <input type="hidden" id="budget-id" value={props.value.id} />
            <Input
              id={"budget-activity"}
              label="Choose Activity"
              type="select"
              placeholder="select activity"
              value={actValue}
              opsi={activityList}
              onClick={setListActivity}
              callBack={props.callBack}
              required
            />

            <CRow>
              <CCol sm={6}>
                <Input
                  id={"budget-frequency"}
                  label="Frequency"
                  type="number"
                  placeholder=""
                  callBack={props.callBack}
                  value={props.value.frequency}
                  required
                />
              </CCol>
              <CCol sm={6}>
                <Input
                  id={"budget-quantity"}
                  label="Quantity"
                  type="number"
                  placeholder=""
                  callBack={props.callBack}
                  value={props.value.quantity}
                  required
                />
              </CCol>
            </CRow>
          </CCol>
          <CCol md={6}>
            <Input
              id={"budget-need"}
              label="Need"
              type="text"
              placeholder={"What you need"}
              value={props.value.need}
              callBack={props.callBack}
              required
            />
            <CRow>
              <CCol sm={5}>
                <Input
                  id={"budget-unit"}
                  label="Unit"
                  type="select"
                  placeholder=""
                  value={props.value.unit}
                  opsi={[
                    { value: "Pcs", text: "pcs" },
                    { value: "%", text: "%" },
                    { value: "unit", text: "unit" },
                  ]}
                  callBack={props.callBack}
                  required
                />
              </CCol>
              <CCol sm={7}>
                <Input
                  id={"budget-price"}
                  label="Unit Price"
                  type="number"
                  placeholder=""
                  callBack={props.callBack}
                  value={props.value.unitPrice}
                  required
                />
              </CCol>
            </CRow>
          </CCol>
        </CRow>
        <CRow>
          <CCol md={4}>
            <Input
              id={"budget-payment-from"}
              label="Payment from"
              type="text"
              placeholder=""
              callBack={props.callBack}
              value={props.value.paymentFrom}
              required
            />
          </CCol>
          <CCol md={4}>
            <Input
              id={"budget-payment-to"}
              label="Payment to"
              type="text"
              placeholder=""
              callBack={props.callBack}
              value={props.value.paymentTo}
              required
            />
          </CCol>
          <CCol md={4}></CCol>
        </CRow>
      </div>
    </div>
  );
};

export default BudgetPlan;
