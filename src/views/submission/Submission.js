import React, { useState, useEffect } from "react";
import { CCol, CContainer, CRow } from "@coreui/react";
import { useNavigate, useParams, useLocation } from "react-router-dom";
import { getAreas } from "src/api/user";
import { getEvent } from "src/api/event";
import { getSubmissionRecap, getMyInnovation } from "src/api/submission";
import CPhase from "./CPhase";
import CButtonProposal from "./CButtonProposal";
import CButtonSubmit from "./CButtonSubmit";
import CButtonSaveProgress from "./CButtonSaveProgress";
import CButtonSubmitLaporan from "./CButtonSubmitLaporan";
import SubmissionPreview from "./SubmissionPreview";
import SubmissionEdit from "./SubmissionEdit";
import Progres from "./Progres";
import Laporan from "./Laporan";
import LaporanPreview from "./LaporanPreview";
import LoadingData from "src/components/LoadingData";

const Submission = (props) => {
  const token = localStorage.getItem("token_jwt");
  const { page, id_event, id_phase, id_innovation } = useParams();
  const navigate = useNavigate();
  const [event, setEvent] = useState([]);
  const [fase, setFase] = useState([]);
  const [sub, setSub] = useState({});
  const [category, setCategory] = useState([]);
  const [area, setArea] = useState([]);
  const [content, setContent] = useState();
  const [button, setButton] = useState();
  const [isNewSubmission, setIsNewSubmission] = useState(true);
  const [innovations, setInnovations] = useState({});
  const [isLoadingData, setisLoadingData] = useState(true);
  const [dataPreview, setDataPreview] = useState();

  const get_event = async (token, id) => {
    console.log("%c run get_event", "background:#99f");
    try {
      const response = await getEvent(token, id);
      console.log(
        "%c set event, fase, category:",
        "background:#99f",
        response.data.event
      );
      setEvent(response.data.event);
      setFase(response.data.event.Phases);
      const cat = [];
      response.data.event.Categories.map(function (item, i) {
        cat.push({
          text: item.name,
          value: item.id,
        });
      });
      setCategory(cat);
    } catch (error) {
      console.log(error);
    }
  };

  const get_area = async (token) => {
    console.log("%c run get_area", "background:yellow");
    try {
      const response = await getAreas(token);
      const ar = [];
      response.data.map(function (item, i) {
        ar.push({
          text: item.name,
          value: item.name,
        });
      });
      setArea(ar);
      console.log("%c set area:", "background:yellow", ar);
    } catch (error) {
      console.log(error);
    }
  };

  const get_submission = (data) => {
    console.log("%c run get_submission", "background: purple; color: #bada55");
    if (data) {
      if (id_phase == data.phaseId) {
        console.log(
          "%c set innovation & sub",
          "background: purple; color: #bada55",
          data
        );
        setInnovations(data);
        setSub(data);
        changeContent(data);
      } else if (!id_phase) {
        console.log(
          "%c set NEW innovation & sub",
          "background: purple; color: #bada55",
          data
        );
        setInnovations(data);
        setSub(data);
        changeContent(data);
      } else {
        get_submissionRecap(token, id_innovation, id_phase);
      }
    } else {
      console.log(
        "%c no previewData, call get_submissionRecap",
        "background: purple; color: #bada55"
      );
      get_submissionRecap(token, id_innovation, id_phase);
    }
  };

  const get_submissionRecap = async (token, id_submission, id_phase) => {
    setisLoadingData(true);
    console.log("%c run get_submissionRecap", "background:orange");
    try {
      const response = await getSubmissionRecap(token, id_submission, id_phase);

      const submission = response.data.submission;
      const inov = Object.assign({}, submission, response.data);

      inov["pitchDecks"] = inov.pitchDecks.filter(
        (c) =>
          (page == "proposal" && c.stage == "PROPOSAL") ||
          (page == "report" && c.stage == "REPORT")
      );

      setInnovations(inov);
      console.log("%c set innovations:", "background:orange", inov);

      setSub(inov);
      changeContent(inov);
      if (props.callBack) {
        props.callBack(inov);
      }
    } catch (error) {
      console.log(error);
    }
    setisLoadingData(false);
  };

  const changeContent = (data) => {
    if (page == "proposal" && !id_innovation) {
      //NEW
      console.log(
        "%c run changeContent NEW SUBMISSION",
        "background: red; color: #bada55"
      );
      setisLoadingData(false);
      setContent(
        <SubmissionEdit data={data} area={area} category={category} />
      );

      setButton(
        <CButtonSubmit
          id={id_event}
          fase={fase}
          new={true}
          type={page == "proposal" ? "preview" : "edit"}
          callBack={(e) => {
            setDataPreview(e);
          }}
        />
      );
    } else if (page == "preview" && !id_innovation) {
      //NEW
      console.log(
        "%c run changeContent NEW SubmissionPreview",
        "background: red; color: #bada55"
      );
      setContent(<SubmissionPreview data={data} />);

      setButton(
        <CButtonSubmit
          id={id_event}
          data={data}
          fase={fase}
          new={true}
          type={page == "proposal" ? "preview" : "edit"}
          callBack={(e) => {
            setDataPreview(e);
          }}
        />
      );
    } else if (
      page == "proposal" &&
      data.submission.status != "SUBMITTED" &&
      props.type != "approval" &&
      data.stage == "PROPOSAL" &&
      id_phase == data.phaseId
    ) {
      console.log(
        "%c run changeContent SubmissionEdit",
        "background: red; color: #bada55"
      );
      setContent(
        <SubmissionEdit data={data} area={area} category={category} />
      );
      setButton(
        <CButtonSubmit
          id_innovation={id_innovation}
          id_phase={id_phase}
          id={id_event}
          data={data}
          fase={fase}
          type={page == "proposal" ? "preview" : "edit"}
          callBack={(e) => {
            setDataPreview(e);
          }}
        />
      );
    } else if (
      page == "preview" ||
      (page == "proposal" && data.submission.status == "SUBMITTED") ||
      (props.type == "approval" && data.stage == "PROPOSAL") ||
      (data.stage && data.stage != "PROPOSAL" && page == "proposal") ||
      (page == "proposal" && id_phase != data.phaseId)
    ) {
      console.log(
        "%c run changeContent SubmissionPreview",
        "background: red; color: #bada55"
      );
      setContent(<SubmissionPreview data={data} />);

      setButton(
        <CButtonSubmit
          id_innovation={id_innovation}
          id_phase={id_phase}
          id={id_event}
          data={data}
          fase={fase}
          type={page == "proposal" ? "preview" : "edit"}
          callBack={(e) => {
            setDataPreview(e);
          }}
        />
      );
      if (
        data.submission.status == "SUBMITTED" ||
        data.submission.stage != "PROPOSAL" ||
        props.type == "approval" ||
        id_phase != data.phaseId
      ) {
        setButton();
      }
    } else if (page == "progress") {
      setContent(<Progres data={data} />);
      setButton(
        <CButtonSaveProgress
          id={data.id}
          className=" mx-2 btn-md text-white d-none d-lg-block"
        />
      );
    } else if (
      page == "report" &&
      data.status != "REPORTED" &&
      props.type != "approval" &&
      id_phase == data.phaseId
    ) {
      setContent(<Laporan data={data} />);
      setButton(
        <CButtonSubmitLaporan
          id_innovation={id_innovation}
          id_phase={id_phase}
          id={id_event}
          fase={fase}
          data={data}
          type={page == "report" ? "preview" : "edit"}
          callBack={(e) => {
            setDataPreview(e);
          }}
        />
      );
    } else if (
      page == "reportpreview" ||
      (page == "report" && data.status == "REPORTED") ||
      (page == "report" && props.type == "approval") ||
      (page == "report" && id_phase != data.phaseId)
    ) {
      setContent(
        <LaporanPreview data={data} fase={[]} type="approval" id={data.id} />
      );
      setButton(
        <CButtonSubmitLaporan
          id_innovation={id_innovation}
          id_phase={id_phase}
          id={id_event}
          fase={fase}
          data={data}
          type={page == "report" ? "preview" : "edit"}
          callBack={(e) => {
            setDataPreview(e);
          }}
        />
      );

      if (
        data.submission.status == "REPORTED" ||
        props.type == "approval" ||
        id_phase != data.phaseId
      ) {
        setButton();
      }
    }
  };

  const get_latest_submission = async (token) => {
    setisLoadingData(true);
    try {
      const response = await getMyInnovation(token);

      const submission = response.data;
      // console.log(submission);
      if (submission.userInnovation) {
        navigate(
          "/event/submission/proposal/" +
            submission.userInnovation.eventId +
            "/" +
            submission.userInnovation.phaseId +
            "/" +
            submission.userInnovation.id
        );
      } else {
        navigate("/event/list", {
          state: {
            visible: true,
            status: "warning",
            message: "Join event first",
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
    setisLoadingData(false);
  };

  useEffect(() => {
    console.log("%c First load submission", "background: #222; color: #bada55");
    // if (id_event == "undefined") {
    //   navigate("/event/list", {
    //     state: {
    //       visible: true,
    //       status: "warning",
    //       message: "Join event first",
    //     },
    //   });
    //   return;
    // }
    if (!id_event && !id_phase && !id_innovation) {
      get_latest_submission(token);
    }
    setFase([{}]);

    get_event(token, id_event);
    get_area(token);
  }, []);

  useEffect(() => {
    if (!event) {
      get_event(token, id_event);
      get_area(token);
    }
    if (!id_event && !id_phase && !id_innovation) {
      get_latest_submission(token);
    }
  }, [id_event]);

  useEffect(() => {
    if (area && category) {
      console.log(
        "%c Effect (page, id_event, id_phase, id_innovation)",
        "background: #666; color: #bada55"
      );
      if (!dataPreview && id_innovation) {
        console.log(
          "%c useEffect, dataPreview null",
          "background: #666; color: #bada55"
        );
        get_submission();
      } else if (dataPreview) {
        console.log(
          "%c useEffect, dataPreview not null",
          "background: #666; color: #bada55",
          dataPreview
        );
        get_submission(dataPreview);
      } else {
        setTimeout(() => {
          changeContent();
        }, 0);
      }
    }
  }, [page, id_event, id_phase, id_innovation, area, category]);

  useEffect(() => {
    console.log(
      "%c Effect (page, dataPreview), \ndataPreview:",
      "background: #888; color: #bada55",
      dataPreview
    );

    if (dataPreview) {
      get_submission(dataPreview);
    }
  }, [page, dataPreview]);

  return (
    <div className="bg-light mt-4 align-items-center">
      <CContainer>
        <h3>Submission</h3>
        <CPhase
          fase={fase}
          new={isNewSubmission}
          innovation={sub}
          stage={sub.stage}
          isApproval={props.type == "approval" ? true : false}
          id_innovation={id_innovation}
          page={page}
        />

        <CRow>
          <CCol>
            {props.type == "approval" ? (
              ""
            ) : (
              <CButtonProposal
                active={page}
                data={sub}
                currentPhase={sub.phaseId}
                cekPhase={id_phase}
                stage={innovations.stage}
              />
            )}
          </CCol>
          <CCol>
            {isLoadingData ? (
              ""
            ) : (
              <div className="d-none  d-lg-block">{button}</div>
            )}
          </CCol>
        </CRow>
      </CContainer>
      <div className=" justify-content-center">
        <div md={9} lg={7} xl={6} sm={12} className="">
          <div className="">
            <div
              style={{ marginRight: "-10px", marginLeft: "-10px" }}
              className="bg-white start-0"
            >
              <div
                className="px-0 pb-5 shadow "
                style={{ borderRadius: "10px" }}
              >
                <CContainer className="pt-4">
                  {isLoadingData ? (
                    <div className="mb-5" style={{ height: "200px" }}>
                      <LoadingData />
                    </div>
                  ) : (
                    <>
                      <div className="d-lg-none">{button}</div>

                      {content}
                    </>
                  )}
                </CContainer>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Submission;
