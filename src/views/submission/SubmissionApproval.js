import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import { useParams, useNavigate } from "react-router-dom";

import { cilSave, cilDescription } from "@coreui/icons";
import {
  getInnovationByID,
  approvalSubmission,
  getSubmissionRecap,
} from "src/api/submission";
import { getEvent, getPhase } from "src/api/event";
import CIcon from "@coreui/icons-react";
import Input from "src/components/Input";
import SubmissionPreview from "./SubmissionPreview";
// import submission from "src/data/submission";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import LoadingContent from "src/components/LoadingContent";
import LoadingData from "src/components/LoadingData";
import LaporanPreview from "./LaporanPreview";
import Submission from "./Submission";

const SubmissionApproval = () => {
  const token = localStorage.getItem("token_jwt");
  const { id_innovation, id_phase } = useParams();
  const navigate = useNavigate();
  const [inovasi, setInovasi] = useState([]);
  const [listInovasi, setListInovasi] = useState([]);
  const [submission, setSubmission] = useState({});
  const [listEvent, setListEvent] = useState([]);
  const [data, setData] = useState();
  const [fase, setFase] = useState([]);
  const [content, setContent] = useState();
  const [mod, setMod] = useState(false);
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmit = async (event) => {
    const status = document.getElementById("action").value;
    const comment = document.getElementById("komen").value;
    const innovationId = id_innovation;

    if (!status || !comment) {
      setModal({
        visible: true,
        message: "Fill in the form corectly",
        status: "warning",
      });
      setMod(false);
      return;
    }

    const data = {
      status,
      comment,
      innovationId,
    };

    approval_submission(token, data);
  };

  const approval_submission = async (token, data) => {
    setIsLoading(true);
    try {
      const response = await approvalSubmission(token, data);
      console.log("Approval", data);
      if (response.data.message) {
        navigate("/event/submission/list", {
          state: {
            visible: true,
            status: "success",
            message: "",
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    setContent(
      <Submission type="approval" callBack={(inov) => setData(inov)} />
    );
  }, []);

  return (
    <div>
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      {/* {console.log("DATA->", data)C} */}

      {content}

      <div
        style={{
          marginRight: "-10px",
          marginLeft: "-10px",
          marginTop: "-100px",
        }}
        className="bg-white start-0"
      >
        <CContainer className="mt-5 ">
          {!data ? (
            ""
          ) : data.status != "SUBMITTED" || id_phase != data.phaseId ? (
            ""
          ) : (
            <CCard className=" shadow border-0  p-4 mb-4">
              <h4 className="">APPROVAL </h4>
              <CRow>
                <CCol>
                  <Input
                    type="select"
                    id="action"
                    label="Action"
                    placeholder="select action"
                    opsi={[
                      {
                        value: "APPROVED",
                        text: "Approve",
                      },
                      {
                        value: "RETURNED",
                        text: "Return",
                      },
                    ]}
                    required
                  />
                </CCol>
                <CCol md={8}>
                  <Input
                    type="text-area"
                    id="komen"
                    value=""
                    label="Say Something"
                    required
                  />
                </CCol>
              </CRow>
              <div className="d-flex justify-content-center">
                <CButton
                  onClick={() => setMod(!mod)}
                  style={{ width: "90px" }}
                  className={"bg-warning text-black  border-0 mt-4 btn-md me-2"}
                >
                  Submit
                </CButton>
                <Modal
                  pesan="Are you sure to Submit?"
                  input={false}
                  callBack={handleSubmit}
                  close={() => setMod(false)}
                  visible={mod}
                />
              </div>
            </CCard>
          )}
        </CContainer>
      </div>
    </div>
  );
};

export default SubmissionApproval;
