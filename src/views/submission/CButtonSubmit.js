import React, { useState, useEffect } from "react";
import { CButton } from "@coreui/react";
import { useNavigate } from "react-router-dom";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import { createProposal, updateProposal } from "src/api/submission";
import LoadingContent from "src/components/LoadingContent";

const CButtonSubmit = (props) => {
  const token = localStorage.getItem("token_jwt");
  const [mod, setMod] = useState(false);
  const [modSubmit, setModSubmit] = useState(false);
  const navigate = useNavigate();
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  const cekTanggal = (start, end) => {
    if (end < start) {
      return false;
    }
  };

  const getContent = (status, stage) => {
    const title = document.getElementById("title").value;
    const tagline = document.getElementById("tagline").value;
    const description = document.getElementById("description").value;
    const categoryId = document.getElementById("category").value;
    const category =
      document.getElementById("category").options[
        document.getElementById("category").selectedIndex
      ].text;
    const revenue = document.getElementById("estRevenue").value;
    const revenueDescription = document.getElementById("descriptionRev").value;
    const objective = document.getElementById("objective").value;
    const digitalization = document.getElementById("digitalization").value;
    const digitization = document.getElementById("digitization").value;

    if (
      (!title ||
        !tagline ||
        !description ||
        !categoryId ||
        !revenue ||
        !revenueDescription) &&
      status == "DRAFT"
    ) {
      setModal({
        visible: true,
        message: "Before save, please fill at least Innovation form",
        status: "warning",
      });
      setIsLoading(false);
      setMod(false);
      return "falsesave";
    }

    if (
      (!title ||
        !tagline ||
        !description ||
        !categoryId ||
        !revenue ||
        !revenueDescription ||
        !objective ||
        !digitalization ||
        !digitization) &&
      status == "SUBMITTED"
    ) {
      setModal({
        visible: true,
        message: "Fill in the form corectly",
        status: "warning",
      });
      setIsLoading(false);
      setMod(false);
      return false;
    }

    let pitchDeck = document.getElementById("pitchdeck").files[0];

    let pitchDeckUrl;
    if (!pitchDeck) {
      if (props.data) {
        pitchDeck = props.data.pitchDeck;
        if (props.data.pitchDecks) {
          if (props.data.pitchDecks.length > 0) {
            pitchDeckUrl = props.data.pitchDecks[0].pitchDeck;
          } else {
            pitchDeckUrl = null;
          }
        }
      }
    } else {
      pitchDeckUrl = URL.createObjectURL(pitchDeck);
    }

    let otherFile = document.getElementById("additional-file").files[0];

    let otherFileUrl;
    if (!otherFile) {
      if (props.data) {
        otherFile = props.data.other;
        if (props.data.pitchDecks) {
          if (props.data.pitchDecks.length > 0) {
            otherFileUrl = props.data.pitchDecks[0].otherFile;
          } else {
            otherFileUrl = null;
          }
        }
      }
    } else {
      otherFileUrl = URL.createObjectURL(otherFile);
    }

    console.log("props data", props.data);
    const submission = {
      eventId: props.id,
      // phaseId: props.id_phase ? props.id_phase : props.fase[0].id,
      phaseId: props.data ? props.data.phaseId : props.fase[0].id,
      phase: { id: props.id_phase ? props.id_phase : props.fase[0].id },
      status: status,
      stage: stage,
      title,
      tagline,
      description,
      categoryId,
      category,
      revenue,
      revenueDescription,

      logo: document.getElementById("logo").files[0]
        ? document.getElementById("logo").files[0]
        : props.data
        ? props.data.logo
        : undefined,

      pitchDeck: pitchDeck,

      other: otherFile,

      objective,
      digitalization,
      digitization,
    };

    const data = submission;
    data["pitchDecks"] = [{ pitchDeck: pitchDeckUrl, otherFile: otherFileUrl }];
    data["submission"] = submission;

    //INNOVATOR
    //array members sebagai array of object penampungan data, sesuai GET API.
    //sedangkan data[`members[${i}][name]`] sebagai jalur input
    const members = [];
    for (
      let i = 0;
      i < document.querySelectorAll("#innovator-name").length;
      i++
    ) {
      const name = document.querySelectorAll("#innovator-name")[i].value;
      const role = document.querySelectorAll("#innovator-role")[i].value;
      const area = document.querySelectorAll("#innovator-area")[i].value;

      if ((!name || !role || !area) && status == "SUBMITTED") {
        setModal({
          visible: true,
          message: "Innovator " + (i + 1) + ": Fill in the form corectly",
          status: "warning",
        });
        setIsLoading(false);
        setMod(false);
        return false;
      }

      const id = document.querySelectorAll("#innovator-id")[i].value;
      if (id) {
        data[`members[${i}][id]`] = id;
        members.push({ id, name, role, area });
      } else {
        members.push({ name, role, area });
      }

      data["members"] = members;

      data[`members[${i}][name]`] = name;
      data[`members[${i}][role]`] = role;
      data[`members[${i}][area]`] = area;
    }

    //ACTIVITY
    const activities = [];
    for (
      let i = 0;
      i < document.querySelectorAll("#activity-name").length;
      i++
    ) {
      const name = document.querySelectorAll("#activity-name")[i].value;
      const type = document.querySelectorAll("#activity-type")[i].value;
      const output = document.querySelectorAll("#activity-output")[i].value;
      const picName = document.querySelectorAll("#activity-pic")[i].value;
      const activityStart =
        document.querySelectorAll("#activity-start")[i].value;
      const activityEnd = document.querySelectorAll("#activity-end")[i].value;

      if (
        (!name ||
          !type ||
          !output ||
          !picName ||
          !activityStart ||
          !activityEnd) &&
        status == "SUBMITTED"
      ) {
        setModal({
          visible: true,
          message: "Activity " + (i + 1) + ": Fill in the form corectly",
          status: "warning",
        });
        setIsLoading(false);
        setMod(false);
        return false;
      }

      if (
        cekTanggal(activityStart, activityEnd) == false &&
        status == "SUBMITTED"
      ) {
        setMod(false);
        setIsLoading(false);
        setModal({
          visible: true,
          message:
            "End date of Activity " +
            (i + 1) +
            " must be higher than start date",
          status: "warning",
        });
        return false;
      }

      //array activities sebagai array of object penampungan data, sesuai GET API.
      //sedangkan data[`activities[${i}][name]`] sebagai jalur input
      const id = document.querySelectorAll("#activity-id")[i].value;
      if (id) {
        data[`activities[${i}][id]`] = id;
        activities.push({
          id,
          name,
          type,
          output,
          picName,
          activityStart,
          activityEnd,
        });
      } else {
        activities.push({
          name,
          type,
          output,
          picName,
          activityStart,
          activityEnd,
        });
      }

      data["activities"] = activities;

      data[`activities[${i}][name]`] = name;
      data[`activities[${i}][type]`] = type;
      data[`activities[${i}][output]`] = output;
      data[`activities[${i}][picName]`] = picName;
      data[`activities[${i}][activityStart]`] = activityStart;
      data[`activities[${i}][activityEnd]`] = activityEnd;
    }

    //BUDGET
    const budgets = [];
    for (
      let i = 0;
      i < document.querySelectorAll("#budget-activity").length;
      i++
    ) {
      const activity = document.querySelectorAll("#budget-activity")[i].value;
      const need = document.querySelectorAll("#budget-need")[i].value;
      const frequency = document.querySelectorAll("#budget-frequency")[i].value;
      const quantity = document.querySelectorAll("#budget-quantity")[i].value;
      const unit = document.querySelectorAll("#budget-unit")[i].value;
      const unitPrice = document.querySelectorAll("#budget-price")[i].value;
      const total =
        document.querySelectorAll("#budget-quantity")[i].value *
        document.querySelectorAll("#budget-frequency")[i].value *
        document.querySelectorAll("#budget-price")[i].value;
      const paymentFrom = document.querySelectorAll("#budget-payment-from")[i]
        .value;
      const paymentTo =
        document.querySelectorAll("#budget-payment-to")[i].value;

      if (
        (!activity ||
          !need ||
          !frequency ||
          !quantity ||
          !unit ||
          !unitPrice ||
          !paymentFrom ||
          !paymentTo) &&
        status == "SUBMITTED"
      ) {
        setModal({
          visible: true,
          message: "Budget Plan " + (i + 1) + ": Fill in the form corectly",
          status: "warning",
        });
        setIsLoading(false);
        setMod(false);
        return false;
      }

      const id = document.querySelectorAll("#budget-id")[i].value;
      if (id) {
        data[`budgets[${i}][id]`] = id;
        budgets.push({
          id,
          activity,
          need,
          frequency,
          quantity,
          unit,
          unitPrice,
          total,
          paymentFrom,
          paymentTo,
        });
      } else {
        budgets.push({
          activity,
          need,
          frequency,
          quantity,
          unit,
          unitPrice,
          total,
          paymentFrom,
          paymentTo,
        });
      }

      data["budgets"] = budgets;

      data[`budgets[${i}][activity]`] = activity;
      data[`budgets[${i}][need]`] = need;
      data[`budgets[${i}][frequency]`] = frequency ? frequency : 0;
      data[`budgets[${i}][quantity]`] = quantity ? quantity : 0;
      data[`budgets[${i}][unit]`] = unit;
      data[`budgets[${i}][unitPrice]`] = unitPrice ? unitPrice : 0;
      data[`budgets[${i}][total]`] = total;
      data[`budgets[${i}][paymentFrom]`] = paymentFrom;
      data[`budgets[${i}][paymentTo]`] = paymentTo;
    }

    //KEYRESULT
    const keyResults = [];
    for (let i = 0; i < document.querySelectorAll("#kr-key").length; i++) {
      const key = document.querySelectorAll("#kr-key")[i].value;
      const target = document.querySelectorAll("#kr-target")[i].value;
      const unit = document.querySelectorAll("#kr-unit")[i].value;

      if ((!key || !target || !unit) && status == "SUBMITTED") {
        setModal({
          visible: true,
          message: "Key Result " + (i + 1) + ": Fill in the form corectly",
          status: "warning",
        });
        setIsLoading(false);
        setMod(false);
        return false;
      }

      const id = document.querySelectorAll("#kr-id")[i].value;
      if (id) {
        data[`keyResults[${i}][id]`] = id;
        keyResults.push({ id, key, target, unit });
      } else {
        keyResults.push({ key, target, unit });
      }

      data["keyResults"] = keyResults;

      data[`keyResults[${i}][key]`] = key;
      data[`keyResults[${i}][target]`] = target;
      data[`keyResults[${i}][unit]`] = unit;
    }

    console.log("KONTEN", data);

    return data;
  };

  const handlePreview = (event) => {
    if (props.type == "edit") {
      if (!props.id_phase) {
        navigate("/event/submission/proposal/" + props.id);
      } else {
        navigate(
          "/event/submission/proposal/" +
            props.id +
            "/" +
            props.id_phase +
            "/" +
            props.id_innovation
        );
      }
    } else {
      const data = getContent("PREVIEW", "PROPOSAL");
      props.callBack(data);
      if (!props.id_phase) {
        navigate("/event/submission/preview/" + props.id);
      } else {
        navigate(
          "/event/submission/preview/" +
            props.id +
            "/" +
            props.id_phase +
            "/" +
            props.id_innovation
        );
      }
    }
  };
  const handleSubmit = (event) => {
    setModSubmit(false);
    const data = getContent("SUBMITTED", "PROPOSAL");
    if (data != false && props.new == true) {
      data["submission"] = "";
      create_proposal(token, data);
    } else if (data != false) {
      data["submission"] = "";
      update_proposal(token, data);
    }

    event.preventDefault();
  };

  const handleSave = (event) => {
    setMod(false);
    const data = getContent("DRAFT", "PROPOSAL");

    if (data != "falsesave" && props.new == true) {
      data["submission"] = "";
      create_proposal(token, data);
    } else if (data != "falsesave") {
      data["submission"] = "";
      update_proposal(token, data);
    }
    // setIsLoading(false);
    event.preventDefault();
  };

  const create_proposal = async (token, data) => {
    setIsLoading(true);
    try {
      const response = await createProposal(token, data);
      console.log("Create Proposal:", response.data.message);

      if (response.data) {
        navigate("/event/detail/" + props.id, {
          state: {
            visible: true,
            status: "success",
            message: "",
          },
        });
      }
    } catch (err) {
      alert(err.message);
    }
    setIsLoading(false);
  };
  const update_proposal = async (token, data) => {
    setIsLoading(true);
    try {
      const response = await updateProposal(token, data, props.id_innovation);
      console.log("Update Proposal:", response.data);

      if (response.data) {
        navigate("/event/detail/" + props.id, {
          state: {
            visible: true,
            status: "success",
            message: "",
          },
        });
      }
    } catch (err) {
      alert(err.message);
    }
    setIsLoading(false);
  };

  if (props.type == "edit") {
    var text = "Edit";
  } else {
    var text = "Preview";
  }

  return (
    <div style={{ textAlign: "right" }} className={props.className}>
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />

      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />

      <CButton
        color="secondary"
        onClick={handlePreview}
        className=" mx-2 btn-md text-white"
        style={{ width: "130px" }}
      >
        {props.type == "edit" ? "Edit" : "Preview"}
      </CButton>
      {text != "Preview" ? (
        <>
          <CButton
            color="secondary"
            className=" mx-2  btn-md text-white"
            style={{ width: "130px", opacity: "0.3" }}
          >
            Save as Draft
          </CButton>

          <CButton
            color="warning"
            className=" mx-2 btn-md"
            style={{ width: "130px", opacity: "0.3" }}
          >
            Submit
          </CButton>
        </>
      ) : (
        <>
          <CButton
            color="secondary"
            onClick={() => setMod(!mod)}
            className=" mx-2  btn-md text-white"
            style={{ width: "130px" }}
          >
            Save as Draft
          </CButton>

          <Modal
            pesan="Save as draft?"
            input={false}
            callBack={handleSave}
            close={() => setMod(false)}
            visible={mod}
          />
          <CButton
            color="warning"
            onClick={() => setModSubmit(!modSubmit)}
            className=" mx-2 btn-md"
            style={{ width: "130px" }}
          >
            Submit
          </CButton>

          <Modal
            pesan="Are you sure to submit?"
            input={false}
            callBack={handleSubmit}
            close={() => setModSubmit(false)}
            visible={modSubmit}
          />
        </>
      )}
    </div>
  );
};

export default CButtonSubmit;
