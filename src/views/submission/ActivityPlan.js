import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import Input from "src/components/Input";
import Moment from "moment";

const ActivityPlan = (props) => {
  return (
    <div class="mb-3 p-3" style={{ backgroundColor: "#F6F5FF" }}>
      <div class="mb-3">
        <CRow className="">
          <CCol md={6}>
            <input type="hidden" id="activity-id" value={props.value.id} />
            <Input
              id={"activity-name"}
              label="Activity Name"
              type="text"
              placeholder={"Name of activity " + props.i}
              value={props.value.name}
              callBack={props.callBack}
              required
            />
            <Input
              id={"activity-output"}
              label="Output"
              type="text"
              placeholder={"Output of activity " + props.i}
              value={props.value.output}
              callBack={props.callBack}
              required
            />
            <Input
              id={"activity-start"}
              label="Start"
              type="date"
              placeholder=""
              value={Moment(props.value.activityStart).format("YYYY-MM-DD")}
              callBack={props.callBack}
              required
            />
          </CCol>
          <CCol md={6}>
            <Input
              id={"activity-type"}
              label="Activity Type"
              type="select"
              placeholder="select type"
              callBack={props.callBack}
              value={props.value.type}
              opsi={[
                { value: "Design & Preparation", text: "Design & Preparation" },
                { value: "GOOB for Validation", text: "GOOB for Validation" },
                {
                  value: "Customer problem Fit as Validation",
                  text: "Customer problem Fit as Validation",
                },
              ]}
              required
            />
            <Input
              id={"activity-pic"}
              label="PIC"
              type="text"
              placeholder={"PIC of activity " + props.i}
              value={props.value.picName}
              callBack={props.callBack}
              required
            />

            <Input
              id={"activity-end"}
              label="End"
              type="date"
              placeholder=""
              value={Moment(props.value.activityEnd).format("YYYY-MM-DD")}
              callBack={props.callBack}
              required
            />
          </CCol>
        </CRow>
      </div>
    </div>
  );
};

export default ActivityPlan;
