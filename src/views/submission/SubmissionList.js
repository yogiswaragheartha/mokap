import React, { useState, useEffect } from "react";
import { CCol, CContainer, CRow } from "@coreui/react";
import { Link } from "react-router-dom";
import { cilSave, cilDescription } from "@coreui/icons";
import { getAllSubmission, updatePhase } from "src/api/submission";
import { getEvent, getPhase } from "src/api/event";
import CIcon from "@coreui/icons-react";
import Input from "src/components/Input";
import { useSelector } from "react-redux";
import Moment from "moment";
import LoadingData from "src/components/LoadingData";
import LoadingContent from "src/components/LoadingContent";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";

const SubmissionList = () => {
  const token = localStorage.getItem("token_jwt");
  const [inovasi, setInovasi] = useState([]);
  const [listInovasi, setListInovasi] = useState([]);
  const [listFase, setListFase] = useState([]);
  const [listEvent, setListEvent] = useState([]);
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [fase, setFase] = useState([]);
  const [mod, setMod] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const { auth } = useSelector((state) => state);
  const [phaseId, setPhaseId] = useState();
  const [modSubmit, setModSubmit] = useState(false);
  const [innovationId, setInnovationId] = useState();

  const get_submission = async (token) => {
    try {
      const response = await getAllSubmission(token);

      console.log("Innovation", response.data);
      setInovasi(response.data.sort((a, b) => (a.id > b.id ? -1 : 1)) || []);
      setListInovasi(
        response.data.sort((a, b) => (a.id > b.id ? -1 : 1)) || []
      );
      setArrFase(response.data);
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  const setArrFase = (submission) => {
    const arrFases = [];
    if (submission) {
      for (let i = 0; i < submission.length; i++) {
        const arrFase = [];
        if (submission[i].Event) {
          submission[i].Event.Phases.sort((a, b) => (a.id < b.id ? -1 : 1)).map(
            function (item) {
              arrFase.push({ text: item.initial, value: item.id });
            }
          );
        }

        arrFases.push(arrFase);
      }
    }
    setFase(arrFases);
  };

  const get_event = async (token) => {
    try {
      const response = await getEvent(token);

      const arrEvent = [
        {
          value: "",
          text: "All Event",
        },
      ];
      response.data.map(function (item, i) {
        arrEvent.push({ text: item.name, value: item.id });
      });
      setListEvent(arrEvent);
    } catch (error) {
      console.log(error);
    }
  };

  const get_phase = async () => {
    try {
      const response = await getPhase();

      const arrPhase = [
        {
          value: "",
          text: "All Phase",
        },
      ];
      response.data.result.map(function (item, i) {
        arrPhase.push({ text: item.nama, value: item.id });
      });
      setListFase(arrPhase);
    } catch (error) {
      console.log(error);
    }
  };

  const updateKonten = () => {
    const idFase = document.getElementById("filter-fase").value;
    const idEvent = document.getElementById("filter-event").value;
    let selectedInovation = [];
    if (idFase && !idEvent) {
      selectedInovation = listInovasi.filter((c) => c.id_phase == idFase);
    } else if (!idFase && idEvent) {
      selectedInovation = listInovasi.filter((c) => c.eventId == idEvent);
    } else if (idFase && idEvent) {
      selectedInovation = listInovasi.filter(
        (c) => c.id_phase == idFase && c.eventId == idEvent
      );
    } else {
      selectedInovation = listInovasi;
    }
    setInovasi(selectedInovation);
    setArrFase(selectedInovation);
  };

  const updateKontenSearch = () => {
    const search = document.getElementById("filter-search").value;

    const filtered = listInovasi.filter(function (obj) {
      if (obj["title"]) {
        return obj["title"].toLowerCase().includes(search.toLowerCase());
      }
    });

    setInovasi(filtered);
    setArrFase(filtered);
  };

  const handleChangeFase = (event) => {
    setPhaseId(event.target.value);
  };

  const handleSave = (id) => {
    setModSubmit(false);
    update_phase(token, { phaseId: phaseId }, id);
  };

  const update_phase = async (token, phaseId, id) => {
    setIsLoading(true);
    try {
      const response = await updatePhase(token, phaseId, id);
      console.log(phaseId);
      console.log("Update Phase " + id, response.data);
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  useEffect(() => {
    get_submission(token);
    get_event(token);
    // get_phase();
  }, []);

  return (
    <div className="bg-light mt-4 align-items-center">
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      {/* <Modal
        pesan="Are you sure to change the phase?"
        input={false}
        callBack={handleSave(e)}
        close={() => setMod(false)}
        visible={mod}
      /> */}
      <CContainer>
        <h3>List of Submission</h3>
        <CRow>
          <CCol>
            <Input
              type="select"
              id="filter-fase"
              placeholder="Filter by Phase"
              value=""
              opsi={!listFase ? [] : listFase}
              callBack={updateKonten}
            />
          </CCol>
          <CCol>
            <Input
              type="select"
              id="filter-event"
              placeholder="Filter by Event"
              value=""
              opsi={!listEvent ? [] : listEvent}
              callBack={updateKonten}
            />
          </CCol>
          <CCol md={6} className="d-flex justify-content-end">
            <Input
              type="text"
              id="filter-search"
              placeholder="Search"
              value=""
              callBack={updateKontenSearch}
            />
          </CCol>
        </CRow>
        {isLoadingData ? (
          <LoadingData />
        ) : (
          <div className="my-4 table-responsive">
            <table class="table table-striped ">
              <thead className="text-center">
                <tr>
                  <th scope="col">Logo</th>
                  <th scope="col">Innovation Title</th>
                  <th scope="col">Phase</th>
                  <th scope="col">Submit Date</th>
                  <th scope="col">Submission</th>
                  <th scope="col">Event</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              {/* {console.log("->", inovasi)} */}
              <tbody>
                {!inovasi
                  ? ""
                  : inovasi.map(function (item, i) {
                      return (
                        <tr className="align-middle">
                          <td>
                            {item.logo ? (
                              <img
                                className="inovasi"
                                src={item.logo}
                                style={{ objectFit: "cover" }}
                              />
                            ) : (
                              <div
                                className="inovasi bg-primary text-center pt-2 text-white "
                                style={{
                                  objectFit: "cover",
                                  fontSize: "28px",
                                }}
                              >
                                {item.title ? item.title.substr(0, 1) : ""}
                              </div>
                            )}
                          </td>
                          <td>{item.title}</td>
                          <td>
                            {auth.access.includes("changePhase") ? (
                              <Input
                                type="select"
                                id="fase"
                                style={{ maxWidth: "200px" }}
                                value={item.Phase ? item.Phase.id : ""}
                                opsi={fase[i] ? fase[i] : []}
                                callBack={(e) => handleChangeFase(e)}
                              />
                            ) : item.Phase ? (
                              item.Phase.name
                            ) : (
                              ""
                            )}
                          </td>
                          <td className="text-center">
                            {Moment(item.createdAt).format("DD MMM YYYY")}
                          </td>
                          <td className="text-center">{item.stage}</td>
                          <td>{item.Event ? item.Event.name : ""}</td>
                          <td>
                            <CRow>
                              {auth.access.includes("reviewProposal") ? (
                                <CCol className="btn">
                                  {" "}
                                  <Link
                                    to={`/event/submission/approval/${
                                      item.stage == "PROPOSAL"
                                        ? "proposal"
                                        : "report"
                                    }/${item.eventId}/${item.phaseId}/${
                                      item.id
                                    }`}
                                    className="text-decoration-none text-dark"
                                  >
                                    <CIcon icon={cilDescription} size="md" />
                                    View
                                  </Link>
                                </CCol>
                              ) : (
                                ""
                              )}
                              {auth.access.includes("changePhase") ? (
                                <CCol
                                  md={7}
                                  className="btn"
                                  onClick={() => {
                                    setInnovationId(item.id);
                                    setModSubmit(true);
                                  }}
                                >
                                  <CIcon icon={cilSave} size="md" /> Save Change
                                </CCol>
                              ) : (
                                ""
                              )}
                            </CRow>
                          </td>
                        </tr>
                      );
                    })}
                <Modal
                  pesan={"Are you sure to change the phase?"}
                  input={false}
                  callBack={() => handleSave(innovationId)}
                  close={() => setModSubmit(false)}
                  visible={modSubmit}
                />
              </tbody>
            </table>
          </div>
        )}
      </CContainer>
    </div>
  );
};

export default SubmissionList;
