import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import { useNavigate } from "react-router-dom";

const CPhase = (props) => {
  const navigate = useNavigate();

  const handleViewPhase = (
    id_event,
    id_phase,
    currentPhase,
    stage,
    id_innovation
  ) => {
    let approval = "";
    if (props.isApproval == true) {
      approval = "approval/";
    }
    if (
      (props.page != null && props.page == "proposal") ||
      (id_phase == currentPhase && stage == "PROPOSAL")
    ) {
      navigate(
        "/event/submission/" +
          approval +
          "proposal/" +
          id_event +
          "/" +
          id_phase +
          "/" +
          id_innovation
      );
    } else {
      navigate(
        "/event/submission/" +
          approval +
          "report/" +
          id_event +
          "/" +
          id_phase +
          "/" +
          id_innovation
      );
    }
  };
  return (
    <div
      className=" justify-content-center my-4 pt-4"
      style={{ borderTop: "1px solid #ccc" }}
    >
      <CRow>
        {!props.fase
          ? ""
          : props.fase
              .sort((a, b) => (a.id < b.id ? -1 : 1))
              .map(function (item, i) {
                {
                  if (
                    i == 0 &&
                    props.new == true &&
                    !props.innovation.phaseId
                  ) {
                    //jika baru daftar, fase aktif adalah fase awal
                    var opa = 1;
                  } else if (props.innovation.phase) {
                    //jika sudah terdaftar, maka fase aktif sesuai data
                    if (props.innovation.phase.id == item.id) {
                      var opa = 1;
                    } else if (props.innovation.phaseId >= item.id) {
                      var opa = 0.6;
                    } else {
                      var opa = 0.3;
                    }
                  } else {
                    var opa = 0.5;
                  }

                  return (
                    <CCol md={3}>
                      <div
                        onClick={() => {
                          if (props.innovation) {
                            if (item.id <= props.innovation.phaseId) {
                              handleViewPhase(
                                item.eventId,
                                item.id,
                                props.innovation.phaseId,
                                props.stage,
                                props.id_innovation
                              );
                            }
                          }
                        }}
                        className={`bg-secondary  ${
                          props.innovation
                            ? item.id <= props.innovation.phaseId
                              ? "clickable"
                              : ""
                            : ""
                        } 
                               text-white  p-2 my-1 d-flex align-items-center justify-content-center`}
                        style={{ minHeight: "70px", opacity: opa }}
                      >
                        <h3>{item.initial}</h3>
                      </div>
                    </CCol>
                  );
                }
              })}
      </CRow>
    </div>
  );
};

export default CPhase;
