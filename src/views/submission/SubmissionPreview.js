import React, { useState, useEffect } from "react";
import { CCard, CCol, CContainer, CRow } from "@coreui/react";
import { Link, useNavigate, useParams, useLocation } from "react-router-dom";

import CButtonProposal from "./CButtonProposal";
import CButtonSubmit from "./CButtonSubmit";
import CPhase from "./CPhase";

const SubmissionPreview = (props) => {
  const token = localStorage.getItem("token_jwt");

  const location = useLocation();
  const { id } = useParams();
  const [sub, setSub] = useState(location.state ? location.state.data : {});
  const [innovator, setInnovator] = useState([]);
  const [activity, setActivity] = useState([{}]);
  const [budget, setBudget] = useState([{}]);
  const [keyResult, setKeyResult] = useState([{}]);
  const [pitch, setPitch] = useState();
  const [otherFile, setOtherFile] = useState();
  const [fase, setFase] = useState(
    location.state ? location.state.fase : props.fase
  );
  const [isNewSubmission, setIsNewSubmission] = useState(
    location.state ? location.state.new : props.new
  );
  const [innovations, setInnovations] = useState();

  // useEffect(() => {
  //   if (props.data) {
  //     // console.log("data props", props.data);
  //     // setSub(props.data);
  //     // setInnovator(props.data.members);
  //     // setActivity(props.data.activities);
  //     // setBudget(props.data.budgets);
  //     // setKeyResult(props.data.keyResults);
  //     // if (props.data.PitchDecks) {
  //     //   if (props.data.PitchDecks.length > 0) {
  //     //     setPitch(props.data.PitchDecks[0].pitchDeck);
  //     //     setOtherFile(props.data.PitchDecks[0].otherFile);
  //     //   }
  //     // }
  //   } else {
  //     setFase(location.state.fase);
  //     setSub(location.state.data);
  //     setInnovator(location.state.data.members || location.state.data.Members);
  //     setActivity(
  //       location.state.data.activities || location.state.data.Activities
  //     );
  //     setBudget(location.state.data.budgets || location.state.data.Budgets);
  //     setKeyResult(
  //       location.state.data.keyResults || location.state.data.KeyResults
  //     );

  //     // if (location.state.data.pitchDeck || location.state.data.other) {
  //     setPitch(
  //       location.state.data.pitchDeck
  //         ? URL.createObjectURL(location.state.data.pitchDeck)
  //         : ""
  //     );
  //     setOtherFile(
  //       location.state.data.other
  //         ? URL.createObjectURL(location.state.data.other)
  //         : ""
  //     );
  //     // }  else
  //     // console.log("DATA", location.state.data);
  //     if (location.state.data.PitchDecks) {
  //       if (location.state.data.PitchDecks.length > 0) {
  //         setPitch(location.state.data.PitchDecks[0].pitchDeck || "");
  //         setOtherFile(location.state.data.PitchDecks[0].otherFile || "");
  //       }
  //     }
  //   }
  // }, []);

  useEffect(() => {
    if (props.data) {
      setFase(props.fase);
      setSub(props.data);
      setInnovator(props.data.members || []);
      setActivity(props.data.activities || []);
      setBudget(props.data.budgets || []);
      setKeyResult(props.data.keyResults || []);
      if (props.data.pitchDecks) {
        if (props.data.pitchDecks.length > 0) {
          setPitch(props.data.pitchDecks[0].pitchDeck);
          setOtherFile(props.data.pitchDecks[0].otherFile);
        }
      }
    }
  }, [props.data]);

  // useEffect(() => {
  //   setFase(props.fase);
  // }, [props.fase]);

  return (
    <CCard className="shadow border-0 p-4 mb-4">
      <h4 className="text-center">PROPOSAL </h4>
      <CRow>
        <CCol lg={7}>
          <table>
            <tr>
              <td style={{ width: "115px" }}>Innovation Title</td>
              <td>:</td>
              <td>{sub.title}</td>
            </tr>
            <tr>
              <td>Tagline</td>
              <td>:</td>
              <td>{sub.tagline}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>:</td>
              <td>{sub.description}</td>
            </tr>
            <tr>
              <td>Category</td>
              <td>:</td>
              <td>{sub.category || sub.categoryId}</td>
            </tr>
          </table>
        </CCol>
        <CCol lg={5}>
          <table>
            <tr>
              <td style={{ width: "115px" }}>Innovator</td>
              <td>:</td>
              <td>
                <ol className="">
                  {!innovator
                    ? ""
                    : innovator.map(function (item, i) {
                        return (
                          <li>
                            {item.name} / {item.role} / {item.area}
                          </li>
                        );
                      })}
                </ol>
              </td>
            </tr>
            <tr>
              <td>Est Revenue</td>
              <td>:</td>
              <td>IDR {Intl.NumberFormat().format(sub.revenue)}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>:</td>
              <td>{sub.revenueDescription}</td>
            </tr>
          </table>
        </CCol>
      </CRow>
      <CRow>
        <CCol lg={7}>
          <div className="mt-4">
            <h5>PITCH DECK</h5>
            <p className="text-muted text-mini">
              {pitch ? (
                <button
                  className="btn btn-md bg-dark text-white"
                  onClick={() => window.open(pitch)}
                >
                  Open file
                </button>
              ) : (
                "-"
              )}
            </p>
          </div>
        </CCol>
        <CCol lg={5}>
          <div className="mt-4">
            <h5>ADDITIONAL FILE</h5>
            <p className="text-muted text-mini">
              {otherFile ? (
                <button
                  className="btn btn-md bg-dark text-white"
                  onClick={() => window.open(otherFile)}
                >
                  Open file
                </button>
              ) : (
                "-"
              )}
            </p>
          </div>
        </CCol>
      </CRow>
      <div className="mt-4 ">
        <h5>ACTIVITY PLAN</h5>
        <div className="table-responsive">
          <table class="table">
            <thead style={{ backgroundColor: "#F6F5FF" }}>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Activity Name</th>
                <th scope="col">Activity Type</th>
                <th scope="col">Output</th>
                <th scope="col">PIC</th>
              </tr>
            </thead>
            <tbody>
              {!activity
                ? ""
                : activity.map(function (item, i) {
                    return (
                      <tr>
                        <th scope="row">{i + 1}</th>
                        <td>{item.name}</td>
                        <td>{item.type}</td>
                        <td>{item.output}</td>
                        <td>{item.picName}</td>
                      </tr>
                    );
                  })}
            </tbody>
          </table>
        </div>
      </div>
      <div className="mt-4 ">
        <h5>BUDGET PLAN</h5>
        <div className="table-responsive">
          <table class="table ">
            <thead>
              <tr>
                <th rowspan="2" scope="col">
                  #
                </th>
                <th rowspan="2" scope="col">
                  Activity Name
                </th>
                <th rowspan="2" scope="col">
                  Need
                </th>
                <th colspan="4" scope="col" className="text-center">
                  Plan
                </th>
              </tr>
              <tr>
                <th scope="col">Freq</th>
                <th scope="col">Qty</th>
                <th scope="col">Unit Price</th>
                <th scope="col">Sub Total</th>
              </tr>
            </thead>
            <tbody>
              {!budget
                ? ""
                : budget.map(function (item, i) {
                    return (
                      <tr>
                        <th scope="row">{i + 1}</th>
                        <td>{item.activity}</td>
                        <td>{item.need}</td>
                        <td>{item.frequency}</td>
                        <td>{item.quantity}</td>
                        <td>{Intl.NumberFormat().format(item.unitPrice)}</td>
                        <td>
                          {Intl.NumberFormat().format(
                            item.total
                              ? item.total
                              : item.frequency * item.quantity * item.unitPrice
                          )}
                        </td>
                      </tr>
                    );
                  })}
            </tbody>
          </table>
        </div>
      </div>
      <div className="mt-4">
        <h5>OKR</h5>
        <CRow>
          <CCol>
            <i>Objective</i>
            <p>{sub.objective}</p>
          </CCol>
          <CCol>
            <i>Digitalization</i>
            <p>{sub.digitalization}</p>
          </CCol>
          <CCol>
            <i>Digitization</i>
            <p>{sub.digitization}</p>
          </CCol>
        </CRow>

        <table class="table">
          <thead style={{ backgroundColor: "#F6F5FF" }}>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Key Result</th>
              <th scope="col">Target</th>
              <th scope="col">Unit</th>
            </tr>
          </thead>
          <tbody>
            {!keyResult
              ? ""
              : keyResult.map(function (item, i) {
                  return (
                    <tr>
                      <th scope="row">{i + 1}</th>
                      <td>{item.key}</td>
                      <td>{item.target}</td>
                      <td>{item.unit}</td>
                    </tr>
                  );
                })}
          </tbody>
        </table>
      </div>
    </CCard>
  );
};

export default SubmissionPreview;
