import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import Input from "src/components/Input";

const KeyResult = (props) => {
  return (
    <div class="mb-3 p-3" style={{ backgroundColor: "#F6F5FF" }}>
      <div class="mb-3">
        <input type="hidden" id="kr-id" value={props.value.id} />

        <CRow className="">
          <CCol md={6}>
            <Input
              id={"kr-key"}
              label={"Key Result "}
              type="text"
              placeholder="what you need?"
              value={props.value.key}
              callBack={props.callBack}
              required
            />
          </CCol>
          <CCol md={3}>
            <Input
              id={"kr-target"}
              label="Target"
              type="text"
              placeholder="what you need?"
              value={props.value.target}
              callBack={props.callBack}
              required
            />
          </CCol>
          <CCol md={3}>
            <Input
              id={"kr-unit"}
              label="Unit"
              type="select"
              placeholder=""
              value={props.value.unit}
              callBack={props.callBack}
              opsi={[
                { value: "pcs", text: "pcs" },
                { value: "%", text: "%" },
                { value: "unit", text: "unit" },
                { value: "user", text: "user" },
                { value: "fitur", text: "fitur" },
              ]}
              required
            />
          </CCol>
        </CRow>
      </div>
    </div>
  );
};

export default KeyResult;
