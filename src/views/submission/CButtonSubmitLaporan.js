import React, { useState, useEffect } from "react";
import { CCol, CRow, CButton } from "@coreui/react";
import Input from "src/components/Input";
import { useNavigate } from "react-router-dom";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import LoadingContent from "src/components/LoadingContent";
import { createReport } from "src/api/submission";

const CButtonSubmitLaporan = (props) => {
  const token = localStorage.getItem("token_jwt");
  const navigate = useNavigate();
  const [modSubmit, setModSubmit] = useState(false);
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });
  const [isLoading, setIsLoading] = useState(false);

  const getContent = () => {
    let pitchDeck = document.getElementById("pitchdeck").files[0];

    let pitchDeckUrl;
    if (!pitchDeck) {
      if (props.data) {
        if (props.data.pitchDecks) {
          if (props.data.pitchDecks.length > 0) {
            pitchDeck = props.data.pitchDecks[0].pitchDeck;
            pitchDeckUrl =
              props.data.pitchDecks[0].pitchDeckUrl ||
              props.data.pitchDecks[0].pitchDeck;
          } else {
            pitchDeckUrl = null;
          }
        }
      }
    } else {
      pitchDeckUrl = URL.createObjectURL(pitchDeck);
    }

    let otherFile = document.getElementById("additional-file").files[0];

    let otherFileUrl;
    if (!otherFile) {
      if (props.data) {
        if (props.data.pitchDecks) {
          if (props.data.pitchDecks.length > 0) {
            otherFile = props.data.pitchDecks[0].otherFile;
            otherFileUrl =
              props.data.pitchDecks[0].otherFileUrl ||
              props.data.pitchDecks[0].otherFile;
          } else {
            otherFileUrl = null;
          }
        }
      }
    } else {
      otherFileUrl = URL.createObjectURL(otherFile);
    }

    const data = {
      id: props.data.id,
      phaseId: props.data.phaseId,
      pitchDeck: pitchDeck,
      other: otherFile,
      otherFile: otherFile,
      pitchDeckUrl,
      otherFileUrl,
    };

    console.log("KONTEN REPORT", data);
    return data;
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setModSubmit(false);

    const data = getContent();

    console.log("KONTEN", data);
    create_report(token, data);
  };

  const handlePreview = async (event) => {
    if (props.type == "edit") {
      navigate(
        "/event/submission/report/" +
          props.id +
          "/" +
          props.id_phase +
          "/" +
          props.id_innovation
      );
    } else {
      const data = props.data;
      data["pitchDecks"] = [getContent()];
      props.callBack(data);
      navigate(
        "/event/submission/reportpreview/" +
          props.id +
          "/" +
          props.id_phase +
          "/" +
          props.id_innovation
      );
    }

    event.preventDefault();
  };

  const create_report = async (token, data) => {
    setIsLoading(true);
    try {
      const response = await createReport(token, data);
      console.log("Create Report:", response.data);

      if (response.data) {
        navigate("/event/detail/" + props.id, {
          state: {
            visible: true,
            status: "success",
            message: "",
          },
        });
      }
    } catch (err) {
      alert(err.message);
    }
    setIsLoading(false);
  };

  if (props.type == "edit") {
    var text = "Edit";
  } else {
    var text = "Preview";
  }

  return (
    <div style={{ textAlign: "right" }} className={props.className}>
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />

      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <CButton
        color="secondary"
        onClick={handlePreview}
        className=" mx-2 btn-md text-white"
        style={{ width: "130px" }}
        hidden={text == "hidden" ? true : false}
      >
        {text}
      </CButton>

      <CButton
        color="warning"
        onClick={() => setModSubmit(!modSubmit)}
        className=" mx-2 btn-md"
        style={{ width: "130px" }}
      >
        Submit
      </CButton>
      <Modal
        pesan="Are you sure to submit?"
        input={false}
        callBack={handleSubmit}
        close={() => setModSubmit(false)}
        visible={modSubmit}
      />
    </div>
  );
};

export default CButtonSubmitLaporan;
