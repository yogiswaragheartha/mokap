import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import axios from "axios";
import { Link, useNavigate, useParams, useLocation } from "react-router-dom";
// import { signIn } from "../../state-redux/actionCreators";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { getUserMokap, updateUserMokap } from "src/api/user";
import { getEvent } from "src/api/event";
import Input from "src/components/Input";
import CButtonProposal from "./CButtonProposal";
import CButtonSubmitLaporan from "./CButtonSubmitLaporan";
import CPhase from "./CPhase";
import Moment from "moment";

const LaporanPreview = (props) => {
  const token = localStorage.getItem("token_jwt");
  const { id } = useParams();
  const location = useLocation();
  const [event, setEvent] = useState([]);
  const [fase, setFase] = useState([]);
  const [pitch, setPitch] = useState();
  const [otherFile, setOtherFile] = useState();
  const [sub, setSub] = useState({});

  var totalRealisasi = 0;

  useEffect(() => {
    setSub(props.data || [{}]);
    // setFase(props.fase || [{}]);
    if (props.data.pitchDecks) {
      if (props.data.pitchDecks.length > 0) {
        setPitch(
          props.data.pitchDecks[props.data.pitchDecks.length - 1].pitchDeck
        );
        setOtherFile(
          props.data.pitchDecks[props.data.pitchDecks.length - 1].otherFile
        );

        if (
          props.data.pitchDecks[props.data.pitchDecks.length - 1].pitchDeckUrl
        ) {
          setPitch(
            props.data.pitchDecks[props.data.pitchDecks.length - 1].pitchDeckUrl
          );
        }
        if (
          props.data.pitchDecks[props.data.pitchDecks.length - 1].otherFileUrl
        ) {
          setOtherFile(
            props.data.pitchDecks[props.data.pitchDecks.length - 1].otherFileUrl
          );
        }
      }
    }
  }, []);

  return (
    <CCard className="shadow border-0 p-4 mb-4">
      <h3 className="text-center mb-5">REPORTING</h3>

      <CRow>
        <CCol lg={7}>
          <table>
            <tr>
              <td style={{ width: "120px" }}>Innovation Title</td>
              <td>:</td>
              <td>{sub.title}</td>
            </tr>
            <tr>
              <td>Tagline</td>
              <td>:</td>
              <td>{sub.tagline}</td>
            </tr>
            <tr>
              <td>Description</td>
              <td>:</td>
              <td>{sub.description}</td>
            </tr>
            <tr>
              <td>Category</td>
              <td>:</td>
              <td>{sub.categoryId}</td>
            </tr>
          </table>
        </CCol>
        <CCol lg={5}>
          <table>
            <tr>
              <td style={{ width: "130px" }}>Innovator</td>
              <td>:</td>
              <td>
                <ol className="">
                  {!sub.members
                    ? ""
                    : sub.members.map(function (item, i) {
                        return (
                          <li>
                            {item.name} / {item.role} / {item.area}
                          </li>
                        );
                      })}
                </ol>
              </td>
            </tr>
            <tr>
              <td>Current Revenue</td>
              <td>:</td>
              <td>
                IDR {Intl.NumberFormat().format(sub.revenue ? sub.revenue : 0)}
              </td>
            </tr>
          </table>
        </CCol>
      </CRow>
      <CRow>
        <CCol lg={7}>
          <div className="my-5">
            <h5>PITCH DECK</h5>
            <p>
              {pitch ? (
                <button
                  className="btn btn-md bg-dark text-white"
                  onClick={() => window.open(pitch)}
                >
                  Open file
                </button>
              ) : (
                "-"
              )}
            </p>
          </div>
        </CCol>
        <CCol lg={5}>
          <div className="my-5">
            <h5>ADDITIONAL FILE</h5>
            <p>
              {otherFile ? (
                <button
                  className="btn btn-md bg-dark text-white"
                  onClick={() => window.open(otherFile)}
                >
                  Open file
                </button>
              ) : (
                "-"
              )}
            </p>
          </div>
        </CCol>
      </CRow>

      <div className="my-4">
        <h5>ACTIVITY</h5>
        <div className="table-responsive">
          <table className="table mt-4">
            <thead style={{ backgroundColor: "#F6F5FF" }}>
              <tr>
                <th scope="col">Activity Name</th>
                <th scope="col">Activity Type</th>
                <th scope="col">Output</th>
                <th scope="col">PIC</th>
                <th scope="col">Progress</th>
              </tr>
            </thead>
            <tbody>
              {console.log(sub)}
              {sub.activities
                ? sub.activities.map(function (item, i) {
                    return (
                      <tr>
                        <td>{item.name}</td>
                        <td>{item.type}</td>
                        <td>{item.output}</td>
                        <td>{item.picName}</td>
                        <td>{item.progress}%</td>
                      </tr>
                    );
                  })
                : ""}
            </tbody>
          </table>
        </div>
      </div>
      <div>
        <h5>BUDGET PLAN</h5>
        <div className="table-responsive">
          <table className="table my-4">
            <thead style={{ backgroundColor: "#F6F5FF" }}>
              <tr>
                <th scope="col" rowspan={2}>
                  Activity Name
                </th>
                <th scope="col" rowspan={2}>
                  Need
                </th>
                <th
                  scope="col"
                  colspan={4}
                  className="text-center bg-secondary"
                >
                  Plan
                </th>

                <th
                  scope="col"
                  colspan={4}
                  className="text-center bg-light border"
                >
                  Update / Actual
                </th>
              </tr>
              <tr>
                <th className="bg-secondary border">Freq</th>
                <th className="bg-secondary border">Qty</th>
                <th className="bg-secondary border">Unit Price</th>
                <th className="bg-secondary border">Sub Total</th>
                <th className="bg-light border">Freq</th>
                <th className="bg-light border">Qty</th>
                <th className="bg-light border">Unit Price</th>
                <th className="bg-light border">Sub Total</th>
              </tr>
            </thead>
            <tbody>
              {sub.budgets
                ? sub.budgets.map(function (item, i) {
                    totalRealisasi =
                      totalRealisasi +
                      item.unitPriceActual *
                        item.quantityActual *
                        item.frequencyActual;
                    return (
                      <tr>
                        <td>{item.activity}</td>
                        <td>{item.need}</td>
                        <td>{item.frequency}</td>
                        <td>{item.quantity}</td>
                        <td>{Intl.NumberFormat().format(item.unitPrice)}</td>
                        <td>
                          {Intl.NumberFormat().format(
                            item.unitPrice * item.quantity * item.frequency
                          )}
                        </td>
                        <td>{item.frequencyActual}</td>
                        <td>{item.quantityActual}</td>
                        <td>
                          {Intl.NumberFormat().format(item.unitPriceActual)}
                        </td>
                        <td>
                          {Intl.NumberFormat().format(
                            item.unitPriceActual *
                              item.quantityActual *
                              item.frequencyActual
                          )}
                        </td>
                      </tr>
                    );
                  })
                : ""}
              <tr>
                <th colspan={6}></th>
                <th>Total</th>
                <th colspan={2}></th>
                <th>{Intl.NumberFormat().format(totalRealisasi)}</th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div className="my-4">
        <h5>OKR</h5>
        <CRow>
          <CCol>
            <i>Objective</i>
            <p>{sub.objective}</p>
          </CCol>
          <CCol>
            <i>Digitalization</i>
            <p>{sub.digitalization}</p>
          </CCol>
          <CCol>
            <i>Digitization</i>
            <p>{sub.digitization}</p>
          </CCol>
        </CRow>
        <div className="table-responsive">
          <table class="table">
            <thead style={{ backgroundColor: "#F6F5FF" }}>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Key Result</th>
                <th scope="col">Target</th>
                <th scope="col">Unit</th>
                <th scope="col">Realization</th>
              </tr>
            </thead>
            <tbody>
              {!sub.keyResults
                ? ""
                : sub.keyResults.map(function (item, i) {
                    return (
                      <tr>
                        <th scope="row">{i + 1}</th>
                        <td>{item.key}</td>
                        <td>{item.target}</td>
                        <td>{item.unit}</td>
                        <td>{item.realization}</td>
                      </tr>
                    );
                  })}
            </tbody>
          </table>
        </div>
      </div>
      <div className="my-4">
        <h5>REVENUE</h5>
        <CRow>
          <CCol>
            {!sub.revenues
              ? ""
              : sub.revenues.map(function (item, i) {
                  return (
                    <div>
                      <CRow className="border-bottom">
                        <CCol>
                          <table>
                            <tr>
                              <td>Periode Start</td>
                              <td>:</td>
                              <td>
                                {Moment(item.periodStart).format("DD MMM YYYY")}
                              </td>
                            </tr>
                            <tr>
                              <td>Periode End</td>
                              <td>:</td>
                              <td>
                                {Moment(item.periodEnd).format("DD MMM YYYY")}
                              </td>
                            </tr>
                          </table>
                        </CCol>
                        <CCol>
                          Current Revenue/Cost Saving: IDR{" "}
                          {Intl.NumberFormat().format(
                            item.amount ? item.amount : 0
                          )}
                        </CCol>
                      </CRow>
                    </div>
                  );
                })}
          </CCol>
          <CCol md={4}>
            Revenue/Cost Saving Plan: IDR{" "}
            {Intl.NumberFormat().format(sub.revenue)}
          </CCol>
        </CRow>
      </div>
    </CCard>
  );
};

export default LaporanPreview;
