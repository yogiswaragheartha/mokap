import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import Input from "src/components/Input";

const Innovator = (props) => {
  return (
    <div class="mb-3 p-3" style={{ backgroundColor: "#F6F5FF" }}>
      {/* <h6 className="text-left mb-3">Innovator {props.i}</h6> */}
      <div class="mb-3">
        <CRow className="" id="innovator">
          <CCol md={4}>
            <input type="hidden" id="innovator-id" value={props.value.id} />
            <Input
              id={"innovator-name"}
              label="Name"
              type="text"
              placeholder={"Name of innovator " + props.i}
              value={props.value.name}
              callBack={props.callBack}
              required
            />
          </CCol>
          <CCol md={4}>
            <Input
              id={"innovator-role"}
              label="Role"
              type="select"
              placeholder="select Role"
              value={props.value.role}
              callBack={props.callBack}
              opsi={[
                { value: "CEO", text: "CEO" },
                { value: "CMO", text: "CMO" },
                { value: "CTO", text: "CTO" },
              ]}
              required
            />
          </CCol>
          <CCol md={4}>
            <Input
              id={"innovator-area"}
              label="Area"
              type="select"
              placeholder="select Area"
              value={props.value.area}
              callBack={props.callBack}
              opsi={props.area}
              required
            />
          </CCol>
        </CRow>
      </div>
    </div>
  );
};

export default Innovator;
