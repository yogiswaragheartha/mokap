import { CButton } from "@coreui/react";
import { Link, useParams } from "react-router-dom";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const InnovationButton = (props) => {
  const { id } = useParams();

  const navigate = useNavigate();

  const handleActivity = async (event) => {
    // navigate(`/innovation/detail/${id}`);
    props.callback(1);

    event.preventDefault();
  };
  const handleBudget = async (event) => {
    // navigate(`/innovation/detail/budget/${id}`);
    props.callback(2);
    event.preventDefault();
  };
  const handleOKR = async (event) => {
    // navigate(`/innovation/detail/okr/${id}`);
    props.callback(3);
    event.preventDefault();
  };
  const handleRevenue = async (event) => {
    // navigate(`/innovation/detail/revenue/${id}`);
    props.callback(4);
    event.preventDefault();
  };

  const buttonActive = "innovation-button-active innovation-button";
  const buttonNotActive = "innovation-button";
  const item = props.item;
  return (
    <>
      <CButton
        color="warning"
        key={1}
        className={item === 1 ? buttonActive : buttonNotActive}
        id={"1"}
        onClick={handleActivity}
      >
        Activity Plan
      </CButton>
      <CButton
        color="warning"
        key={2}
        className={item === 2 ? buttonActive : buttonNotActive}
        id={"2"}
        onClick={handleBudget}
      >
        Budget Plan
      </CButton>
      <CButton
        color="warning"
        key={3}
        className={item === 3 ? buttonActive : buttonNotActive}
        id={"3"}
        onClick={handleOKR}
      >
        OKR Plan
      </CButton>
      <CButton
        color="warning"
        key={4}
        className={item === 4 ? buttonActive : buttonNotActive}
        id={"4"}
        onClick={handleRevenue}
      >
        Revenue Plan
      </CButton>
    </>
  );
};
export default InnovationButton;
