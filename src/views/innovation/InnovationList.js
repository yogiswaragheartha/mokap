import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
  CTable,
} from "@coreui/react";
import { useNavigate } from "react-router-dom";
import { cilSave, cilDescription } from "@coreui/icons";
import { getInnovation, getArea } from "src/api/submission";
import { getEvent, getPhase } from "src/api/event";
import CIcon from "@coreui/icons-react";
import Input from "src/components/Input";
import LoadingData from "src/components/LoadingData";
import axios from "axios";
import { useLocation } from "react-router-dom";

const InnovationList = () => {
  const token = localStorage.getItem("token_jwt");
  const navigate = useNavigate();
  const [inovasi, setInovasi] = useState([]);
  const [listInovasi, setListInovasi] = useState([]);
  const [listFase, setListFase] = useState([]);
  const [listProgress, setListProgress] = useState([]);
  const [listArea, setListArea] = useState([]);
  const [isLoading, setisLoading] = useState(true);
  const [listEvent, setListEvent] = useState([]);
  const [resetValue, setResetValue] = useState("");

  const [listInovasiFilterEvent, setListInovasiFilterEvent] = useState([]);
  const [listInovasiFilterKonten, setListInovasiFilterKonten] = useState([]);
  const [areaValue, setAreaValue] = useState("");

  const { state } = useLocation();
  const ev = state;
  console.log("state", ev);

  const [filterEventDashboard, setFilterEventDashboard] = useState(ev);

  const get_innovation = async (token) => {
    try {
      if (!ev) {
        setFilterEventDashboard([]);
        console.log("state check", filterEventDashboard);

        const response = await getInnovation(token);
        const inov = response.data.sort((a, b) => (a.id > b.id ? -1 : 1));
        setListInovasiFilterEvent(inov || []);
        setInovasi(inov || []);
        setListInovasi(inov || []);
      } else if (ev) {
        const idEvent = ev.state.eventValue;
        const idArea = ev.state.areaId;
        setFilterEventDashboard(idEvent);
        setAreaValue(idArea);
        console.log("idevent", idEvent);
        console.log("idarea", idArea);

        const response = await getInnovation(token);
        const inov = response.data.sort((a, b) => (a.id > b.id ? -1 : 1));

        if (!idArea && idEvent) {
          setListInovasiFilterEvent(inov.filter((c) => c.eventId == idEvent));
          setListInovasi(inov.filter((c) => c.eventId == idEvent));
          setInovasi(inov.filter((c) => c.eventId == idEvent));
        } else if (idArea && !idEvent) {
          setListInovasiFilterEvent(inov || []);
          setListInovasi(inov.filter((c) => c.User.areaId == idArea));
          setInovasi(inov.filter((c) => c.User.areaId == idArea));
        } else {
          setListInovasiFilterEvent(inov.filter((c) => c.eventId == idEvent));
          setListInovasi(
            inov.filter((c) => c.eventId == idEvent && c.User.areaId == idArea)
          );
          setInovasi(
            inov.filter((c) => c.eventId == idEvent && c.User.areaId == idArea)
          );
        }
        get_phase();
      }
    } catch (error) {
      console.log(error);
    }
    setisLoading(false);
  };

  const get_event = async (token) => {
    try {
      const response = await getEvent(token);

      console.log("Event", response.data);
      const arrEvent = [
        {
          text: "All Events",
          value: "",
        },
      ];
      response.data
        .sort((a, b) => (a.id > b.id ? -1 : 1))
        .map(function (item, i) {
          arrEvent.push({ text: item.name, value: item.id, key: item.id });
        });

      setListEvent(arrEvent);
    } catch (error) {
      console.log(error);
    }
  };

  const get_progress = async () => {
    try {
      const arrProgress = [
        {
          text: "All Progress",
          value: "",
        },
        { text: "<25%", value: 25 },
        { text: "25%-50%", value: 50 },
        { text: "50%-75%", value: 75 },
        { text: ">75%", value: 100 },
      ];

      setListProgress(arrProgress);
      console.log("progress", arrProgress);
    } catch (error) {
      console.log(error);
    }
    navigate("/innovation/list", { replace: true });
  };

  const get_area = async (token) => {
    try {
      const response = await getArea(token);

      const arrArea = [
        {
          value: "",
          text: "All Area",
        },
      ];
      console.log(response.data);
      response.data
        .sort((a, b) => (a.id < b.id ? -1 : 1))
        .map(function (item, i) {
          arrArea.push({ text: item.name, value: item.id, key: item.id });
        });
      setListArea(arrArea);
    } catch (error) {
      console.log(error);
    }
  };

  const updateEvent = () => {
    const idEvent = document.getElementById("filter-event").value;
    const idArea = document.getElementById("filter-area").value;
    const idFase = document.getElementById("filter-fase").value;
    const idProgress = document.getElementById("filter-progress").value;

    console.log("id event", idEvent);
    console.log("id Progress", idProgress);
    console.log("id area", idArea);
    console.log("id Fase", idFase);

    if (idEvent && !idArea) {
      setListInovasiFilterEvent(
        listInovasi.filter((c) => c.eventId == idEvent)
      );
      setInovasi(listInovasi.filter((c) => c.eventId == idEvent));
    } else if (idEvent && idArea) {
      setListInovasiFilterEvent(
        listInovasi.filter((c) => c.eventId == idEvent)
      );
      setListInovasiFilterKonten(
        listInovasi.filter(
          (c) => c.User.areaId == idArea && c.eventId == idEvent
        )
      );
      setInovasi(
        listInovasi.filter(
          (c) => c.eventId == idEvent && c.User.areaId == idArea
        )
      );
    } else if (!idEvent && idArea) {
      setListInovasiFilterEvent(listInovasi);
      setListInovasiFilterKonten(
        listInovasi.filter((c) => c.User.areaId == idArea)
      );
      setInovasi(listInovasi.filter((c) => c.User.areaId == idArea));
    } else if (!idEvent && !idArea) {
      setListInovasiFilterEvent(listInovasi);
      setInovasi(listInovasi);
    }
  };

  const get_phase = async () => {
    try {
      const idEvent = document.getElementById("filter-event").value;
      if (idEvent) {
        const response = await axios.get(
          process.env.REACT_APP_URL_BE + "/events/" + idEvent,
          {
            headers: {
              "X-Access-Token": token,
            },
          }
        );

        const arrPhase = [
          {
            value: "",
            text: "All Phases",
          },
        ];

        response.data.event.Phases.map(function (item, i) {
          arrPhase.push({ text: item.name, value: item.id });
        });
        setListFase(arrPhase);
      } else {
        setListFase([]);
      }
    } catch (error) {
      console.log(error);
    }
  };
  const handleChange = () => {
    const a = [];
    setResetValue(a);
  };
  const handleChangeEvent = () => {
    const idEvent = document.getElementById("filter-event").value;

    setFilterEventDashboard(idEvent);
  };

  const updateKonten = () => {
    const idEvent = document.getElementById("filter-event").value;
    const idFase = document.getElementById("filter-fase").value;
    const idArea = document.getElementById("filter-area").value;
    const idProgress = document.getElementById("filter-progress").value;

    if (!idEvent) {
      if (idArea && !idFase && !idProgress) {
        setListInovasiFilterKonten(
          listInovasi.filter((c) => c.User.areaId == idArea)
        );
        setInovasi(listInovasi.filter((c) => c.User.areaId == idArea));
      } else if (!idArea && idFase && !idProgress) {
        setListInovasiFilterKonten(listInovasi);
        setInovasi(listInovasi);
      } else if (!idArea && !idFase) {
        if (idProgress == 25) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) => parseInt(c.avgProgress) <= 25 || c.avgProgress == null
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) => parseInt(c.avgProgress) <= 25 || c.avgProgress == null
            )
          );
        } else if (idProgress == 50) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 25 && parseInt(c.avgProgress) <= 50
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 25 && parseInt(c.avgProgress) <= 50
            )
          );
        } else if (idProgress == 75) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 50 && parseInt(c.avgProgress) <= 75
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 50 && parseInt(c.avgProgress) <= 75
            )
          );
        } else if (idProgress == 100) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 75 && parseInt(c.avgProgress) <= 100
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 75 && parseInt(c.avgProgress) <= 100
            )
          );
        } else if (!idProgress) {
          setListInovasiFilterKonten(listInovasi);
          setInovasi(listInovasi);
        }
      } else if (idArea && idFase && !idProgress) {
        setListInovasiFilterKonten(
          listInovasi.filter(
            (c) => c.User.areaId == idArea && c.phaseId == idFase
          )
        );
        setInovasi(
          listInovasi.filter(
            (c) => c.User.areaId == idArea && c.phaseId == idFase
          )
        );
      } else if (!idFase) {
        if (idArea && idProgress == 25) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                (c.User.areaId == idArea && parseInt(c.avgProgress) <= 25) ||
                (c.User.areaId == idArea && c.avgProgress == null)
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                (c.User.areaId == idArea && parseInt(c.avgProgress) <= 25) ||
                (c.User.areaId == idArea && c.avgProgress == null)
            )
          );
        } else if (idProgress == 50) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
        } else if (idProgress == 75) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
        } else if (idProgress == 100) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
        } else if (!idProgress) {
          setInovasi(listInovasi.filter((c) => c.User.areaId == idArea));
          setListInovasiFilterKonten(
            listInovasi.filter((c) => c.User.areaId == idArea)
          );
        }
      } else if (!idArea && idFase) {
        if (idProgress == 25) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) => parseInt(c.avgProgress) <= 25 || c.avgProgress == null
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) => parseInt(c.avgProgress) <= 25 || c.avgProgress == null
            )
          );
        } else if (idProgress == 50) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 25 && parseInt(c.avgProgress) <= 50
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 25 && parseInt(c.avgProgress) <= 50
            )
          );
        } else if (idProgress == 75) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 50 && parseInt(c.avgProgress) <= 75
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 50 && parseInt(c.avgProgress) <= 75
            )
          );
        } else if (idProgress == 100) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 75 && parseInt(c.avgProgress) <= 100
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                parseInt(c.avgProgress) > 75 && parseInt(c.avgProgress) <= 100
            )
          );
        } else if (!idProgress) {
          setListInovasiFilterKonten(listInovasi);
          setInovasi(listInovasi);
        }
      } else if (idFase) {
        if (idArea && idProgress == 25) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                (c.User.areaId == idArea && parseInt(c.avgProgress) <= 25) ||
                (c.User.areaId == idArea && c.avgProgress == null)
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                (c.User.areaId == idArea && parseInt(c.avgProgress) <= 25) ||
                (c.User.areaId == idArea && c.avgProgress == null)
            )
          );
        } else if (idProgress == 50) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
        } else if (idProgress == 75) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
        } else if (idProgress == 100) {
          setListInovasiFilterKonten(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
          setInovasi(
            listInovasi.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
        } else if (!idProgress) {
          setListInovasiFilterKonten(
            listInovasi.filter((c) => c.User.areaId == idArea)
          );
          setInovasi(listInovasi.filter((c) => c.User.areaId == idArea));
        }
      }
    }

    //////////////////////////////////////////////////////////////////////////////////////////
    else if (idEvent) {
      if (idArea && !idFase && !idProgress) {
        setListInovasiFilterKonten(
          listInovasiFilterEvent.filter((c) => c.User.areaId == idArea)
        );
        setInovasi(
          listInovasiFilterEvent.filter((c) => c.User.areaId == idArea)
        );
      } else if (!idArea && idFase && !idProgress) {
        setListInovasiFilterKonten(
          listInovasiFilterEvent.filter((c) => c.phaseId == idFase)
        );
        setInovasi(listInovasiFilterEvent.filter((c) => c.phaseId == idFase));
      } else if (!idArea && !idFase) {
        if (idProgress == 25) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) => parseInt(c.avgProgress) <= 25 || c.avgProgress == null
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) => parseInt(c.avgProgress) <= 25 || c.avgProgress == null
            )
          );
        } else if (idProgress == 50) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                parseInt(c.avgProgress) > 25 && parseInt(c.avgProgress) <= 50
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                parseInt(c.avgProgress) > 25 && parseInt(c.avgProgress) <= 50
            )
          );
        } else if (idProgress == 75) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                parseInt(c.avgProgress) > 50 && parseInt(c.avgProgress) <= 75
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                parseInt(c.avgProgress) > 50 && parseInt(c.avgProgress) <= 75
            )
          );
        } else if (idProgress == 100) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                parseInt(c.avgProgress) > 75 && parseInt(c.avgProgress) <= 100
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                parseInt(c.avgProgress) > 75 && parseInt(c.avgProgress) <= 100
            )
          );
        } else if (!idProgress) {
          setListInovasiFilterKonten(listInovasiFilterEvent);
          setInovasi(listInovasiFilterEvent);
        }
      } else if (idArea && idFase && !idProgress) {
        setListInovasiFilterKonten(
          listInovasiFilterEvent.filter(
            (c) => c.User.areaId == idArea && c.phaseId == idFase
          )
        );
        setInovasi(
          listInovasiFilterEvent.filter(
            (c) => c.User.areaId == idArea && c.phaseId == idFase
          )
        );
      } else if (!idFase) {
        if (idArea && idProgress == 25) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                (c.User.areaId == idArea && parseInt(c.avgProgress) <= 25) ||
                (c.User.areaId == idArea && c.avgProgress == null)
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                (c.User.areaId == idArea && parseInt(c.avgProgress) <= 25) ||
                (c.User.areaId == idArea && c.avgProgress == null)
            )
          );
        } else if (idProgress == 50) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
        } else if (idProgress == 75) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
        } else if (idProgress == 100) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
        } else if (!idProgress) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter((c) => c.User.areaId == idArea)
          );
          setInovasi(
            listInovasiFilterEvent.filter((c) => c.User.areaId == idArea)
          );
        }
      } else if (!idArea && idFase) {
        if (idProgress == 25) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                (c.phaseId == idFase && parseInt(c.avgProgress) <= 25) ||
                (c.phaseId == idFase && c.avgProgress == null)
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                (c.phaseId == idFase && parseInt(c.avgProgress) <= 25) ||
                (c.phaseId == idFase && c.avgProgress == null)
            )
          );
        } else if (idProgress == 50) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
        } else if (idProgress == 75) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
        } else if (idProgress == 100) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
        } else if (!idProgress) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter((c) => c.phaseId == idFase)
          );
          setInovasi(listInovasiFilterEvent.filter((c) => c.phaseId == idFase));
        }
      } else if (idFase) {
        if (idArea && idProgress == 25) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                (c.phaseId == idFase &&
                  c.User.areaId == idArea &&
                  parseInt(c.avgProgress) <= 25) ||
                (c.phaseId == idFase &&
                  c.User.areaId == idArea &&
                  c.avgProgress == null)
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                (c.phaseId == idFase &&
                  c.User.areaId == idArea &&
                  parseInt(c.avgProgress) <= 25) ||
                (c.phaseId == idFase &&
                  c.User.areaId == idArea &&
                  c.avgProgress == null)
            )
          );
        } else if (idProgress == 50) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 25 &&
                parseInt(c.avgProgress) <= 50
            )
          );
        } else if (idProgress == 75) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 50 &&
                parseInt(c.avgProgress) <= 75
            )
          );
        } else if (idProgress == 100) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) =>
                c.phaseId == idFase &&
                c.User.areaId == idArea &&
                parseInt(c.avgProgress) > 75 &&
                parseInt(c.avgProgress) <= 100
            )
          );
        } else if (!idProgress) {
          setListInovasiFilterKonten(
            listInovasiFilterEvent.filter(
              (c) => c.phaseId == idFase && c.User.areaId == idArea
            )
          );
          setInovasi(
            listInovasiFilterEvent.filter(
              (c) => c.phaseId == idFase && c.User.areaId == idArea
            )
          );
        }
      }
    }
  };

  const updateKontenSearch = () => {
    const idFase = document.getElementById("filter-fase").value;
    const idArea = document.getElementById("filter-area").value;
    const idProgress = document.getElementById("filter-progress").value;
    const search = document.getElementById("filter-search").value;

    if (idArea || idFase || idProgress) {
      const filtered = listInovasiFilterKonten.filter(function (obj) {
        if (obj["title"]) {
          return obj["title"].toLowerCase().includes(search.toLowerCase());
        }
      });
      setInovasi(filtered);
    } else {
      const filtered = listInovasiFilterEvent.filter(function (obj) {
        if (obj["title"]) {
          return obj["title"].toLowerCase().includes(search.toLowerCase());
        }
      });
      setInovasi(filtered);
    }
  };

  useEffect(() => {
    get_event(token);
    get_innovation(token);
    get_progress();
    get_phase();
    get_area(token);
  }, []);

  var elements = document.getElementsByClassName("clickable");
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    element.addEventListener("click", function () {
      var href = this.dataset.href;
      if (href) {
        window.location.assign(href);
      }
    });
  }

  return (
    <div className="bg-light mt-4 align-items-center">
      <CContainer>
        <h3>Innovation List</h3>
        <CRow className="mb-4">
          <CCol>
            <Input
              type="select"
              id="filter-event"
              placeholder="Filter by Event"
              value={filterEventDashboard}
              opsi={!listEvent ? [] : listEvent}
              callBack={() => {
                updateEvent();
                get_phase();
                handleChange();
                handleChangeEvent();
              }}
            />
          </CCol>
          <CCol>
            <Input
              type="select"
              id="filter-area"
              placeholder="Filter by Area"
              value={areaValue}
              opsi={!listArea ? [] : listArea}
              callBack={updateKonten}
            />
          </CCol>
          <CCol>
            <Input
              type="select"
              id="filter-fase"
              placeholder="Filter by Phase"
              value={resetValue}
              opsi={!listFase ? [] : listFase}
              callBack={updateKonten}
            />
          </CCol>
          <CCol>
            <Input
              type="select"
              id="filter-progress"
              placeholder="Filter by Progress"
              value={resetValue}
              opsi={!listProgress ? [] : listProgress}
              callBack={updateKonten}
            />
          </CCol>
          <CCol>
            <Input
              type="text"
              id="filter-search"
              placeholder="Search"
              value={resetValue}
              callBack={updateKontenSearch}
            />
          </CCol>
        </CRow>
        {isLoading ? (
          <LoadingData />
        ) : (
          <div class="table-responsive shadow bg-body rounded">
            <table class="table table-striped-list-inovasi table-hover">
              <thead class="tablehead-innovation-list">
                <tr>
                  <th
                    class="text-center tablehead-innovation-list-text"
                    scope="col"
                    style={{ padding: "0.5rem 3rem 0.5rem 1rem" }}
                  >
                    Logo
                  </th>
                  <th
                    scope="col"
                    style={{ padding: "0.5rem 1rem" }}
                    class="text-start tablehead-innovation-list-text"
                  >
                    Innovation Title
                  </th>
                  <th
                    scope="col"
                    style={{ padding: "0.5rem 1rem" }}
                    class="text-start tablehead-innovation-list-text"
                  >
                    Phase
                  </th>
                  <th
                    scope="col"
                    style={{ padding: "0.5rem 1rem" }}
                    class="text-start tablehead-innovation-list-text"
                  >
                    Area
                  </th>
                  <th
                    scope="col"
                    style={{ padding: "0.5rem 1rem" }}
                    class="text-start tablehead-innovation-list-text"
                  >
                    Tagline
                  </th>
                  <th
                    scope="col"
                    style={{ padding: "0.5rem 1rem" }}
                    class="text-start tablehead-innovation-list-text"
                  >
                    Event
                  </th>
                  <th
                    scope="col"
                    style={{ padding: "0.5rem 3rem 0.5rem 1rem" }}
                    class="text-center tablehead-innovation-list-text"
                  >
                    Progress
                  </th>
                </tr>
              </thead>
              <tbody>
                {!inovasi
                  ? ""
                  : inovasi.map(function (item, i) {
                      return (
                        <tr
                          class="clickable align-middle"
                          onClick={() => {
                            navigate("/innovation/detail/" + item.id);
                          }}
                        >
                          <td
                            key={item.id}
                            className="justify-content-center"
                            style={{ padding: "0.5rem 3rem 0.5rem 1rem" }}
                          >
                            {item.logo ? (
                              <img
                                className="inovasi text-center"
                                src={item.logo}
                                style={{ objectFit: "cover" }}
                              />
                            ) : (
                              <div
                                className="inovasi bg-primary text-center pt-2 text-white "
                                style={{
                                  objectFit: "cover",
                                  fontSize: "28px",
                                }}
                              >
                                {item.title ? item.title.substr(0, 1) : ""}
                              </div>
                            )}
                          </td>
                          <td
                            style={{ padding: "0.5rem 1rem" }}
                            class="text-start"
                          >
                            {item.title}
                          </td>
                          <td
                            style={{ padding: "0.5rem 1rem" }}
                            class="text-start"
                          >
                            {item.Phase ? item.Phase.name : ""}
                          </td>
                          <td
                            style={{ padding: "0.5rem 1rem" }}
                            class="text-start"
                          >
                            {item.User ? item.User.Area.name : ""}
                          </td>
                          <td
                            style={{ padding: "0.5rem 1rem" }}
                            class="text-start"
                          >
                            {item.tagline}
                          </td>
                          <td
                            style={{ padding: "0.5rem 1rem" }}
                            class="text-start"
                          >
                            {item.Event ? item.Event.name : ""}
                          </td>

                          <td
                            className={
                              item.avgProgress <= 25
                                ? "text-red text-center"
                                : item.avgProgress <= 50
                                ? "text-orange text-center"
                                : item.avgProgress <= 75
                                ? "text-yellow text-center"
                                : "text-green text-center"
                            }
                            style={{ padding: "0.5rem 3rem 0.5rem 1rem" }}
                          >
                            {item.avgProgress
                              ? Math.round(item.avgProgress)
                              : 0}
                            %
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </div>
        )}
      </CContainer>
    </div>
  );
};

export default InnovationList;
