import { CTable } from "@coreui/react";
import { Link } from "react-router-dom";
function InnovatorList(props) {
  return (
    <CTable responsive striped hover color="light" className="mt-2">
      <thead className="text-start">
        <tr>
          <th scope="col" className="tableheadinnovation">
            Nama
          </th>
          <th scope="col" className="tableheadinnovation">
            Role
          </th>
          <th scope="col" className="tableheadinnovation">
            Area
          </th>
        </tr>
      </thead>
      <tbody>
        {!props.member
          ? ""
          : props.member.map(function (item, i) {
              return (
                <tr className="align-middle">
                  <td>{item.name}</td>
                  <td>{item.role}</td>
                  <td>{item.area}</td>
                </tr>
              );
            })}
      </tbody>
    </CTable>
  );
}

export default InnovatorList;
