import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CNavLink,
  CCardImage,
  CCardText,
  CCardTitle,
  CRow,
  CAccordion,
  CTable,
} from "@coreui/react";
import { CSpinner } from "@coreui/react";

function Detail(props) {
  return (
    <CRow>
      <CCol lg={4} className="mb-2 justify-content-center">
        <div class="card" className="card-inovasi">
          <div class="card-body h-100">
            <CRow className="h-100 align-items-center">
              <CCol lg={4} className=" d-flex justify-content-center ">
                {props.inovasi.logo ? (
                  <img
                    className="inovasi ms-2"
                    src={props.inovasi.logo}
                    style={{ objectFit: "cover" }}
                  />
                ) : (
                  <div
                    className="inovasi bg-primary text-center pt-2 text-white "
                    style={{
                      objectFit: "cover",
                      fontSize: "28px",
                    }}
                  >
                    {props.inovasi.title
                      ? props.inovasi.title.substr(0, 1)
                      : ""}
                  </div>
                )}
              </CCol>
              <CCol md={7} lg={8} xl={8}>
                <div class="row">
                  {props.inovasi.title ? (
                    <h5 class="text-start fw-bold text-black">
                      {props.inovasi.title}
                    </h5>
                  ) : (
                    ""
                  )}
                </div>
                <div class="row">
                  <h6 class="text-start fw-bold text-black-50">
                    {props.inovasi.tagline}
                  </h6>
                </div>
              </CCol>
            </CRow>
          </div>
        </div>
      </CCol>
      <CCol className="mb-2 justify-content-center">
        <div class="card" className="card-inovasi-2">
          <div class="card-body">
            <CRow>
              <CCol>
                <div class="row ">
                  <h5 class="text-center fw-bold text-black-50">
                    Current Phase
                  </h5>
                </div>
                <div className="row text-center">
                  {props.inovasi.Phase ? (
                    <h1 class="text-center fw-bold text-black">
                      {props.inovasi.Phase.initial}
                    </h1>
                  ) : (
                    ""
                  )}
                </div>
              </CCol>
            </CRow>
          </div>
        </div>
      </CCol>
      <CCol className="mb-2 justify-content-center">
        <div class="card" className="card-inovasi-2">
          <div class="card-body">
            <CRow>
              <CCol class="">
                <div class="row">
                  <h5 class="text-center fw-bold text-black-50">
                    Current Progress
                  </h5>
                </div>
                <div class="row">
                  <h1 class="text-center fw-bold text-black">
                    {props.progress ? props.progress + " %" : 0 + " %"}
                  </h1>
                </div>
              </CCol>
            </CRow>
          </div>
        </div>
      </CCol>
      <CCol className="mb-2 justify-content-center">
        <div class="card" className="card-inovasi-2">
          <div class="card-body">
            <CRow>
              <CCol class="">
                <div class="row">
                  <h5 class="text-center fw-bold text-black-50">AREA</h5>
                </div>
                <div class="row">
                  {props.inovasi ? (
                    props.inovasi.Members ? (
                      <h4 class="text-center fw-bold text-black">
                        {props.inovasi.Members[0]
                          ? props.inovasi.Members[0].area
                          : ""}
                      </h4>
                    ) : (
                      ""
                    )
                  ) : (
                    ""
                  )}
                </div>
              </CCol>
            </CRow>
          </div>
        </div>
      </CCol>
    </CRow>
  );
}
export default Detail;
