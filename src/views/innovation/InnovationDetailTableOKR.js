import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CNavLink,
  CCardImage,
  CCardText,
  CCardTitle,
  CRow,
  CAccordion,
  CTable,
} from "@coreui/react";
import { isPropsEqual } from "@fullcalendar/react";
function InnovationDetailTableOKR(props) {
  return (
    <CRow>
      <CCol md={12} lg={12} xl={12}>
        <CCard className="p-4">
          <CRow>
            <CCol xl={4}>
              <CCard className="py-4" style={{ border: "none" }}>
                <CCardText
                  style={{
                    fontWeight: "bold",
                    marginBottom: "0px",
                    paddingLeft: "1rem",
                    paddingRight: "1rem",
                  }}
                >
                  Objective
                </CCardText>
                <CCardText
                  style={{
                    paddingLeft: "1rem",
                    paddingRight: "1rem",
                  }}
                >
                  {props.objective}
                </CCardText>
              </CCard>
            </CCol>
            <CCol xl={4}>
              <CCard className="py-4" style={{ border: "none" }}>
                <CCardText
                  style={{
                    fontWeight: "bold",
                    marginBottom: "0px",
                    paddingLeft: "1rem",
                    paddingRight: "1rem",
                  }}
                >
                  Digitalization
                </CCardText>
                <CCardText
                  style={{
                    paddingLeft: "1rem",
                    paddingRight: "1rem",
                  }}
                >
                  {props.digitalization}
                </CCardText>
              </CCard>
            </CCol>
            <CCol xl={4}>
              <CCard className="py-4" style={{ border: "none" }}>
                <CCardText
                  style={{
                    fontWeight: "bold",
                    marginBottom: "0px",
                    paddingLeft: "1rem",
                    paddingRight: "1rem",
                  }}
                >
                  Digitization
                </CCardText>
                <CCardText
                  style={{
                    paddingLeft: "1rem",
                    paddingRight: "1rem",
                  }}
                >
                  {props.digitization}
                </CCardText>
              </CCard>
            </CCol>
          </CRow>
          <CRow className="table-responsive">
            <table class="table-okr">
              <thead>
                <tr>
                  <th>Key Result</th>
                  <th>Target</th>
                  <th>Unit</th>
                </tr>
              </thead>

              {props.okr.map(function (item, i) {
                return (
                  <tr>
                    <td>{item.key}</td>
                    <td>{item.target}</td>
                    <td>{item.unit}</td>
                  </tr>
                );
              })}
            </table>
          </CRow>
        </CCard>
      </CCol>
    </CRow>
  );
}
export default InnovationDetailTableOKR;
