import { CCard, CCol, CRow } from "@coreui/react";
import React, { useState, useEffect } from "react";

function InnovationDetailTableBudget(props) {
  const [total, setTotal] = useState(0);

  useEffect(() => {
    let tot = 0;
    props.budget.map(function (item, i) {
      tot += total + item.frequency * item.quantity * item.unitPrice;
    });
    setTotal(tot);
  }, [props.budget]);

  return (
    <CRow>
      <CCol md={12} lg={12} xl={12}>
        <CCard className="p-4 table-responsive">
          <table class="table-budget ">
            <thead>
              <tr>
                <th rowspan="2" colspan="2">
                  Activity Name
                </th>
                <th rowspan="2" colspan="2">
                  Need
                </th>
                <th rowspan="1" colspan="4" className="table-budget3">
                  Plan
                </th>
              </tr>
              <tr>
                <th className="table-budget2 ">Freq.</th>
                <th className="table-budget2">Qty</th>
                <th className="table-budget2">Unit Price</th>
                <th className="table-budget3">Sub Total</th>
              </tr>
            </thead>
            <tbody>
              {!props.budget
                ? ""
                : props.budget.map(function (item, i) {
                    return (
                      <tr>
                        <td colspan="2">{item.activity}</td>
                        <td colspan="2">{item.need}</td>
                        <td colspan="1" class="text-center">
                          {item.frequency}
                        </td>
                        <td colspan="1" class="text-center">
                          {item.quantity}
                        </td>
                        <td colspan="1" class="text-center">
                          {Intl.NumberFormat().format(item.unitPrice)}
                        </td>
                        <td colspan="1" class="text-center">
                          {Intl.NumberFormat().format(
                            item.frequency * item.quantity * item.unitPrice
                          )}
                        </td>
                      </tr>
                    );
                  })}

              <tr>
                <td colspan="2" className="table-budget-total"></td>
                <td colspan="2" className="table-budget-total"></td>
                <td colspan="1" className="table-budget-total">
                  Total
                </td>
                <td colspan="1" className="table-budget-total"></td>
                <td colspan="1" className="table-budget-total"></td>
                <td colspan="1" className="table-budget-total">
                  {Intl.NumberFormat().format(total)}
                </td>
              </tr>
            </tbody>
          </table>
        </CCard>
      </CCol>
    </CRow>
  );
}

export default InnovationDetailTableBudget;
