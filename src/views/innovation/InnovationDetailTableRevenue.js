import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CNavLink,
  CCardImage,
  CCardText,
  CCardTitle,
  CRow,
  CAccordion,
  CTable,
} from "@coreui/react";
function InnovationDetailTableRevenue(props) {
  return (
    <>
      <CRow>
        <CCol md={12} lg={12} xl={12}>
          <CCard className="p-4">
            <CRow className="justify-content-center my-4">
              <CCol xl={4}>
                <CCard className="p-4 mt-4 card-revenue">
                  <CCardTitle className="text-center font-weight-bold">
                    <h3
                      style={{
                        fontWeight: "bold",
                        marginBottom: "0",
                      }}
                    >
                      IDR {Intl.NumberFormat().format(props.revenue)}
                    </h3>
                  </CCardTitle>
                </CCard>
                <CCard className="mt-4" style={{ border: "none" }}>
                  <CCardTitle>
                    <h4>Description</h4>
                  </CCardTitle>
                  <CCardText>
                    <p
                      style={{
                        textAlign: "justify",
                        textJustify: "inter-word",
                      }}
                    >
                      {props.description}
                    </p>
                  </CCardText>
                </CCard>
              </CCol>
            </CRow>
          </CCard>
        </CCol>
      </CRow>
      <CRow>
        <CCol md={12} lg={12} xl={12}>
          <CCard className="p-4"></CCard>
        </CCol>
      </CRow>
    </>
  );
}

export default InnovationDetailTableRevenue;
