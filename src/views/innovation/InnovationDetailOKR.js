import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CNavLink,
  CCardImage,
  CCardText,
  CCardTitle,
  CRow,
  CAccordion,
  CTable,
} from "@coreui/react";
import axios from "axios";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import CIcon from "@coreui/icons-react";
import { cilMediaPlay, cilMediaStop, cilAddressBook } from "@coreui/icons";

import { getEvent } from "src/api/event";
import { getInnovation } from "src/api/innovation";
import EventJoined from "./EventJoined";
import InnovatorList from "./InnovatorList";
import Detail from "./Detail";
import InnovationButton from "./InnovationButton";
import InnovationDetailTableOKR from "./InnovationDetailTableOKR";

const InnovationDetailOKR = () => {
  const [inovasi, setInovasi] = useState([]);
  const [event, setEvent] = useState([]);
  const [ev, setEv] = useState();

  const { id } = useParams();

  const get_inovasi = async (idinovasi) => {
    try {
      const response = await getInnovation(idinovasi);

      console.log("Inovasi", response.data.result[0]);
      console.log("ev", response.data.result[0].id_event);

      setInovasi(response.data.result[0]);
      setEv(response.data.result[0].id_event);
    } catch (error) {
      console.log(error);
    }
  };

  const get_event = async () => {
    try {
      const response = await getEvent();

      console.log("Event", response.data.items);

      setEvent(response.data.result || []);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    get_inovasi(id);
    get_event();
  }, []);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row ">
      <CContainer>
        <CRow className="">
          <CRow>
            <h3>Innovation Detail</h3>
          </CRow>

          <CCol md={12} lg={12} xl={12}>
            <CCard className="mt-2 border-0">
              <>
                <Detail key={inovasi.id} inovasi={inovasi} />
                <CRow>
                  <CCol md={12} lg={12} xl={12}>
                    <CCardTitle className="fw-bold text-black  mt-4">
                      Description
                    </CCardTitle>
                    <CCardText className="my-4">
                      <p className="" style={{ textAlign: "justify" }}>
                        {inovasi.ket}
                      </p>
                    </CCardText>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol md={12} lg={12} xl={12}>
                    <CCardTitle className="fw-bold text-black  mt-4">
                      Innovator
                    </CCardTitle>
                    <CCardText>
                      <div className="my-4 table-responsive">
                        <InnovatorList />
                      </div>
                    </CCardText>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol md={12} lg={12} xl={12}>
                    <CCardTitle className="fw-bold text-black  mt-4">
                      Event Joined
                    </CCardTitle>
                    <CCardText>
                      {event
                        .filter(function (event) {
                          return event.id == ev;
                        })
                        .map(function (event) {
                          return <EventJoined key={event.id} event={event} />;
                        })}
                    </CCardText>
                  </CCol>
                </CRow>
                <CRow>
                  <CCol md={12} lg={12} xl={12}>
                    <CCardTitle className="fw-bold text-black  mt-4">
                      Summary
                    </CCardTitle>
                    <CRow>
                      <div
                        className="d-grid gap-3 d-flex justify-content-center"
                        style={{ width: "100%" }}
                      >
                        <InnovationButton item={"3"} />
                      </div>
                    </CRow>
                    <InnovationDetailTableOKR />
                  </CCol>
                </CRow>
              </>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default InnovationDetailOKR;
