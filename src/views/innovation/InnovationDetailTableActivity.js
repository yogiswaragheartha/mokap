import React, { useState, useEffect, useRef } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CToast,
  CToastHeader,
  CToastBody,
  CToaster,
  CForm,
  CFormInput,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
function InnovationDetailTableActivity(props) {
  const [toast, addToast] = useState(0);
  const toaster = useRef();

  const handleDate = (e) => {
    // alert("a");
    console.log("event Cal", e.event);
    // alert(e.event.title);
    addToast(
      <CToast className="mt-2">
        <CToastHeader closeButton>
          <svg
            className="rounded me-2"
            width="20"
            height="20"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="xMidYMid slice"
            focusable="false"
            role="img"
          >
            <rect
              width="100%"
              height="100%"
              fill={e.event.backgroundColor}
            ></rect>
          </svg>
          <strong className="me-auto">{e.event._def.extendedProps.type}</strong>
        </CToastHeader>
        <CToastBody>
          <table>
            <tr>
              <td>Activity</td>
              <td>:</td>
              <td>{e.event._def.extendedProps.name}</td>
            </tr>
            <tr>
              <td>PIC</td>
              <td>:</td>
              <td>{e.event.title}</td>
            </tr>
            <tr>
              <td>Output</td>
              <td>:</td>
              <td>{e.event._def.extendedProps.output}</td>
            </tr>
            <tr>
              <td>Start</td>
              <td>:</td>
              <td>{e.event._def.extendedProps.activityStart}</td>
            </tr>
            <tr>
              <td>End</td>
              <td>:</td>
              <td>{e.event._def.extendedProps.activityEnd}</td>
            </tr>
          </table>{" "}
        </CToastBody>
      </CToast>
    );
  };

  return (
    <CRow>
      <CCol>
        <CCard className="p-4">
          <CRow className="align-items-center justify-content-center">
            <CCol lg={3} className="m-4 order-1">
              <table class="table-activity">
                <thead>
                  <tr>
                    <th rowspan="1" colspan="1">
                      Clr
                    </th>
                    <th rowspan="1" colspan="5">
                      Activity Plan
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {!props.activity
                    ? ""
                    : props.activity.map(function (item, i) {
                        return (
                          <tr>
                            <td>
                              <div
                                style={{
                                  width: "25px",
                                  height: "25px",
                                  borderRadius: "50%",
                                  backgroundColor: props.calActivity[i]
                                    ? props.calActivity[i].backgroundColor
                                    : "",
                                }}
                              ></div>
                            </td>
                            <td>
                              {item.type}: {item.name}
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </CCol>
            <CCol lg={7} className="m-4 order-2">
              <FullCalendar
                id="cal"
                plugins={[dayGridPlugin]}
                initialView="dayGridMonth"
                weekends={true}
                height="600px"
                events={props.calActivity}
                color={"#ff0000"}
                dayMaxEvents={true}
                businessHours={true}
                eventClick={handleDate}
                headerToolbar={{
                  left: "prev,next",
                  center: "title",
                  right: "today",
                }}
              />
            </CCol>
          </CRow>
          <CToaster ref={toaster} push={toast} placement="middle-start" />
          <CRow></CRow>
        </CCard>
      </CCol>
    </CRow>
  );
}

export default InnovationDetailTableActivity;
