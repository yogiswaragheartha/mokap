import { Link } from "react-router-dom";
function EventJoined(props) {
  return (
    <div
      className="list-item-event d-flex justify-content-left"
      style={{
        display: "inline-block",
      }}
    >
      <div
        className="justifiy-content-start align-item-center d-flex"
        style={{
          position: "relative",
          zIndex: "1",
          width: "50%",
          left: "-1rem",
        }}
      >
        <Link
          style={{ textDecoration: "none", width: "100%" }}
          to={"/event/detail/" + props.event.id}
        >
          <div
            className="ms-5 text-white "
            style={{
              position: "absolute",
              height: "100px",
              zIndex: "2",
              marginTop: "75px",
            }}
          >
            <h5>{props.event.name}</h5>
          </div>
          <img
            src={props.event.banner}
            alt={props.event.name}
            style={{
              height: "180px",
              width: "100%",
              objectFit: "cover",
              borderRadius: "30px",
              filter: "brightness(30%)",
            }}
          />
          <div
            className="justifiy-content-center align-item-center active-indicator text-white"
            style={{
              marginTop: "-50px",
              marginL: "-40px",
            }}
          >
            <table>
              <tr>
                <td>
                  <div
                    style={{
                      width: "15px",
                      height: "15px",
                      borderRadius: "50%",
                    }}
                    className={
                      props.event.status === "Active"
                        ? "bg-success"
                        : "bg-light"
                    }
                  ></div>
                </td>
                <td>
                  <h7>{props.event.status}</h7>
                </td>
              </tr>
            </table>
          </div>
        </Link>
      </div>
    </div>
  );
}

export default EventJoined;
