import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { CCol, CContainer, CRow } from "@coreui/react";
import { getInnovation, getArea } from "src/api/submission";
import { getEvent, getPhase } from "src/api/event";
import Input from "src/components/Input";
import { useSelector } from "react-redux";
import { CSpinner } from "@coreui/react";
import LoadingData from "src/components/LoadingData";
const MyInnovation = () => {
  const token = localStorage.getItem("token_jwt");
  const navigate = useNavigate();
  const [inovasi, setInovasi] = useState([]);
  const [listInovasi, setListInovasi] = useState([]);
  const { auth } = useSelector((state) => state);
  const [isLoading, setisLoading] = useState(true);
  const get_innovation = async (token) => {
    try {
      const response = await getInnovation(token);

      console.log("Innovation", response.data);
      setInovasi(
        response.data
          .filter((c) => c.createdByUserId == auth.id)
          .sort((a, b) => (a.id > b.id ? -1 : 1)) || []
      );
      setListInovasi(
        response.data
          .filter((c) => c.createdByUserId == auth.id)
          .sort((a, b) => (a.id > b.id ? -1 : 1)) || []
      );
      setisLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  const updateKontenSearch = () => {
    const search = document.getElementById("filter-search").value;

    const filtered = listInovasi.filter(function (obj) {
      if (obj["title"]) {
        return obj["title"].toLowerCase().includes(search.toLowerCase());
      }
    });
    setInovasi(filtered);
  };

  useEffect(() => {
    get_innovation(token);
  }, [auth.id]);

  var elements = document.getElementsByClassName("clickable");
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    element.addEventListener("click", function () {
      var href = this.dataset.href;
      if (href) {
        window.location.assign(href);
      }
    });
  }
  return (
    <div className="bg-light mt-4 align-items-center">
      <CContainer>
        <h3>My Innovation</h3>
        <CRow>
          <CCol md={4}>
            <Input
              type="text"
              id="filter-search"
              placeholder="Search"
              value=""
              callBack={updateKontenSearch}
            />
          </CCol>
          <CCol></CCol>
        </CRow>
        {isLoading ? (
          <LoadingData />
        ) : (
          <div className="my-4 table-responsive">
            <table class="table table-striped ">
              <thead className="text-center">
                <tr>
                  <th scope="col">Logo</th>
                  <th scope="col">Innovation Title</th>
                  <th scope="col">Phase</th>
                  <th scope="col">Tagline</th>
                  <th scope="col">Event</th>
                  <th scope="col">Progress</th>
                </tr>
              </thead>
              <tbody>
                {!inovasi
                  ? ""
                  : inovasi.map(function (item, i) {
                      return (
                        <tr
                          class="clickable align-middle"
                          onClick={() => {
                            navigate("/innovation/detail/" + item.id);
                          }}
                        >
                          <td className="justify-content-center">
                            {item.logo ? (
                              <img
                                className="inovasi"
                                src={item.logo}
                                style={{ objectFit: "cover" }}
                              />
                            ) : (
                              <div
                                className="inovasi bg-primary text-center pt-2 text-white "
                                style={{
                                  objectFit: "cover",
                                  fontSize: "28px",
                                }}
                              >
                                {item.title ? item.title.substr(0, 1) : ""}
                              </div>
                            )}
                          </td>
                          <td>{item.title}</td>
                          <td className="text-center">
                            {item.Phase ? item.Phase.name : ""}
                          </td>
                          <td>{item.tagline}</td>
                          <td>{item.Event ? item.Event.name : ""}</td>

                          <td
                            className={
                              item.avgProgress <= 25
                                ? "text-red text-center"
                                : item.avgProgress <= 50
                                ? "text-orange text-center"
                                : item.avgProgress <= 75
                                ? "text-yellow text-center"
                                : "text-green text-center"
                            }
                          >
                            {item.avgProgress
                              ? Math.round(item.avgProgress)
                              : 0}
                            %
                          </td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </div>
        )}
      </CContainer>
    </div>
  );
};

export default MyInnovation;
