import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CNavLink,
  CCardImage,
  CCardText,
  CCardTitle,
  CRow,
  CAccordion,
  CTable,
} from "@coreui/react";
import axios from "axios";
import { useNavigate, NavLink, useParams } from "react-router-dom";
import CIcon from "@coreui/icons-react";
import { cilMediaPlay, cilMediaStop, cilAddressBook } from "@coreui/icons";

import { getEvent } from "src/api/event";
import { getInnovationByID } from "src/api/submission";
import EventJoined from "./EventJoined";
import InnovatorList from "./InnovatorList";
import Detail from "./Detail";
import InnovationButton from "./InnovationButton";
import InnovationDetailTableRevenue from "./InnovationDetailTableRevenue";
import InnovationDetailTableBudget from "./InnovationDetailTableBudget";
import InnovationDetailTableOKR from "./InnovationDetailTableOKR";
import InnovationDetailTableActivity from "./InnovationDetailTableActivity";
import { useSelector } from "react-redux";
import { CSpinner } from "@coreui/react";
import LoadingData from "src/components/LoadingData";

const InnovationDetail = () => {
  const token = localStorage.getItem("token_jwt");
  const [inovasi, setInovasi] = useState([]);
  const [event, setEvent] = useState([]);
  const [isLoadContent, setIsLoadContent] = useState(true);
  const [progress, setProgress] = useState(0);
  const [ev, setEv] = useState();
  const buttonset = "1";
  const [calActivity, setCalActivity] = useState([]);
  const { id } = useParams();
  const [activeTab, setActiveTab] = useState(1);
  const { auth } = useSelector((state) => state);

  const get_inovasi = async (token, idinovasi) => {
    try {
      const response = await getInnovationByID(token, idinovasi);

      console.log("Inovasi", response.data);

      setInovasi(response.data);

      const arrColor = [
        "#FB7272",
        "#FFE463",
        "#6BF4DB",
        "#5F49D2",
        "#1CCB39",
        "#CB1C1C",
      ];
      const arrAct = [];
      let totProgres = 0;

      response.data.Activities.map(function (item, i) {
        arrAct.push({
          title: item.picName,
          date: item.activityStart,
          backgroundColor: arrColor[i] ? arrColor[i] : "#f6960b",
          borderColor: arrColor[i] ? arrColor[i] : "#f6960b",
          textColor: "black",
          url: "#cal",

          id: i,
          type: item.type,
          name: item.name,
          output: item.output,
          activityStart: item.activityStart,
          activityEnd: item.activityEnd,
        });
        totProgres += item.progress;
      });
      setProgress(Math.round(totProgres / response.data.Activities.length));

      setCalActivity(arrAct);

      get_event(token, response.data.eventId);
    } catch (error) {
      console.log(error);
    }
  };

  const get_event = async (token, id) => {
    try {
      const response = await getEvent(token, id);

      console.log("Event", response.data);

      setEvent(response.data.event || []);
    } catch (error) {
      console.log(error);
    }
    setIsLoadContent(false);
  };

  const hancleChangeTab = (e) => {
    setActiveTab(e);
  };

  useEffect(() => {
    get_inovasi(token, id);
  }, []);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row ">
      <CContainer>
        <CRow className="">
          <CRow>
            <h3>Innovation Detail</h3>
          </CRow>

          {isLoadContent ? (
            <LoadingData />
          ) : (
            <CCol md={12} lg={12} xl={12}>
              <CCard className="mt-2 border-0">
                <>
                  <Detail
                    key={inovasi.id}
                    inovasi={inovasi}
                    progress={progress}
                  />
                  <CRow>
                    <CCol md={12} lg={12} xl={12}>
                      <CCardTitle className="fw-bold text-black  mt-4">
                        Description
                      </CCardTitle>
                      <CCardText className="my-4">
                        <p className="" style={{ textAlign: "justify" }}>
                          {inovasi.description}
                        </p>
                      </CCardText>
                    </CCol>
                  </CRow>
                  <CRow>
                    <CCol md={12} lg={12} xl={12}>
                      <CCardTitle className="fw-bold text-black  mt-4">
                        Innovator
                      </CCardTitle>
                      <CCardText>
                        <div className="my-4 table-responsive">
                          <InnovatorList member={inovasi.Members} />
                        </div>
                      </CCardText>
                    </CCol>
                  </CRow>
                  <CRow>
                    <CCol md={12} lg={12} xl={12}>
                      <CCardTitle className="fw-bold text-black  mt-4">
                        Event Joined
                      </CCardTitle>
                      <CCardText>
                        <EventJoined key={event.id} event={event} />
                      </CCardText>
                    </CCol>
                  </CRow>
                  {auth.id != inovasi.createdByUserId && auth.role > 2 ? (
                    ""
                  ) : (
                    <CRow>
                      <CCol md={12} lg={12} xl={12}>
                        <CCardTitle className="fw-bold text-black  mt-4">
                          Summary
                        </CCardTitle>
                        <CRow>
                          <div
                            className="d-grid gap-3 d-flex justify-content-center"
                            style={{ width: "100%" }}
                          >
                            <InnovationButton
                              item={activeTab}
                              callback={hancleChangeTab}
                            />
                          </div>
                        </CRow>

                        {activeTab == 1 ? (
                          <InnovationDetailTableActivity
                            activity={inovasi.Activities}
                            calActivity={calActivity}
                          />
                        ) : activeTab == 2 ? (
                          <InnovationDetailTableBudget
                            budget={inovasi.Budgets}
                          />
                        ) : activeTab == 3 ? (
                          <InnovationDetailTableOKR
                            okr={inovasi.KeyResults}
                            objective={inovasi.objective}
                            digitalization={inovasi.digitalization}
                            digitization={inovasi.digitization}
                          />
                        ) : activeTab == 4 ? (
                          <InnovationDetailTableRevenue
                            revenue={inovasi.revenue}
                            description={inovasi.revenueDescription}
                          />
                        ) : (
                          ""
                        )}
                      </CCol>
                    </CRow>
                  )}
                </>
              </CCard>
            </CCol>
          )}
        </CRow>
      </CContainer>
    </div>
  );
};

export default InnovationDetail;
