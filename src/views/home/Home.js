import React, { useState, useEffect } from "react";

import {
  CCol,
  CContainer,
  CRow,
  CCarousel,
  CCarouselItem,
  CImage,
  CCard,
  CCardBody,
} from "@coreui/react";
import logo from "src/assets/images/logo.png";
import ListInovasi from "./ListInovasi";
import SliderBanner from "./Slider";
import ListEvent from "./ListEvent";
import SebaranInovasi from "./SebaranInovasi";
import { getHome } from "src/api/dashboard";
import LoadingData from "src/components/LoadingData";
import view_all from "src/assets/images/view_all.png";
import { Link } from "react-router-dom";

const Home = () => {
  const [inovasi, setInovasi] = useState([]);
  const [event, setEvent] = useState([]);
  const [announcement, setAnnouncement] = useState([]);
  const token = localStorage.getItem("token_jwt");
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [sebaran, setSebaran] = useState([]);

  const get_home = async (token) => {
    try {
      const response = await getHome(token);

      console.log("Home", response.data);
      setEvent(
        response.data.event
          .sort((a, b) => (a.id > b.id ? -1 : 1))
          .slice(0, 3) || []
      );
      setInovasi(
        response.data.innovation
          .sort((a, b) => (a.id > b.id ? -1 : 1))
          .slice(0, 10) || []
      );
      setAnnouncement(response.data.announcement);
      setSebaran(response.data.sebaran);
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  useEffect(() => {
    get_home(token);
  }, []);

  return (
    <>
      {isLoadingData ? (
        <LoadingData />
      ) : (
        <>
          <div className="bg-light mt-6">
            <CContainer>
              <SliderBanner data={announcement} />
            </CContainer>
          </div>

          <div className="bg-light mt-5">
            <CContainer>
              <div className="flex-container mb-3">
                <div class="flex-child ">
                  <h3 className="text-3xl font-semibold text-Blacks">
                    List of Innovations
                  </h3>
                </div>
                <div class="flex-child" style={{ offset: "1rem" }}>
                  <Link
                    style={{ textDecoration: "none" }}
                    to={"/innovation/list"}
                  >
                    <div
                      class="float-right"
                      style={{ position: "relative", top: "1rem" }}
                    >
                      <div>
                        <img
                          src={view_all}
                          alt="|||"
                          style={{ maxHeight: "12px" }}
                        ></img>
                      </div>
                      <div style={{ marginLeft: "2px" }}>
                        <p
                          style={({ textAlign: "right" }, { color: "#6982D8" })}
                        >
                          view all
                        </p>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>

              <ListInovasi data={inovasi} />
            </CContainer>
          </div>
          <div className="bg-light mt-5">
            <CContainer>
              <CRow className="justify-content-evenly">
                <CCol sm={12} lg={6} className="mb-5">
                  <ListEvent data={event} />
                </CCol>
                <CCol sm={12} lg={6}>
                  <SebaranInovasi data={sebaran} />
                </CCol>
              </CRow>
            </CContainer>
          </div>
        </>
      )}
    </>
  );
};

export default Home;
