import { CCard, CRow } from "@coreui/react";
import ListInovasi_Item from "./ListInovasi_Item";

function ListInovasi(props) {
  return (
    <>
      <CCard className="mb-5" class="shadow bg-body rounded">
        <div className="list justify-content-center p-4">
          <CRow
            xs={{ cols: 1 }}
            sm={{ cols: 2 }}
            md={{ cols: 3 }}
            lg={{ cols: 4 }}
            xl={{ cols: 5 }}
          >
            {props.data.map(function (item) {
              // console.log(item);
              return (
                <ListInovasi_Item
                  id={item.id}
                  name={item.title}
                  photo={item.logo}
                  desc={item.tagline}
                />
              );
            })}
          </CRow>
        </div>
      </CCard>
    </>
  );
}

export default ListInovasi;
