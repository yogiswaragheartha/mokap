import { Link } from "react-router-dom";

function Banner(props) {
  // console.log("item banner:", props.item);
  return (
    <Link to={props.link}>
      <div>
        <img
          className="d-block w-100"
          style={{
            borderRadius: "25px",
            maxHeight: "400px",
            objectFit: "cover",
          }}
          src={props.item}
          alt={props.title}
        />
      </div>
    </Link>
  );
}

export default Banner;
