import { CCard, CCol } from "@coreui/react";
import ListEvent_Item from "./ListEvent_Item";

function ListEvent(props) {
  // console.log("props", props);
  const event = props.data.filter((item, id) => id > props.data.length - 4);

  return (
    <>
      <h2 className="text-3xl font-semibold mb-3 text-Blacks">Events</h2>
      <CCard className="mb-5" class="shadow bg-body rounded">
        <div className="list">
          {event.map(function (item) {
            return <ListEvent_Item key={item.id} data={item} />;
          })}
        </div>
      </CCard>
    </>
  );
}

export default ListEvent;
