import { CCol } from "@coreui/react";
import { Link } from "react-router-dom";
function ListInovasi_Item(props) {
  return (
    <CCol className=" d-flex ">
      <Link
        style={{ textDecoration: "none", width: "100%" }}
        to={"/innovation/detail/" + props.id}
        className="my-2 list-inov"
      >
        <table className="m-2">
          <tr>
            <td>
              {props.photo ? (
                <img
                  className="inovasi"
                  src={props.photo}
                  style={{
                    objectFit: "cover",
                  }}
                />
              ) : (
                <div
                  className="inovasi bg-primary text-center pt-2 text-white"
                  style={{
                    objectFit: "cover",
                    fontSize: "28px",
                  }}
                >
                  {props.name ? props.name.substr(0, 1) : ""}
                </div>
              )}
            </td>
            <td style={{ padding: "0 0.5rem 0 0.5rem" }}>
              <h6 class="text-start fw-bold text-black pt-2">{props.name}</h6>
              <h6 class="text-start text-mini lh-1 text-black-50">
                {props.desc}
              </h6>
            </td>
          </tr>
        </table>
      </Link>
    </CCol>
  );
}

export default ListInovasi_Item;
