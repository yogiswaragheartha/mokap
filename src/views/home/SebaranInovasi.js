import React, { useState, useEffect } from "react";
import { useRef } from "react";
import { CCard, CCol, CCardBody } from "@coreui/react";
import { CChartBar, CChartPie } from "@coreui/react-chartjs";
// import sebaran_inovasi from "src/data/sebaran_inovasi";
import { getSebaranInovasi } from "src/api/dashboard";
import { Pie, getElementsAtEvent } from "react-chartjs-2";
import { useNavigate } from "react-router-dom";

function SebaranInovasi(props) {
  const chartRef = useRef();
  const navigate = useNavigate();

  const onClickss = (event) => {
    console.log(getElementsAtEvent(chartRef.current, event)[0].index + 1);
    const eventValue = "";
    const areaId = getElementsAtEvent(chartRef.current, event)[0].index + 1;

    const state = { eventValue, areaId };
    navigate("/innovation/list", {
      state: { state },
    });
  };

  // const [sebaran_inovasi, setSebaran_inovasi] = useState([]);
  // const get_sebaran_inovasi = async () => {
  //   try {
  //     const response = await getSebaranInovasi();

  //     console.log("Sebaran Inovasi", response.data.result);
  //     setSebaran_inovasi(response.data.result);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };
  useEffect(() => {
    // get_sebaran_inovasi();
  }, []);
  return (
    <>
      <h2 className="text-3xl font-semibold mb-3 text-Blacks">
        Sebaran Inovasi
      </h2>
      {!props.data ? (
        ""
      ) : (
        <CCard className="mb-5 shadow">
          <CCardBody>
            <CChartPie
              ref={chartRef}
              onClick={onClickss}
              data={{
                labels: props.data.map(function (item) {
                  return item.name;
                }),
                datasets: [
                  {
                    data: props.data.map(function (item) {
                      return item.totalInovasi;
                    }),
                    backgroundColor: [
                      "#FF6384",
                      "#36A2EB",
                      "#FFCE56",
                      "#8463FF",
                      "#52489C",
                      "#00798C",
                      "#E63946",
                    ],
                    hoverBackgroundColor: ["#222"],
                  },
                ],
              }}
            />
          </CCardBody>
        </CCard>
      )}
    </>
  );
}

export default SebaranInovasi;
