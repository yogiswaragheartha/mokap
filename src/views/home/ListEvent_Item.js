import { CCol, CRow } from "@coreui/react";
import { Link } from "react-router-dom";
function ListEvent_Item(props) {
  return (
    <Link
      style={{ textDecoration: "none", width: "100%" }}
      to={"/event/detail/" + props.data.id}
    >
      <div
        className="list-item-event d-flex justify-content-center "
        style={{
          display: "inline-block",
        }}
      >
        <div
          className="justifiy-content-center align-item-center d-flex"
          style={{ position: "relative", zIndex: "1", width: "100%" }}
        >
          <div
            className="ms-5 text-white "
            style={{
              position: "absolute",
              height: "100px",
              zIndex: "2",
              marginTop: "75px",
            }}
          >
            <h5>{props.data.name}</h5>
          </div>
          <img
            src={props.data.banner}
            alt="Inovasi"
            style={{
              height: "180px",
              width: "100%",
              objectFit: "cover",
              borderRadius: "30px",
              filter: "brightness(30%)",
            }}
          />
          <div
            className="justifiy-content-center align-item-center active-indicator text-white"
            style={{ marginTop: "-50px", marginRight: "-40px" }}
          >
            <table>
              <tr>
                <td>
                  <div
                    style={{
                      width: "15px",
                      height: "15px",
                      borderRadius: "50%",
                    }}
                    className={
                      props.data.status == true
                        ? "bg-success mt-1 me-1"
                        : "bg-light mt-1 me-1"
                    }
                  ></div>
                </td>
                <td>
                  <h7>{props.data.status == true ? "Active" : "Inactive"}</h7>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </Link>
  );
}

export default ListEvent_Item;
