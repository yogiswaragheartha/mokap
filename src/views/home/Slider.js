import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import Banner from "./Banner";
import data_banner from "src/data/data_banner";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { getAnnouncement } from "src/api/event";

const PrevBtn = (props) => {
  const { className, onClick } = props;
  return <div className={className} onClick={onClick} />;
};

const NextBtn = (props) => {
  const { className, onClick } = props;
  return <div className={className} onClick={onClick} />;
};

function SliderBanner(props) {
  const [announcement, setAnnouncement] = useState([]);
  const token = localStorage.getItem("token_jwt");

  const get_announcement = () => {
    setAnnouncement(props.data || []);
  };

  useEffect(() => {
    get_announcement();
  }, []);
  return (
    <Slider
      className="relative "
      slidesToShow={1}
      slidesToScroll={1}
      initialSlide={0}
      swipeToSlide={true}
      infinite={true}
      speed={500}
      arrows={true}
      autoplay={true}
      prevArrow={<PrevBtn />}
      nextArrow={<NextBtn />}
    >
      {announcement.map((item) => {
        return (
          <Banner
            item={item.banner}
            link={"/announcement/detail/" + item.id + "/mokap-announcement"}
          />
        );
      })}
    </Slider>
  );
}

export default SliderBanner;
