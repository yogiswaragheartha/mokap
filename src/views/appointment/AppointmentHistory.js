import React, { useState, useEffect } from "react";
import { CCard, CContainer } from "@coreui/react";
import CButtonAppointment from "./CButtonAppointment";
// import { getAppointment } from "src/api/event";
import { getMyAppointment } from "src/api/appointment";

import Moment from "moment";

const AppointmentHistory = () => {
  const token = localStorage.getItem("token_jwt");
  const [appointment, setAppointment] = useState([]);

  const get_appointment = async (token) => {
    try {
      const response = await getMyAppointment(token);

      console.log("Appointment", response.data);
      setAppointment(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    get_appointment(token);
  }, []);

  return (
    <div className="bg-light my-4 align-items-center">
      <CContainer>
        <h3 className="mb-5">Appointment</h3>

        <CButtonAppointment active="history" />
        <CCard className="border-0 shadow p-5">
          {!appointment
            ? ""
            : appointment.map(function (item, i) {
                {
                  return (
                    <div
                      style={{ borderLeft: "2px solid #bbe" }}
                      className="ps-2 "
                    >
                      <table>
                        <tr>
                          <td className="py-2">
                            <div
                              style={{
                                borderRadius: "50%",
                                width: "10px",
                                height: "10px",
                                marginLeft: "-15px",
                              }}
                              className="bg-primary"
                            ></div>
                          </td>
                          <td className="pb-4">
                            <div>Appointment {i + 1}</div>
                            <div>{item.topic}</div>
                            <div className="text-muted">
                              {Moment(item.bookingDate).format("DD MMM YYYY")};{" "}
                              {Moment(item.timeStart).zone(0).format("HH:mm")}-
                              {Moment(item.timeEnd).zone(0).format("HH:mm")}
                            </div>
                            <div>Mentor: {item.mentorId}</div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  );
                }
              })}
        </CCard>
      </CContainer>
    </div>
  );
};

export default AppointmentHistory;
