import React, { useState, useEffect, useRef } from "react";
import {
  CButton,
  CCard,
  CCol,
  CContainer,
  CToast,
  CToastHeader,
  CToastBody,
  CToaster,
  CRow,
} from "@coreui/react";
import axios from "axios";
import { Link, useNavigate, useParams, useLocation } from "react-router-dom";
import CButtonAppointment from "./CButtonAppointment";
import { getAppointment, updateApproval } from "src/api/appointment";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";
import { useSelector } from "react-redux";
import FullCalendar from "@fullcalendar/react"; // must go before plugins
import dayGridPlugin from "@fullcalendar/daygrid"; // a plugin!
import Moment from "moment";
import LoadingData from "src/components/LoadingData";
import LoadingContent from "src/components/LoadingContent";

const AppointmentApproval = () => {
  const token = localStorage.getItem("token_jwt");
  const location = useLocation();
  const [appointment, setAppointment] = useState([]);
  const [waitingApproval, setWaitingApproval] = useState([]);
  const [events, setEvents] = useState([]);
  const [modApprove, setModApprove] = useState(false);
  const [modReject, setModReject] = useState(false);
  const [comment, setComment] = useState();
  const [idAppointment, setIdAppointment] = useState();
  const { auth } = useSelector((state) => state);
  const [toast, addToast] = useState(0);
  const toaster = useRef();
  const [isLoadingData, setIsLoadingData] = useState(true);
  const [isLoading, setisLoading] = useState(false);
  const [modal, setModal] = useState({
    visible: false,
    status: "",
    message: "",
  });

  const handleApprove = (event, comment) => {
    setModApprove(false);
    const status = "APPROVED";
    const data = { status, comment };
    console.log("Approve", data, idAppointment);
    update_approval(token, data, idAppointment);
  };

  const handleReject = (event, comment) => {
    setModReject(false);
    const status = "REJECTED";
    const data = { status, comment };
    console.log("Reject", data, idAppointment);
    update_approval(token, data, idAppointment);
  };

  const update_approval = async (token, data, id) => {
    setisLoading(true);
    try {
      const response = await updateApproval(token, data, id);
      console.log(response.data);
      if (response.data.message) {
        setModal({
          visible: true,
          status: "success",
          message: "",
        });
      }

      get_appointment(token);
    } catch (error) {
      console.log(error);
      setModal({
        visible: true,
        status: "danger",
        message: error.message,
      });
    }
    setisLoading(false);
  };
  const handleDate = (e) => {
    // alert("a");
    console.log("event Cal", e.event);
    // alert(e.event.title);
    addToast(
      <CToast className="mt-2">
        <CToastHeader closeButton>
          <svg
            className="rounded me-2"
            width="20"
            height="20"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="xMidYMid slice"
            focusable="false"
            role="img"
          >
            <rect
              width="100%"
              height="100%"
              fill={e.event.backgroundColor}
            ></rect>
          </svg>
          <strong className="me-auto">
            {e.event._def.extendedProps.topik}
          </strong>
        </CToastHeader>
        <CToastBody>
          <table>
            <tr>
              <td>Tanggal</td>
              <td>:</td>
              <td>{e.event._def.extendedProps.tgl}</td>
            </tr>
            <tr>
              <td>Waktu</td>
              <td>:</td>
              <td>{e.event._def.extendedProps.waktu}</td>
            </tr>
            <tr>
              <td>Bookers</td>
              <td>:</td>
              <td>{e.event.title}</td>
            </tr>
            <tr>
              <td>Mentor</td>
              <td>:</td>
              <td>{e.event._def.extendedProps.mentor}</td>
            </tr>
          </table>{" "}
        </CToastBody>
      </CToast>
    );
  };

  const get_appointment = async (token) => {
    try {
      const response = await getAppointment(token);

      let myAppointment = [];
      if (response.data) {
        myAppointment = response.data.filter((c) => c.mentorId == auth.id);
      }

      console.log("MyAppointment", myAppointment);

      setAppointment(
        myAppointment.filter((c) => c.status != "WAITING APPROVAL")
      );
      setWaitingApproval(
        myAppointment.filter((c) => c.status == "WAITING APPROVAL")
      );

      const arrEvents = [];
      myAppointment.map(function (item, i) {
        arrEvents.push({
          title: item.innovator ? item.innovator.fullName : "Unknown",
          date: item.bookingDate,
          backgroundColor: "#f6960b",
          borderColor: "#f6960b",
          textColor: "black",
          url: "#cal",

          id: i,
          topik: item.topic,
          mentor: item.mentor.fullName,
          tgl: item.bookingDate,
          waktu:
            Moment(item.timeStart).zone(0).format("HH:mm") +
            " - " +
            Moment(item.timeEnd).zone(0).format("HH:mm"),
        });
      });

      setEvents(arrEvents);

      setIsLoadingData(false);
    } catch (error) {
      console.log(error);
      setIsLoadingData(false);
    }
  };

  useEffect(() => {
    get_appointment(token);
  }, [auth]);

  return (
    <div className="bg-light my-4 align-items-center">
      <CContainer>
        <ModalAlert
          visible={modal.visible}
          status={modal.status}
          message={modal.message}
          callBack={() => setModal({ visible: false, message: "", status: "" })}
        />
        <ModalAlert
          visible={isLoading}
          status="loading"
          message={<LoadingContent />}
        />
        <h3 className="mb-5">Appointment Approval</h3>
        {isLoadingData ? (
          <LoadingData />
        ) : (
          <>
            <CRow>
              <CCol></CCol>
              <CCol md={8} lg={6}>
                <FullCalendar
                  id="cal"
                  plugins={[dayGridPlugin]}
                  initialView="dayGridMonth"
                  weekends={true}
                  height="450px"
                  events={events}
                  color={"#ff0000"}
                  dayMaxEvents={true}
                  businessHours={true}
                  eventClick={handleDate}
                  headerToolbar={{
                    left: "prev,next",
                    center: "title",
                    right: "today",
                  }}
                />
              </CCol>
              <CToaster ref={toaster} push={toast} placement="middle-start" />
              <CCol></CCol>
            </CRow>

            <CCard className="my-5 border-0">
              <h4>Appointment Book</h4>
              <div className="table-responsive">
                <table className="table table-striped mt-4 text-center">
                  <thead className="text-center">
                    <tr>
                      <th scope="col">Date Booked</th>
                      <th scope="col">Time</th>
                      <th scope="col">Propose Mentor</th>
                      <th scope="col">Bookers</th>
                      <th scope="col">Topic</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {!waitingApproval
                      ? ""
                      : waitingApproval.map(function (item, i) {
                          return (
                            <tr>
                              <td>{item.bookingDate}</td>
                              <td>
                                {Moment(item.timeStart).zone(0).format("HH:mm")}
                                -{Moment(item.timeEnd).zone(0).format("HH:mm")}
                              </td>
                              <td>{item.mentor.fullName}</td>
                              <td>
                                {item.innovator
                                  ? item.innovator.fullName
                                  : "Unknown"}
                              </td>
                              <td>{item.topic}</td>
                              <td>
                                <CButton
                                  onClick={() => {
                                    setModApprove(!modApprove);
                                    setComment("");
                                    setIdAppointment(item.id);
                                  }}
                                  style={{ width: "90px" }}
                                  className={
                                    "bg-success text-white border-0 me-2"
                                  }
                                >
                                  Approve
                                </CButton>

                                <CButton
                                  onClick={() => {
                                    setModReject(!modReject);
                                    setComment("");
                                    setIdAppointment(item.id);
                                  }}
                                  style={{ width: "90px" }}
                                  className={
                                    "bg-danger text-white border-0 me-2"
                                  }
                                >
                                  Reject
                                </CButton>
                              </td>
                            </tr>
                          );
                        })}
                    <Modal
                      pesan="Are you sure to Approve?"
                      input={true}
                      callBack={(e, komen) => handleApprove(e, komen)}
                      close={() => setModApprove(false)}
                      visible={modApprove}
                    />
                    <Modal
                      pesan="Are you sure to Reject?"
                      input={true}
                      callBack={(e, komen) => handleReject(e, komen)}
                      close={() => setModReject(false)}
                      visible={modReject}
                    />
                  </tbody>
                </table>
              </div>
            </CCard>
            <CCard className="my-5 border-0 ">
              <h4>My Appointment</h4>
              <div className="table-responsive">
                <table className="table table-striped mt-4 text-center">
                  <thead className="text-center">
                    <tr>
                      <th scope="col">Date Booked</th>
                      <th scope="col">Time</th>
                      <th scope="col">Propose Mentor</th>
                      <th scope="col">Bookers</th>
                      <th scope="col">Topic</th>
                    </tr>
                  </thead>
                  <tbody>
                    {!appointment
                      ? ""
                      : appointment.map(function (item, i) {
                          return (
                            <tr>
                              <td>{item.bookingDate}</td>
                              <td>
                                {Moment(item.timeStart).zone(0).format("HH:mm")}
                                -{Moment(item.timeEnd).zone(0).format("HH:mm")}
                              </td>
                              <td>{item.mentor.fullName}</td>
                              <td>
                                {item.innovator
                                  ? item.innovator.fullName
                                  : "Unknown"}
                              </td>
                              <td>{item.topic}</td>
                            </tr>
                          );
                        })}
                  </tbody>
                </table>
              </div>
            </CCard>
          </>
        )}
      </CContainer>
    </div>
  );
};

export default AppointmentApproval;
