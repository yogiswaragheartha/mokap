import React, { useState, useEffect } from "react";
import { CCol, CRow } from "@coreui/react";
import Input from "src/components/Input";
import { useNavigate } from "react-router-dom";

const CButtonAppointment = (props) => {
  const navigate = useNavigate();

  const handleHistory = async (event) => {
    navigate("/event/appointment/history/");
    event.preventDefault();
  };
  const handleBook = async (event) => {
    navigate("/event/appointment/");
    event.preventDefault();
  };

  return (
    <div class="btn-group">
      <font
        href="#"
        class="btn btn-primary"
        aria-current="page"
        onClick={handleBook}
        style={{
          borderTopLeftRadius: "50px",
          borderTopRightRadius: "50px",
          width: "120px",
          opacity: props.active == "book" ? "1" : "0.5",
        }}
      >
        Book
      </font>
      <font
        href="#"
        class="btn btn-primary"
        onClick={handleHistory}
        style={{
          borderTopLeftRadius: "50px",
          borderTopRightRadius: "50px",
          width: "120px",
          opacity: props.active == "history" ? "1" : "0.5",
        }}
      >
        History
      </font>
    </div>
  );
};

export default CButtonAppointment;
