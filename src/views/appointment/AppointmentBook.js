import React, { useState, useEffect } from "react";
import { CButton, CCard, CCol, CContainer, CRow } from "@coreui/react";
import { Link, useNavigate, useParams, useLocation } from "react-router-dom";
import CButtonAppointment from "./CButtonAppointment";
import Input from "src/components/Input";
// import { getAppointment } from "src/api/event";
import Moment from "moment";
import { getAppointment, createAppointment } from "src/api/appointment";
import { getAllUser } from "src/api/user";
import LoadingContent from "src/components/LoadingContent";
import Modal from "src/components/Modal";
import ModalAlert from "src/components/ModalAlert";

const AppointmentBook = () => {
  const token = localStorage.getItem("token_jwt");
  const { id } = useParams();
  const location = useLocation();
  const [appointment, setAppointment] = useState([]);
  const [mentor, setMentor] = useState([]);
  const [date, setDate] = useState();
  const [timeStart, setTimeStart] = useState();
  const [timeEnd, setTimeEnd] = useState();
  const [mentorId, setMentorId] = useState();
  const [topic, setTopic] = useState();
  const [mod, setMod] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [modal, setModal] = useState({
    visible: location.state
      ? location.state.visible == true
        ? true
        : false
      : false,
    message: location.state ? location.state.message : "",
    status: location.state ? location.state.status : "",
  });

  const handleChangeDate = (event) => {
    setDate(event.target.value);
  };
  const handleChangeTimeStart = (event) => {
    setTimeStart(event.target.value);
  };
  const handleChangeTimeEnd = (event) => {
    setTimeEnd(event.target.value);
  };
  const handleChangeMentorId = (event) => {
    setMentorId(event.target.value);
  };
  const handleChangeTopic = (event) => {
    setTopic(event.target.value);
  };

  const handleSubmit = () => {
    if (!date || !timeStart || !timeEnd || !mentorId || !topic) {
      setModal({
        visible: true,
        message: "Fill in the form corectly",
        status: "warning",
      });
      return;
    }
    const data = {
      bookingDate: date,
      timeStart: date + "T" + timeStart + ":00",
      timeEnd: date + "T" + timeEnd + ":00",
      mentorId,
      topic,
    };
    console.log("Konten", data);
    create_appointment(token, data);
  };

  const create_appointment = async (token, data) => {
    setIsLoading(true);
    setMod(false);
    try {
      const response = await createAppointment(token, data);

      console.log("Create Appointment", response.data);
      setIsLoading(false);
      setModal({ visible: true, status: "success", message: "" });
      get_appointment(token);
      setDate("");
      setTimeStart("");
      setTimeEnd("");
      setMentorId("");
      setTopic("");
    } catch (error) {
      console.log(error);
      setModal({ visible: true, status: "danger", message: error.message });
      setIsLoading(false);
    }
  };

  const get_appointment = async (token) => {
    try {
      const response = await getAppointment(token);

      const appointments = response.data
        .filter((c) => c.bookingDate != null && c.mentorId != null)
        .sort((a, b) => (a.id > b.id ? -1 : 1));

      console.log("Appointment", appointments);
      setAppointment(appointments);
    } catch (error) {
      console.log(error);
    }
  };

  const get_allUser = async (token) => {
    try {
      const response = await getAllUser(token);
      const mentors = response.data
        .sort((a, b) =>
          a.fullName.toLowerCase() < b.fullName.toLowerCase() ? -1 : 1
        )
        .filter((c) => c.roleId <= 2); //Admin dan AMA

      console.log("Mentor", mentors);

      const arrMentor = [];
      mentors.map(function (item, i) {
        arrMentor.push({
          text: item.fullName,
          value: item.id,
        });
      });
      setMentor(arrMentor);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    get_appointment(token);
    get_allUser(token);
  }, []);

  return (
    <div className="bg-light my-4 align-items-center">
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <CContainer>
        <h3 className="mb-5">Appointment</h3>
        <CButtonAppointment active="book" />
        <CCard className="border-0 shadow p-4">
          <h4 className="text-center mb-4">Booking</h4>

          <CRow>
            <CCol md={1}></CCol>
            <CCol md={10}>
              <CRow>
                <CCol>
                  <Input
                    id="date"
                    label="Booking Date"
                    type="date"
                    placeholder=""
                    value={date}
                    callBack={(e) => handleChangeDate(e)}
                    required
                  />
                </CCol>
                <CCol>
                  <Input
                    id="start"
                    label="Time Start"
                    type="time"
                    placeholder=""
                    value={timeStart}
                    callBack={(e) => handleChangeTimeStart(e)}
                    required
                  />
                </CCol>
                <CCol>
                  <Input
                    id="end"
                    label="Time End"
                    type="time"
                    placeholder=""
                    value={timeEnd}
                    callBack={(e) => handleChangeTimeEnd(e)}
                    required
                  />
                </CCol>
                <CCol>
                  <Input
                    id="mentor"
                    label="Mentor"
                    type="select"
                    placeholder="Select Mentor"
                    value={mentorId}
                    callBack={(e) => handleChangeMentorId(e)}
                    opsi={mentor}
                    required
                  />
                </CCol>
              </CRow>
              <Input
                id="topic"
                label="Topic"
                type="text-area"
                placeholder="Describe the topic"
                value={topic}
                callBack={(e) => handleChangeTopic(e)}
                required
              />
              <div className="text-center ">
                <CButton
                  color="warning"
                  className=" mx-2 btn-md"
                  style={{ width: "130px" }}
                  onClick={() => setMod(!mod)}
                >
                  <Modal
                    pesan="Are you sure to Submit?"
                    input={false}
                    callBack={handleSubmit}
                    close={() => setMod(false)}
                    visible={mod}
                  />
                  Submit
                </CButton>
              </div>
            </CCol>
            <CCol md={1}></CCol>
          </CRow>

          <h3 className="text-center my-5">Appointment Books</h3>
          <div className="table-responsive">
            <table class="table table-striped ">
              <thead className="text-center">
                <tr>
                  <th scope="col">Date Booked</th>
                  <th scope="col">Time</th>
                  <th scope="col">Propose Mentor</th>
                  <th scope="col">Topic</th>
                  <th scope="col">Status</th>
                  <th scope="col">AMA Comment</th>
                </tr>
              </thead>
              <tbody>
                {!appointment
                  ? ""
                  : appointment.map(function (item, i) {
                      return (
                        <tr>
                          <td>
                            {Moment(item.bookingDate).format("DD MMM YYYY")}
                          </td>
                          <td>
                            {Moment(item.timeStart).zone(0).format("HH:mm")}-
                            {Moment(item.timeEnd).zone(0).format("HH:mm")}
                          </td>
                          <td>{item.mentor ? item.mentor.fullName : ""}</td>
                          <td>{item.topic}</td>
                          <td>{item.status}</td>
                          <td>{item.comment}</td>
                        </tr>
                      );
                    })}
              </tbody>
            </table>
          </div>
        </CCard>
      </CContainer>
    </div>
  );
};

export default AppointmentBook;
