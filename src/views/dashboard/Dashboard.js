import React, { useState, useEffect } from "react";
import { useRef } from "react";
// import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
// import { Pie, getElementsAtEvent } from "react-chartjs-2";

import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CContainer,
  CTableRow,
} from "@coreui/react";
import { CChartBar, CChartPie } from "@coreui/react-chartjs";
import { useSelector } from "react-redux";
import WidgetsDropdown from "../widgets/WidgetsDropdown";
import axios from "axios";
import { getDashboard } from "src/api/dashboard";
import { getEvent } from "src/api/event";
import Input from "src/components/Input";
import LoadingData from "src/components/LoadingData";
import { useNavigate } from "react-router-dom";

const Dashboard = () => {
  const token = localStorage.getItem("token_jwt");
  const { auth } = useSelector((state) => state);
  const [isLoadingData, setIsLoadingData] = useState(true);
  const navigate = useNavigate();

  const [sebaran, setSebaran] = useState([]);
  const [sebaran_inovasi, setSebaran_inovasi] = useState([]);
  const [totInovasi, setTotInovasi] = useState();
  const [progInovasi, setProgInovasi] = useState([]);
  const [totPendapatan, setTotPendapatan] = useState();
  const [totPengeluaran, setTotPengeluaran] = useState();
  const [costEffectiveness, setCostEffectiveness] = useState();
  const [top5Progress, setTop5Progress] = useState([]);
  const [listArea, setListArea] = useState([]);

  const [karakteristikEvent, setKarakteristikEvent] = useState([]);
  const [budgetEvent, setBudgetEvent] = useState([]);
  const [revenueEvent, setRevenueEvent] = useState([]);
  const [valueEvent, setValueEvent] = useState({ value: "", text: "" });
  const [valueArea, setValueArea] = useState();
  const [listEvent, setListEvent] = useState([]);
  const [resetValue, setResetValue] = useState("");
  const [eventValue, setEventValue] = useState("");

  const chartRef = useRef();

  const get_dashboard_first = async (token) => {
    try {
      const response = await getEvent(token);

      const arrEvent = [];
      response.data
        .sort((a, b) => (a.id > b.id ? -1 : 1))
        .map(function (item, i) {
          arrEvent.push({ text: item.name, value: item.id });
        });
      setListEvent(arrEvent);

      const idEvent = arrEvent[0].value;
      setValueEvent(arrEvent[0]);
      const response_2 = await axios.get(
        process.env.REACT_APP_URL_BE + "/dashboard/?eventId=" + idEvent,
        {
          headers: {
            "X-Access-Token": token,
          },
        }
      );
      console.log("DASHBOARD", response_2.data);
      const arrArea = [
        {
          value: "",
          text: "All Area",
        },
      ];

      response_2.data.spreadInnovation.map(function (item, i) {
        arrArea.push({ text: item.name, value: item.id });
      });
      setListArea(arrArea);

      console.log("list event", idEvent);
      setTotInovasi(response_2.data.summary.totalInnovation || "0");
      setProgInovasi(response_2.data.summary.progressAverage || "0");
      setTotPendapatan(response_2.data.summary.totalRevenue || "");
      setTotPengeluaran(response_2.data.summary.totalExpense || "");
      setCostEffectiveness(response_2.data.summary.costEffectiveness || "");
      setTop5Progress(response_2.data.top5progress || []);
      setSebaran(response_2.data.spreadInnovation || []);
      setSebaran_inovasi(response_2.data.spreadInnovation || []);

      setKarakteristikEvent(response_2.data.event || []);
      setBudgetEvent(response_2.data.budget || []);
      setRevenueEvent(response_2.data.revenue || []);
    } catch (error) {
      console.log(error);
    }
    setIsLoadingData(false);
  };

  const updateEvent = async () => {
    try {
      setIsLoadingData(true);
      const idEvent = document.getElementById("filter-event").value;
      const idArea = document.getElementById("filter-area").value;
      setEventValue(idEvent);
      setValueArea(idArea);
      // console.log("id event", idEvent);

      const response = await axios.get(
        process.env.REACT_APP_URL_BE +
          "/dashboard/?eventId=" +
          idEvent +
          "&areaId=" +
          idArea,
        {
          headers: {
            "X-Access-Token": token,
          },
        }
      );
      console.log("Dashboard data all", response.data);
      setTotInovasi(response.data.summary.totalInnovation || "0");
      setProgInovasi(response.data.summary.progressAverage || "0");
      setTotPendapatan(response.data.summary.totalRevenue || "");
      setTotPengeluaran(response.data.summary.totalExpense || "");
      setCostEffectiveness(response.data.summary.costEffectiveness || "");
      setTop5Progress(response.data.top5progress || []);
      setSebaran(response.data.spreadInnovation || []);
      setSebaran_inovasi(response.data.spreadInnovation || []);

      setKarakteristikEvent(response.data.event || []);
      setBudgetEvent(response.data.budget || []);
      setRevenueEvent(response.data.revenue || []);
    } catch (error) {
      console.log(eror);
    }
    setIsLoadingData(false);
  };

  const updateArea = () => {
    try {
      const idArea = document.getElementById("filter-area").value;

      if (idArea) {
        setSebaran_inovasi(sebaran.filter((c) => c.id == idArea));
        console.log("sebaran inovasi", sebaran);
      } else {
        setSebaran_inovasi(sebaran);
      }
    } catch (error) {
      console.log(eror);
    }
  };
  const handleChange = () => {
    const a = [];
    setResetValue(a);
  };

  // const onClickss = (event) => {
  //   console.log(getElementsAtEvent(chartRef.current, event)[0].index + 1);
  //   console.log("event id", eventValue);
  //   const areaId = getElementsAtEvent(chartRef.current, event)[0].index + 1;

  //   const state = { eventValue, areaId };
  //   navigate("/innovation/list", {
  //     state: { state },
  //   });
  // };

  useEffect(() => {
    get_dashboard_first(token);
  }, []);

  return (
    <CContainer>
      {auth.access.includes("filterFunction") ? (
        <CRow>
          <CCol md={3}>
            <Input
              type="select"
              id="filter-event"
              placeholder="Filter by Event"
              value={valueEvent.value}
              opsi={!listEvent ? [] : listEvent}
              callBack={() => {
                updateEvent();
                handleChange();
              }}
            />
          </CCol>
          <CCol md={3}>
            <Input
              type="select"
              id="filter-area"
              placeholder="Filter by Area"
              value={resetValue}
              opsi={!listArea ? [] : listArea}
              callBack={updateEvent}
            />
          </CCol>
        </CRow>
      ) : (
        <h3 className="mb-4">{valueEvent.text}</h3>
      )}

      {isLoadingData ? (
        <LoadingData />
      ) : (
        <>
          {auth.access.includes("performanceSummary") ? (
            <WidgetsDropdown
              totInovasi={totInovasi}
              progInovasi={progInovasi.toLocaleString("en-US").substring(0, 5)}
              totPengeluaran={totPengeluaran}
              totPendapatan={totPendapatan}
              costEffectiveness={costEffectiveness}
            />
          ) : (
            ""
          )}

          {auth.access.includes("ringkasanEvent") ? (
            <div className="my-4">
              <h4>Ringkasan Event</h4>
              <div className="table-responsive">
                <table class="table table-bordered border-primary">
                  <thead className="text-center">
                    <tr>
                      <th scope="col" colspan={2}></th>
                      {karakteristikEvent.map(function (item, i) {
                        return <th scope="col">{item.name}</th>;
                      })}
                    </tr>
                  </thead>
                  <tbody class="text-center">
                    <tr>
                      <td rowspan={3} className="align-middle">
                        Karakteristik Event
                      </td>

                      <td style={{ textAlign: "left" }}># Peserta</td>
                      {karakteristikEvent.map(function (item, i) {
                        return <td>{item.totalPeserta}</td>;
                      })}
                    </tr>
                    <tr>
                      <td style={{ textAlign: "left" }}># Tim</td>
                      {karakteristikEvent.map(function (item, i) {
                        return <td>{item.totalTim}</td>;
                      })}
                    </tr>
                    <tr>
                      <td style={{ textAlign: "left" }}># Ide</td>
                      {karakteristikEvent.map(function (item, i) {
                        return <td>{item.totalIde}</td>;
                      })}
                    </tr>
                    <tr className="bg-prpl">
                      <td colspan={7}></td>
                    </tr>

                    <tr>
                      <td rowspan={3} className="align-middle">
                        Performa Biaya Inovasi
                      </td>
                      <td style={{ textAlign: "left" }}>
                        Total Estimasi Pengeluaran
                      </td>
                      {budgetEvent.map(function (item, i) {
                        return (
                          <td>
                            IDR{" "}
                            {parseFloat(item.estimasi).toLocaleString("en-US")}
                          </td>
                        );
                      })}
                    </tr>
                    <tr>
                      <td style={{ textAlign: "left" }}>Total Pengeluaran</td>
                      {budgetEvent.map(function (item, i) {
                        return (
                          <td>
                            IDR {parseFloat(item.total).toLocaleString("en-US")}
                          </td>
                        );
                      })}
                    </tr>
                    <tr>
                      <td style={{ textAlign: "left" }}>
                        % Realisasi Pengeluaran
                      </td>
                      {budgetEvent.map(function (item, i) {
                        var num = item.realisasi * 100;
                        return (
                          <td>
                            {num.toLocaleString("en-US").substring(0, 5)} %
                          </td>
                        );
                      })}
                    </tr>
                    <tr className="bg-prpl">
                      <td colspan={7}></td>
                    </tr>
                    <tr>
                      <td rowspan={3} className="align-middle">
                        Performa Pendapatan Inovasi
                      </td>
                      <td style={{ textAlign: "left" }}>
                        Total Estimasi Pendapatan
                      </td>
                      {revenueEvent.map(function (item, i) {
                        return (
                          <td>
                            IDR{" "}
                            {parseFloat(item.estimasi).toLocaleString("en-US")}
                          </td>
                        );
                      })}
                    </tr>
                    <tr>
                      <td style={{ textAlign: "left" }}>Total Pendapatan</td>
                      {revenueEvent.map(function (item, i) {
                        return (
                          <td>
                            IDR {parseFloat(item.total).toLocaleString("en-US")}
                          </td>
                        );
                      })}
                    </tr>
                    <tr>
                      <td style={{ textAlign: "left" }}>
                        % Realisasi Pendapatan
                      </td>
                      {revenueEvent.map(function (item, i) {
                        var num = item.realisasi * 100;
                        return (
                          <td>
                            {num.toLocaleString("en-US").substring(0, 5)} %
                          </td>
                        );
                      })}
                    </tr>
                    <tr className="bg-prpl">
                      <td colspan={7}></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          ) : (
            ""
          )}

          <CRow>
            {auth.access.includes("expenseTrend") ? (
              <CCol sm={12} lg={6}>
                <CCard className="mb-4 ">
                  <CCardHeader className="bg-prpl">Expenses Trend</CCardHeader>
                  <CCardBody>
                    <CChartBar
                      data={{
                        labels: budgetEvent.map(function (item) {
                          return item.phase;
                        }),
                        datasets: [
                          {
                            label: "Total Estimasi Pengeluaran",
                            backgroundColor: "#7979f8",
                            data: budgetEvent.map(function (item) {
                              return item.estimasi / 1000000;
                            }),
                          },
                          {
                            label: "Total Pengeluaran",
                            backgroundColor: "#f87979",
                            data: budgetEvent.map(function (item) {
                              return item.total / 1000000;
                            }),
                          },
                        ],
                      }}
                      labels="Fase"
                    />
                    <font className="text-mini">(dalam juta rupiah)</font>
                  </CCardBody>
                </CCard>
              </CCol>
            ) : (
              ""
            )}

            {auth.access.includes("revenueTrend") ? (
              <CCol sm={12} lg={6}>
                <CCard className="mb-4">
                  <CCardHeader className="bg-prpl">Revenue Trend </CCardHeader>
                  <CCardBody>
                    <CChartBar
                      data={{
                        labels: revenueEvent.map(function (item) {
                          return item.name;
                        }),
                        datasets: [
                          {
                            label: "Total Estimasi Pendapatan",
                            backgroundColor: "#7979f8",
                            data: revenueEvent.map(function (item) {
                              return item.estimasi / 1000000;
                            }),
                          },
                          {
                            label: "Total Pendapatan",
                            backgroundColor: "#f87979",
                            data: revenueEvent.map(function (item) {
                              return item.total / 1000000;
                            }),
                          },
                        ],
                      }}
                      labels="Fase"
                    />
                    <font className="text-mini">(dalam juta rupiah)</font>
                  </CCardBody>
                </CCard>
              </CCol>
            ) : (
              ""
            )}

            {auth.access.includes("spreadOfInnovation") ? (
              <CCol sm={12} lg={6}>
                <CCard className="mb-4">
                  <CCardHeader className="bg-prpl">
                    Spread of Innovation
                  </CCardHeader>
                  <CCardBody>
                    <CChartPie
                      data={{
                        labels: sebaran_inovasi.map(function (item) {
                          return item.name;
                        }),
                        datasets: [
                          {
                            data: sebaran_inovasi.map(function (item) {
                              return item.totalInnovation;
                            }),
                            backgroundColor: [
                              "#FF6384",
                              "#36A2EB",
                              "#FFCE56",
                              "#8463FF",
                              "#52489C",
                              "#00798C",
                              "#E63946",
                            ],
                            hoverBackgroundColor: ["#222"],
                          },
                        ],
                      }}
                    />
                  </CCardBody>
                </CCard>
              </CCol>
            ) : (
              ""
            )}

            {auth.access.includes("top5Progress") ? (
              <CCol sm={12} lg={6}>
                <CCard className="mb-4">
                  <CCardHeader className="bg-prpl">Top 5 Progress</CCardHeader>
                  <div className="p-3">
                    <CTable
                      hover
                      className="table-bordered border-primary text-center"
                    >
                      <CTableHead>
                        <CTableRow>
                          <CTableHeaderCell scope="col">No</CTableHeaderCell>
                          <CTableHeaderCell scope="col">
                            Team Name
                          </CTableHeaderCell>
                          <CTableHeaderCell scope="col">
                            % Progress
                          </CTableHeaderCell>
                        </CTableRow>
                      </CTableHead>
                      <CTableBody>
                        {top5Progress
                          .sort((a, b) => b.avgprogress - a.avgprogress)
                          .slice(0, 5)
                          .map(function (item, i) {
                            return (
                              <CTableRow>
                                <CTableHeaderCell scope="row">
                                  {i + 1}
                                </CTableHeaderCell>
                                <CTableDataCell>{item.title}</CTableDataCell>
                                <CTableDataCell>
                                  {item.avgprogress?.substring(0, 5) || "0.000"}
                                  %
                                </CTableDataCell>
                              </CTableRow>
                            );
                          })}
                      </CTableBody>
                    </CTable>
                  </div>
                </CCard>
              </CCol>
            ) : (
              ""
            )}
          </CRow>
        </>
      )}
    </CContainer>
  );
};

export default Dashboard;
