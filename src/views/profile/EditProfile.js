import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInputGroup,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import axios from "axios";
import { Link, useNavigate, useLocation } from "react-router-dom";
import { signIn } from "../../state-redux/actionCreators";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import {
  getUserMokap,
  updateUserMokap,
  getAreas,
  getRoles,
} from "src/api/user";
import ModalAlert from "src/components/ModalAlert";
import LoadingContent from "src/components/LoadingContent";

const EditProfile = () => {
  const token = localStorage.getItem("token_jwt");
  const location = useLocation();
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [isShowPass, setIsShowPass] = useState(false);
  const [role, setRole] = useState("");
  const [area, setArea] = useState("");
  const [errorPass, setErrorPass] = useState();
  const [textSubmit, setTextSubmit] = useState("Submit");
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });
  const [listRole, setListRole] = useState([]);
  const [listArea, setListArea] = useState([]);
  const [image, setImage] = useState("");
  const [newImage, setNewImage] = useState({});
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const AC = bindActionCreators({ signIn }, dispatch);

  const handleChangeEmail = (event) => {
    setEmail(event.target.value);
  };

  const handleChangeName = (event) => {
    setName(event.target.value);
  };

  const handleChangePassword = (e) => {
    const pesan = [];
    if (e.target.value.length < 8) {
      pesan.push("at least 8 characters");
    }
    if (e.target.value == e.target.value.toUpperCase()) {
      pesan.push("lowercase");
    }
    if (e.target.value == e.target.value.toLowerCase()) {
      pesan.push("uppercase");
    }
    if (!/\d/.test(e.target.value)) {
      pesan.push("number");
    }

    setErrorPass(pesan.join(", "));
    setPassword(e.target.value);
  };

  const handleChangeRole = (event) => {
    setRole(event.target.value);
  };

  const handleChangeArea = (event) => {
    setArea(event.target.value);
  };

  const handleChangeImage = (event) => {
    setNewImage(event.target.files[0]);
    console.log("gbr", event.target.files[0]);
    setImage(URL.createObjectURL(event.target.files[0]));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!email || !name || !role || !area || errorPass) {
      setModal({
        visible: true,
        message: "Semua form harus terisi dengan benar",
        status: "warning",
      });
      return;
    }
    setIsLoading(true);
    setTextSubmit("Loading...");
    const passwordConfirmation = password;

    try {
      const response = await updateUserMokap(
        token,
        id,
        email,
        name,
        role,
        area,
        password,
        passwordConfirmation,
        newImage
      );
      console.log("Update User:", response.data.message);

      AC.signIn({
        username: username,
        id: id,
        foto: newImage,
        role: role,
        access: ["all"],
      });

      // setModal({
      //   visible: true,
      //   message: (
      //     <CButton
      //       color="success"
      //       onClick={() => navigate("/profile")}
      //       className="text-white"
      //       style={{ width: "100px" }}
      //     >
      //       Ok
      //     </CButton>
      //   ),
      //   status: "success",
      // });
      setIsLoading(false);
      if (response.data.message) {
        setTextSubmit("Submit");
        navigate("/profile", {
          state: { visible: true },
        });
      }
    } catch (err) {
      alert("Akun salah");
      setIsLoading(false);
    }
  };

  const getRole = async (token) => {
    try {
      const response = await getRoles(token);

      // console.log("getRole", response.data);

      const arrRole = [];
      response.data.map(function (item, i) {
        arrRole.push({ text: item.name, value: item.id });
      });
      setListRole(arrRole);
    } catch (error) {
      console.log(error);
    }
  };

  const getArea = async (token) => {
    try {
      const response = await getAreas(token);

      // console.log("getArea", response.data);

      const arrArea = [];
      response.data.map(function (item, i) {
        arrArea.push({ text: item.name, value: item.id });
      });
      setListArea(arrArea);
    } catch (error) {
      console.log(error);
    }
  };
  const getUser = async (token) => {
    try {
      const response = await getUserMokap(token);

      if (response.data) {
        // console.log("getUserMokap", response.data);
        setId(response.data.id);
        setUsername(response.data.username);
        setName(response.data.fullName);
        setEmail(response.data.email);
        setRole(response.data.roleId);
        setArea(response.data.areaId);
        setImage(response.data.avatar);
        setPassword();
        document.getElementById("password").value = "";
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getArea(token);
    getRole(token);
    if (location.data == undefined) {
      getUser(token);
    }
  }, []);

  useEffect(() => {
    setUsername(auth.username);
  }, [auth.username]);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row align-items-center">
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6} sm={12}>
            <CCard className="">
              <CCardBody className="p-4 shadow">
                <CForm>
                  <label
                    for="image"
                    class="btn avatar avatar-lg img-center mb-5 p-0 bg-secondary border-0"
                    style={{ width: "150px", height: "150px" }}
                  >
                    {!image ? (
                      ""
                    ) : (
                      <img
                        class="avatar-img"
                        style={{
                          width: "150px",
                          height: "150px",
                          objectFit: "cover",
                        }}
                        src={image}
                      ></img>
                    )}
                  </label>
                  <input
                    class="form-control"
                    type="file"
                    id="image"
                    onChange={handleChangeImage}
                    hidden
                  />
                  <CInputGroup className="mb-3">
                    <CInputGroupText className="inputgrouptext">
                      Username
                    </CInputGroupText>
                    <input
                      class="form-control"
                      placeholder="Username"
                      value={username}
                      onChange={handleChangeName}
                      disabled
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText className="inputgrouptext">
                      Name
                    </CInputGroupText>
                    <input
                      class={`form-control ${
                        name == "" ? "border-danger " : ""
                      }`}
                      placeholder="Name"
                      value={name}
                      onChange={handleChangeName}
                    />
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText className="inputgrouptext">
                      Role
                    </CInputGroupText>
                    <select
                      size="sm"
                      className={`form-control ${
                        role == "" ? "border-danger " : ""
                      }`}
                      aria-label="Small select example"
                      value={role}
                      onChange={handleChangeRole}
                    >
                      <option value="">Pilih Role Anda</option>
                      {!listRole
                        ? ""
                        : listRole.map(function (item, i) {
                            return (
                              <option value={item.value}>{item.text}</option>
                            );
                          })}
                    </select>
                  </CInputGroup>
                  <CInputGroup className="mb-3">
                    <CInputGroupText className="inputgrouptext">
                      Email
                    </CInputGroupText>
                    <input
                      class={`form-control ${
                        email == "" ? "border-danger " : ""
                      }`}
                      placeholder="Email"
                      value={email}
                      onChange={handleChangeEmail}
                    />
                  </CInputGroup>

                  <CInputGroup className="mb-3">
                    <CInputGroupText className="inputgrouptext">
                      Password
                    </CInputGroupText>
                    <input
                      class={`form-control `}
                      placeholder="New Password"
                      type={isShowPass ? "text" : "password"}
                      value={password}
                      autocomplete="off"
                      id="password"
                      onChange={handleChangePassword}
                    />
                    <span
                      className="clickable input-group-text bg-light border-0 "
                      style={{ borderRadius: "0px 10px 10px 0px" }}
                      onClick={() => setIsShowPass(!isShowPass)}
                    >
                      <i
                        className={`${
                          isShowPass ? "bi-eye-fill" : "bi-eye-slash-fill"
                        }`}
                      ></i>
                    </span>
                  </CInputGroup>
                  {!errorPass ? (
                    ""
                  ) : (
                    <div
                      className="text-mini text-danger text-center"
                      style={{
                        marginBottom: "5px",
                        marginTop: "-20px",
                      }}
                    >
                      {errorPass}
                    </div>
                  )}
                  <CInputGroup className="mb-3">
                    <CInputGroupText className="inputgrouptext">
                      Area
                    </CInputGroupText>
                    <select
                      size="sm"
                      className={`form-control ${
                        area == "" ? "border-danger " : ""
                      }`}
                      aria-label="Small select example"
                      value={area}
                      onChange={handleChangeArea}
                    >
                      <option>Pilih Area Anda</option>
                      {!listArea
                        ? ""
                        : listArea.map(function (item, i) {
                            return (
                              <option value={item.value}>{item.text}</option>
                            );
                          })}
                    </select>
                  </CInputGroup>

                  <div className="d-grid">
                    <CButton
                      color="secondary"
                      onClick={handleSubmit}
                      className="text-white mt-4"
                    >
                      {textSubmit}
                    </CButton>
                  </div>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default EditProfile;
