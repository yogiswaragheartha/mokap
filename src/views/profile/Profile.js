import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInputGroup,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilPencil } from "@coreui/icons";
import axios from "axios";
import { useNavigate, Link, useLocation } from "react-router-dom";

import { signOut } from "../../state-redux/actionCreators";
import { bindActionCreators } from "redux";
import { useDispatch, useSelector } from "react-redux";
import { getUserMokap, getAreaById, getRoleById } from "src/api/user";
import ModalAlert from "src/components/ModalAlert";
import { Modal } from "@coreui/coreui";

const Register = () => {
  const token = localStorage.getItem("token_jwt");
  const location = useLocation();
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [name, setName] = useState("");
  const [role, setRole] = useState("");
  const [area, setArea] = useState("");
  const [image, setImage] = useState("");
  const navigate = useNavigate();
  const [modal, setModal] = useState({
    visible: location.state
      ? location.state.visible == true
        ? true
        : false
      : false,
    message: "",
    status: "success",
  });

  // const { auth } = useSelector((state) => state);
  const dispatch = useDispatch();
  const AC = bindActionCreators({ signOut }, dispatch);

  const getUser = async (token) => {
    try {
      const response = await getUserMokap(token);

      // console.log("getUser", response.data);
      setUsername(response.data.username);
      setName(response.data.fullName);
      setEmail(response.data.email);
      setImage(response.data.avatar);
      setRole(response.data.Role.name);
      setArea(response.data.Area.name);

      // getRoleId(token, response.data.roleId);
      // getAreaId(token, response.data.areaId);
    } catch (error) {
      console.log(error);
    }
  };

  // const getAreaId = async (token, id) => {
  //   try {
  //     const response = await getAreaById(token, id);

  //     setArea(response.data.name);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // const getRoleId = async (token, id) => {
  //   try {
  //     const response = await getRoleById(token, id);

  //     setRole(response.data.name);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  const logout = () => {
    localStorage.clear();
    AC.signOut();
    navigate("/login");
  };

  useEffect(() => {
    getUser(token);
  }, []);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row align-items-center">
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={9} lg={7} xl={6} sm={12}>
            <CCard className="">
              <CCardBody className="p-4 shadow">
                <div style={{ width: "99%", textAlign: "right" }}>
                  <Link to="/profile/edit" className="text-decoration-none">
                    <CIcon icon={cilPencil} size="xl" />
                  </Link>
                </div>
                <div>
                  <label
                    for="image"
                    class="avatar avatar-lg img-center mb-5  bg-secondary"
                    style={{ width: "150px", height: "150px" }}
                  >
                    {!image ? (
                      ""
                    ) : (
                      <img
                        class="avatar-img"
                        style={{
                          width: "150px",
                          height: "150px",
                          objectFit: "cover",
                        }}
                        src={image}
                      ></img>
                    )}
                  </label>
                  <table>
                    <tr className="tabel-custom">
                      <td className="inputgrouptext">Username</td>
                      <td>:</td>
                      <td>{username}</td>
                    </tr>
                    <tr className="tabel-custom">
                      <td className="inputgrouptext">Name</td>
                      <td>:</td>
                      <td> {name}</td>
                    </tr>
                    <tr className="tabel-custom">
                      <td className="inputgrouptext">Role</td>
                      <td>:</td>
                      <td> {role}</td>
                    </tr>
                    <tr className="tabel-custom">
                      <td className="inputgrouptext">Email</td>
                      <td>:</td> <td>{email}</td>
                    </tr>
                    {/* <tr className="tabel-custom">
                      <td className="inputgrouptext">Password</td>
                      <td>:</td>
                      <td>**************</td>
                    </tr> */}
                    {/* <tr className="tabel-custom">
                      <td className="inputgrouptext">Phone</td>
                      <td>:</td> <td>{phone}</td>
                    </tr> */}

                    <tr className="tabel-custom">
                      <td className="inputgrouptext">Area</td>
                      <td>:</td>
                      <td> {area}</td>
                    </tr>
                    {/* <tr className="tabel-custom">
                      <td className="inputgrouptext">Team</td>
                      <td>:</td>
                      <td>{team}</td>
                    </tr> */}
                  </table>
                  <div className="d-grid">
                    <CButton
                      color="secondary"
                      className="text-white bg-danger border-0 mt-4"
                      onClick={logout}
                    >
                      Logout
                    </CButton>
                  </div>
                </div>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Register;
