import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CRow,
} from "@coreui/react";
import { login } from "src/api/user";
import logo from "src/assets/images/logo.png";
import login_bg from "src/assets/images/login_bg.png";
import Input from "src/components/Input";
import ModalAlert from "src/components/ModalAlert";
import LoadingContent from "src/components/LoadingContent";

const Login = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [textButton, setTextButton] = useState("Sign in");
  const [isLoading, setIsLoading] = useState(false);
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });

  const url_button = `https://git.digitalamoeba.id/oauth/authorize?client_id=${process.env.REACT_APP_CLIENT_ID}&redirect_uri=${process.env.REACT_APP_URL_SERVER}/authorize&response_type=code&state=loginMokap&scope=read_user`;

  const signInGit = () => {
    document.getElementById("signButton").innerHTML = "Loading..";
    window.location.href = url_button;
  };

  const handleChangeEmail = (e) => {
    setEmail(e.target.value);
  };

  const handleChangePassword = (e) => {
    setPassword(e.target.value);
  };

  const signIn = () => {
    setIsLoading(true);
    setTextButton("Loading..");
    // window.location.href = `${process.env.REACT_APP_URL_SERVER}/authorize`;
    loginAction();
  };

  const loginAction = async () => {
    const data = { username: email, password };
    try {
      const response = await login(data);
      console.log(response.data);

      if (response.data.status == "error") {
        setIsLoading(false);
        setModal({
          visible: true,
          message: response.data.message,
          status: "warning",
        });
      } else {
        setIsLoading(false);
        setModal({
          visible: true,
          message: "",
          status: "success",
        });
        localStorage.setItem("token_jwt", response.data.accessToken || "");
        navigate("/");
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
    setTextButton("Sign in");
  };

  return (
    <div
      className="bg-light min-vh-100 d-flex flex-row align-items-center img-fluid"
      style={{ backgroundImage: `url(${login_bg})` }}
    >
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />

      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8} lg={6}>
            <CCardGroup style={{ width: "100%" }}>
              <CCard
                className="p-4 border-0 "
                style={{
                  borderRadius: "30px",
                  height: "570px",
                  backgroundColor: "rgba(256, 256, 256, 0.8)",
                }}
              >
                <CCardBody>
                  <img
                    src={logo}
                    style={{ width: "120px" }}
                    className="img-center img-fluid"
                  />

                  <h1 className="text-center">
                    Say hi to <b>Mokap</b>
                    <i>!</i>
                  </h1>
                  <p className="text-center text-medium-emphasis fw-light text-break">
                    More than just an innovation management tool. <br />
                    <b>It's legit awesome.</b>
                  </p>
                  <CForm>
                    <CRow>
                      <CCol
                        className="mb-4 mx-auto "
                        xs={12}
                        lg={8}
                        style={{ marginTop: "-10px" }}
                      >
                        <Input
                          id="email"
                          label=""
                          type="email"
                          placeholder="Email or Username"
                          callBack={(e) => handleChangeEmail(e)}
                          style={{
                            height: "45px",
                            textAlign: "center",
                            borderRadius: "10px",
                            border: "0px",
                          }}
                        />
                        <Input
                          id="password"
                          label=""
                          type="password"
                          placeholder={"Password"}
                          callBack={(e) => handleChangePassword(e)}
                          style={{
                            height: "45px",
                            textAlign: "center",
                            borderRadius: "10px 0px 0px 10px",
                            border: "0px",
                          }}
                        />

                        <CButton
                          color="primary"
                          className="w-100 mt-4"
                          onClick={signIn}
                          id="signUser"
                        >
                          {textButton}
                        </CButton>
                      </CCol>
                    </CRow>

                    <CRow>
                      <CCol xs={12} lg={8} className="mx-auto">
                        <CRow>
                          <CCol className="py-2">
                            <div className="border-bottom border-dark opacity-50"></div>
                          </CCol>
                          <CCol xs={2}>
                            <p className="text-center">Or</p>
                          </CCol>
                          <CCol className="py-2">
                            <div className="border-bottom border-dark opacity-50"></div>
                          </CCol>
                        </CRow>

                        <CButton
                          className="px-4 w-100"
                          style={{
                            backgroundColor: "#00590F",
                            border: "1px solid #00590F",
                          }}
                          onClick={signInGit}
                          id="signButton"
                        >
                          Sign in with Amoeba Git
                        </CButton>
                        <p className="text-center mt-1">
                          Don’t have an account?{" "}
                          <Link to="/register" className="text-decoration-none">
                            <>Register Here</>
                          </Link>
                        </p>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
