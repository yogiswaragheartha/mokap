import React, { useEffect, useState } from "react";
import { Link, useSearchParams, useNavigate } from "react-router-dom";
import { CContainer, CProgress, CProgressBar } from "@coreui/react";
import axios from "axios";
import { getTokenGit, getUserGit, isRegistered } from "src/api/user";

const Authorize = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const code = searchParams.get("code");
  const navigate = useNavigate();
  const [bar, setBar] = useState({ pesan: "Authorize..", value: 0 });

  const getAccessToken = async () => {
    setTimeout(() => setBar({ pesan: "Get git token..", value: 40 }), 500);
    try {
      const response = await getTokenGit(code);
      console.log(response.data.access_token);
      isFirstLogin(response.data.access_token);
    } catch (error) {
      console.log(error);
    }
  };

  // const getUser = async (access_token) => {
  //   try {
  //     const response = await getUserGit(access_token);

  //     console.log("GetUser", response.data);
  //     isFirstLogin(response.data.username);
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  const isFirstLogin = async (token) => {
    setTimeout(() => {
      setBar({ pesan: "Get Mokap data..", value: 75 });
    }, 500);
    try {
      const response = await isRegistered(token);

      console.log("isRegistered", response.data);
      localStorage.setItem("token_jwt", response.data.accessToken || "");
      setTimeout(() => setBar({ pesan: "Complete!", value: 100 }), 500);
      // localStorage.setItem("id", response.data.id || "");
      setTimeout(() => {
        if (response.data.registered == true) {
          navigate("/");
        } else {
          navigate("/profile/edit", {
            state: { data: response.data, newuser: true },
          });
        }
      }, 1000);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    setTimeout(() => {
      setBar({ pesan: "Get access to the Amoeba Git..", value: 25 });
    }, 500);
    setTimeout(() => {
      getAccessToken();
    }, 500);
  }, []);

  return (
    <div className=" " style={{ marginTop: "150px" }}>
      <CContainer sm>
        <h4>{bar.pesan}</h4>
        <CProgress className="mb-3">
          <CProgressBar
            color="primary"
            variant="striped"
            animated
            value={bar.value}
          />
        </CProgress>
      </CContainer>
    </div>
  );
};

export default Authorize;
