import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CRow,
} from "@coreui/react";
import { getAreas, getRoles, login, signUp } from "src/api/user";
import logo from "src/assets/images/logo.png";
import login_bg from "src/assets/images/login_bg.png";
import Input from "src/components/Input";
import ModalAlert from "src/components/ModalAlert";
import LoadingContent from "src/components/LoadingContent";

const Register = () => {
  const navigate = useNavigate();
  const [listRole, setListRole] = useState([]);
  const [listArea, setListArea] = useState([]);
  const [fullName, setFullName] = useState();
  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [roleId, setRoleId] = useState();
  const [areaId, setAreaId] = useState();
  const [password, setPassword] = useState();
  const [errorPass, setErrorPass] = useState();
  const [passwordConfirmation, setConfirmPassword] = useState();
  const [cekConfirmPass, setCekConfirmPass] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [modal, setModal] = useState({
    visible: false,
    message: "",
    status: "",
  });

  const getRole = async () => {
    try {
      const response = await getRoles();

      const arrRole = [];
      response.data.map(function (item, i) {
        arrRole.push({ text: item.name, value: item.id });
      });
      setListRole(arrRole);
    } catch (error) {
      console.log(error);
    }
  };

  const getArea = async () => {
    try {
      const response = await getAreas();

      const arrArea = [];
      response.data.map(function (item, i) {
        arrArea.push({ text: item.name, value: item.id });
      });
      setListArea(arrArea);
    } catch (error) {
      console.log(error);
    }
  };

  const handleRegister = () => {
    const data = {
      username,
      fullName,
      email,
      roleId,
      areaId,
      password,
      passwordConfirmation,
    };
    if (
      !username ||
      !fullName ||
      !email ||
      !roleId ||
      !areaId ||
      !password ||
      errorPass ||
      !cekConfirmPass
    ) {
      setModal({
        visible: true,
        message: "Please fill in the form correctly",
        status: "warning",
      });

      return;
    }
    console.log("KONTEN REGISTER", data);
    createUser(data);
  };

  const createUser = async (data) => {
    setIsLoading(true);
    try {
      const response = await signUp(data);
      console.log(response.data);

      if (response.data.registered) {
        loginAction(username, password);
      } else {
        setModal({
          visible: true,
          message: response.data.message,
          status: "warning",
        });
      }
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const loginAction = async (username, password) => {
    setIsLoading(true);
    const data = { username, password };
    try {
      const response = await login(data);

      setIsLoading(false);

      localStorage.setItem("token_jwt", response.data.accessToken || "");
      navigate("/");
    } catch (error) {
      console.log(error);
    }
    setIsLoading(false);
  };

  const handleChangeUsername = (e) => {
    setUsername(e.target.value);
  };

  const handleChangeFullName = (e) => {
    setFullName(e.target.value);
  };

  const handleChangeEmail = (e) => {
    setEmail(e.target.value);
  };

  const handleChangeRole = (e) => {
    setRoleId(e.target.value);
  };

  const handleChangeArea = (e) => {
    setAreaId(e.target.value);
  };

  const handleChangePassword = (e) => {
    const pesan = [];
    if (e.target.value.length < 8) {
      pesan.push("at least 8 characters");
    }
    if (e.target.value == e.target.value.toUpperCase()) {
      pesan.push("lowercase");
    }
    if (e.target.value == e.target.value.toLowerCase()) {
      pesan.push("uppercase");
    }
    if (!/\d/.test(e.target.value)) {
      pesan.push("number");
    }

    setErrorPass(pesan.join(", "));
    setPassword(e.target.value);
    if (passwordConfirmation == e.target.value) {
      setCekConfirmPass(true);
    } else {
      setCekConfirmPass(false);
    }
  };

  const handleChangeConfirmPassword = (e) => {
    setConfirmPassword(e.target.value);

    if (e.target.value != password) {
      setCekConfirmPass(false);
    } else {
      setCekConfirmPass(true);
    }
  };

  var styleForm = {
    height: "37px",
    textAlign: "center",
    borderRadius: "10px",
    border: "0px",
    marginTop: "-5px",
  };

  useEffect(() => {
    getArea();
    getRole();
  }, []);

  return (
    <div
      className="bg-light min-vh-100 d-flex flex-row align-items-center img-fluid"
      style={{ backgroundImage: `url(${login_bg})` }}
    >
      <ModalAlert
        visible={modal.visible}
        status={modal.status}
        message={modal.message}
        callBack={() => setModal({ visible: false, message: "", status: "" })}
      />
      <ModalAlert
        visible={isLoading}
        status="loading"
        message={<LoadingContent />}
      />
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={8} lg={6}>
            <CCardGroup style={{ width: "100%" }}>
              <CCard
                className="p-4 border-0 "
                style={{
                  borderRadius: "30px",
                  height: "570px",
                  backgroundColor: "rgba(256, 256, 256, 0.8)",
                }}
              >
                <CCardBody>
                  <img
                    src={logo}
                    style={{ width: "120px" }}
                    className="img-center img-fluid"
                  />
                  <h6 className="text-center">Please Complete Your Data</h6>

                  <CForm>
                    <CRow>
                      <CCol
                        className="mb-4 mx-auto "
                        xs={12}
                        lg={8}
                        style={{ marginTop: "-10px" }}
                      >
                        <Input
                          id="username"
                          label=""
                          type="text"
                          placeholder="Username"
                          callBack={(e) => handleChangeUsername(e)}
                          style={styleForm}
                        />
                        <Input
                          id="name"
                          label=""
                          type="text"
                          placeholder="Full Name"
                          callBack={(e) => handleChangeFullName(e)}
                          style={styleForm}
                        />
                        <Input
                          id="email"
                          label=""
                          type="email"
                          placeholder={"Email"}
                          callBack={(e) => handleChangeEmail(e)}
                          style={styleForm}
                        />

                        <Input
                          id="role"
                          label=""
                          type="select"
                          placeholder="Select Role"
                          value=""
                          opsi={listRole}
                          callBack={(e) => handleChangeRole(e)}
                          style={styleForm}
                        />

                        <Input
                          id="area"
                          label=""
                          type="select"
                          placeholder="Select Area"
                          value=""
                          opsi={listArea}
                          callBack={(e) => handleChangeArea(e)}
                          style={styleForm}
                        />
                        <div style={{ marginTop: "-10px" }}>
                          <Input
                            id="password"
                            label=""
                            type="password"
                            placeholder={"Password"}
                            callBack={(e) => handleChangePassword(e)}
                            style={{
                              height: "37px",
                              textAlign: "center",
                              borderRadius: "10px 0px 0px 10px",
                              border: "0px",
                            }}
                          />

                          {!errorPass ? (
                            ""
                          ) : (
                            <div
                              className="text-mini text-danger text-center"
                              style={{
                                marginBottom: "-18px",
                                marginTop: "21px",
                              }}
                            >
                              {errorPass}
                            </div>
                          )}
                        </div>
                        <div style={{ marginTop: "-5px" }}>
                          <Input
                            id="confirm-password"
                            label=""
                            type="password"
                            placeholder={"Confirm Password"}
                            callBack={(e) => handleChangeConfirmPassword(e)}
                            style={{
                              height: "37px",
                              textAlign: "center",
                              borderRadius: "10px 0px 0px 10px",
                              border: "0px",
                            }}
                          />

                          {cekConfirmPass == true ? (
                            ""
                          ) : (
                            <div
                              className="text-mini text-danger text-center"
                              style={{
                                marginBottom: "-18px",
                                marginTop: "21px",
                              }}
                            >
                              Password not match
                            </div>
                          )}
                        </div>

                        <CButton
                          color="primary"
                          className="w-100 mt-4"
                          onClick={handleRegister}
                          id="signButton"
                        >
                          Sign-up
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Register;
