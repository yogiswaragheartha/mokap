import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CCardImage,
  CCardText,
  CCardTitle,
  CForm,
  CInputGroup,
  CRow,
} from "@coreui/react";
import axios from "axios";
import { useNavigate, Link, useParams } from "react-router-dom";
import { getSearch } from "src/api/dashboard";
import LoadingData from "src/components/LoadingData";

const Search = (props) => {
  const [inovasi, setInovasi] = useState([]);
  const [event, setEvent] = useState([]);
  const [isLoadingData, SetIsLoadingData] = useState(true);
  const navigate = useNavigate();
  const { search } = useParams();

  const get_search = async (search) => {
    try {
      const response = await getSearch(search);

      console.log("Search", response.data);
      setInovasi(response.data.innovation || []);
      setEvent(response.data.events || []);
    } catch (error) {
      console.log(error);
    }
    SetIsLoadingData(false);
  };

  useEffect(() => {
    get_search(search);
  }, []);

  useEffect(() => {
    get_search(search);
  }, [search]);

  return (
    <div className="bg-light d-flex mt-4 mb-5 flex-row ">
      <CContainer>
        <CRow className="">
          <h3>Search Result for "{search}"</h3>
          {isLoadingData ? (
            <LoadingData />
          ) : (
            <CCol md={12} lg={8}>
              {event.map(function (item) {
                return (
                  <CCard className="mt-2">
                    <CCardBody
                      className="clickable p-4 shadow"
                      onClick={() => {
                        navigate("/event/detail/" + item.id);
                      }}
                    >
                      <CRow>
                        <CCol md={3}>
                          <img
                            orientation="left"
                            src={item.banner}
                            style={{ width: "100%" }}
                          />
                        </CCol>
                        <CCol xs={12} md={9}>
                          <CCardTitle className="text-secondary">
                            {item.name}
                          </CCardTitle>
                          <CCardText>
                            <p className="text-mini ">{item.description}</p>
                          </CCardText>
                        </CCol>
                      </CRow>
                    </CCardBody>
                  </CCard>
                );
              })}
              {inovasi.map(function (item) {
                return (
                  <CCard className="mt-2">
                    <CCardBody
                      className="clickable p-4 shadow"
                      onClick={() => {
                        navigate("/innovation/detail/" + item.id);
                      }}
                    >
                      <CRow>
                        <CCol md={3}>
                          <img
                            orientation="left"
                            src={item.logo}
                            style={{ width: "100%" }}
                          />
                        </CCol>
                        <CCol xs={12} md={9}>
                          <CCardTitle className="text-secondary">
                            {item.title}
                          </CCardTitle>
                          <CCardText>
                            <p className="text-mini ">{item.description}</p>
                          </CCardText>
                        </CCol>
                      </CRow>
                    </CCardBody>
                  </CCard>
                );
              })}
            </CCol>
          )}
        </CRow>
      </CContainer>
    </div>
  );
};

export default Search;
