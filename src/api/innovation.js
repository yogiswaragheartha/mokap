import axios from "axios";
export const getInnovation = (token, id) => {
  if (!id) {
    return axios.get("https://mokap-be-staging.herokuapp.com/innovations/", {
      headers: {
        "X-Access-Token": token,
      },
    });
  } else {
    return axios.get(
      "https://mokap-be-staging.herokuapp.com/innovations/" + id,
      {
        headers: {
          "X-Access-Token": token,
        },
      }
    );
  }
};
