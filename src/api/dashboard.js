import axios from "axios";

export const getOverviewFase = () => {
  return axios.get("https://yeshub.net/api/dashboard/overview_fase");
};

export const getSearch = (search) => {
  return axios.get(
    process.env.REACT_APP_URL_BE + "/search/?keyword=" + search,
    {
      // headers: {
      //   "X-Access-Token": token,
      // },
    }
  );
};

export const getNotification = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/notifications", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getHome = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/homepage", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getDashboard = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/dashboard", {
    headers: {
      "X-Access-Token": token,
    },
  });
};
