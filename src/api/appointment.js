import axios from "axios";

export const getAppointment = (token, id) => {
  if (!id) {
    return axios.get(process.env.REACT_APP_URL_BE + "/appointments/", {
      headers: {
        "X-Access-Token": token,
      },
    });
  } else {
    return axios.get(process.env.REACT_APP_URL_BE + "/appointments/" + id, {
      headers: {
        "X-Access-Token": token,
      },
    });
  }
};

export const getMyAppointment = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/appointments/user", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const createAppointment = (token, data) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "application/json",
  };

  return axios.post(process.env.REACT_APP_URL_BE + "/appointments/", data, {
    headers: header,
  });
};

export const updateApproval = (token, data, id) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "application/json",
  };

  return axios.put(
    process.env.REACT_APP_URL_BE + "/appointments/approval/" + id,
    data,
    {
      headers: header,
    }
  );
};
