import axios from "axios";

// export const getEvent = (id) => {
//   if (!id) {
//     return axios.get("https://yeshub.net/api/event/get_event");
//   } else {
//     return axios.get("https://yeshub.net/api/event/get_event?id=" + id);
//   }
// };

export const getEvent = (token, id) => {
  if (!id) {
    return axios.get(process.env.REACT_APP_URL_BE + "/events/", {
      headers: {
        "X-Access-Token": token,
      },
    });
  } else {
    return axios.get(process.env.REACT_APP_URL_BE + "/events/" + id, {
      headers: {
        "X-Access-Token": token,
      },
    });
  }
};

export const createEvent = (token, data) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.post(
    process.env.REACT_APP_URL_BE + "/events/",

    data,

    {
      headers: header,
    }
  );
};

export const updateEvent = (token, data, id) => {
  const header = {
    "x-access-token": token,
    // "Content-Type": "application/json",
    "Content-Type": "multipart/form-data",
  };

  return axios.put(
    process.env.REACT_APP_URL_BE + "/events/" + id,

    data,
    // {
    //   "phases[0][name]": "COBA HARDCODE",
    // },

    {
      headers: header,
    }
  );
};

export const getPhase = (id) => {
  if (!id) {
    return axios.get("https://yeshub.net/api/event/get_phase");
  } else {
    return axios.get("https://yeshub.net/api/event/get_phase?id=" + id);
  }
};

export const getAppointment = (id) => {
  if (!id) {
    return axios.get("https://yeshub.net/api/appointment/get_appointment");
  } else {
    return axios.get(
      "https://yeshub.net/api/appointment/get_appointment?id=" + id
    );
  }
};

// export const getAnnouncement = (id) => {
//   if (!id) {
//     return axios.get("https://yeshub.net/api/event/get_announcement");
//   } else {
//     return axios.get("https://yeshub.net/api/event/get_announcement?id=" + id);
//   }
// };

export const getAnnouncement = (token, id) => {
  if (!id) {
    return axios.get(process.env.REACT_APP_URL_BE + "/announcements/", {
      headers: {
        "X-Access-Token": token,
      },
    });
  } else {
    return axios.get(process.env.REACT_APP_URL_BE + "/announcements/" + id, {
      headers: {
        "X-Access-Token": token,
      },
    });
  }
};

export const deleteAnnouncement = (token, id) => {
  return axios.delete(process.env.REACT_APP_URL_BE + "/announcements/" + id, {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const createAnnouncement = (token, data) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.post(
    process.env.REACT_APP_URL_BE + "/announcements/",

    data,

    {
      headers: header,
    }
  );
};

export const updateAnnouncement = (token, data, id) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.put(
    process.env.REACT_APP_URL_BE + "/announcements/" + id,

    data,

    {
      headers: header,
    }
  );
};
