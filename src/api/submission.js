import axios from "axios";

export const getInnovation = (token, id) => {
  if (!id) {
    return axios.get(process.env.REACT_APP_URL_BE + "/innovations/", {
      headers: {
        "X-Access-Token": token,
      },
    });
  } else {
    return axios.get(process.env.REACT_APP_URL_BE + "/innovations/" + id, {
      headers: {
        "X-Access-Token": token,
      },
    });
  }
};

export const getInnovationByID = (token, id) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/submissions/" + id, {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getSubmissionRecap = (token, innovationId, phaseId) => {
  return axios.get(
    process.env.REACT_APP_URL_BE +
      "/submissions/recap/" +
      innovationId +
      "?phase=" +
      phaseId,
    {
      headers: {
        "X-Access-Token": token,
      },
    }
  );
};

export const getMyListInnovation = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/innovations/user", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getMyInnovation = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/submissions/", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getAllSubmission = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/submissions/getAll", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getArea = (token, id) => {
  if (!id) {
    return axios.get(process.env.REACT_APP_URL_BE + "/areas/", {
      headers: {
        "X-Access-Token": token,
      },
    });
  } else {
    return axios.get(process.env.REACT_APP_URL_BE + "/areas/" + id, {
      headers: {
        "X-Access-Token": token,
      },
    });
  }
};

export const createProposal = (token, data) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.post(
    process.env.REACT_APP_URL_BE + "/submissions/proposal",

    data,

    {
      headers: header,
    }
  );
};

export const createReport = (token, data) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.post(
    process.env.REACT_APP_URL_BE + "/submissions/report",
    data,
    {
      headers: header,
    }
  );
};

export const updatePhase = (token, data, id) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "application/json",
  };

  return axios.put(
    process.env.REACT_APP_URL_BE + "/submissions/approval/" + id,
    data,
    {
      headers: header,
    }
  );
};

export const updateProposal = (token, data, id) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.put(
    process.env.REACT_APP_URL_BE + "/submissions/proposal/" + id,

    data,

    {
      headers: header,
    }
  );
};

export const updateProgress = (token, data, id) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.put(
    process.env.REACT_APP_URL_BE + "/submissions/progress/" + id,

    data,

    {
      headers: header,
    }
  );
};

export const approvalSubmission = (token, data) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "application/json",
  };

  return axios.post(
    process.env.REACT_APP_URL_BE + "/submissions/approval",

    data,

    {
      headers: header,
    }
  );
};

// export const updateEvent = (token, data, id) => {
//   const header = {
//     "x-access-token": token,
//     "Content-Type": "multipart/form-data",
//   };

//   return axios.put(
//     "https://mokap-be-staging.herokuapp.com/events/" + id,

//     data,

//     {
//       headers: header,
//     }
//   );
// };
