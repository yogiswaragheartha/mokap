import axios from "axios";

export const getTokenGit = (code) => {
  return axios.post("https://git.digitalamoeba.id/oauth/token", {
    client_id: process.env.REACT_APP_CLIENT_ID,
    client_secret: process.env.REACT_APP_CLIENT_SECRET,
    code,
    grant_type: "authorization_code",
    redirect_uri: process.env.REACT_APP_URL_SERVER + "/authorize",
    // code_verifier: randomstring.generate(128),
  });
};

export const getUserGit = (access_token) => {
  return axios.get(
    "https://git.digitalamoeba.id/api/v4/user?access_token=" + access_token
  );
};

export const login = (data) => {
  const header = {
    "Content-Type": "application/json",
  };

  return axios.post(process.env.REACT_APP_URL_BE + "/auth/login", data, {
    headers: header,
  });
};

export const signUp = (data) => {
  const header = {
    "Content-Type": "application/json",
  };

  return axios.post(process.env.REACT_APP_URL_BE + "/auth/signup", data, {
    headers: header,
  });
};

// export const isRegistered = (access_token) => {
//   //get user git, validasi registrasi, lalu output data user & status isregistered
//   return axios.get(
//     "https://yeshub.net/api/profile/is_registered?access_token=" + access_token
//   );
// };

export const isRegistered = (access_token) => {
  return axios.post(process.env.REACT_APP_URL_BE + "/auth/signin", {
    access_token,
  });
};

// export const getUserMokap = (token) => {
//   return axios.get("https://yeshub.net/api/profile/get_user?token=" + token);
// };

export const getAreas = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/areas/", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getAreaById = (token, id) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/areas/" + id, {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getRoles = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/roles/", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const createRole = (token, data) => {
  const header = {
    "X-Access-Token": token,
    "Content-Type": "application/json",
  };

  return axios.post(process.env.REACT_APP_URL_BE + "/roles", data, {
    headers: header,
  });
};

export const editRole = (token, id, data) => {
  const header = {
    "X-Access-Token": token,
    "Content-Type": "application/json",
  };
  return axios.put(process.env.REACT_APP_URL_BE + "/roles/" + id, data, {
    headers: header,
  });
};

export const getRoleById = (token, id) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/roles/" + id, {
    headers: {
      "X-Access-Token": token,
    },
  });
};

// export const getUserMokap = (token) => {
//   return axios.get("https://yeshub.net/api/profile/get_user?token=" + token);
// };
export const getUserMokap = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/users/", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

export const getAllUser = (token) => {
  return axios.get(process.env.REACT_APP_URL_BE + "/users/getall", {
    headers: {
      "X-Access-Token": token,
    },
  });
};

// export const updateUserMokap = (
//   email,
//   password,
//   name,
//   phone,
//   role,
//   area,
//   team,
//   username,
//   image
// ) => {
//   return axios.post("https://yeshub.net/api/profile/add_user", {
//     email,
//     password,
//     name,
//     phone,
//     role,
//     area,
//     team,
//     username,
//     image,
//   });
// };

export const updateUserMokap = (
  token,
  id,
  email,
  name,
  role,
  area,
  password,
  passwordConfirmation,
  image
) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.put(
    process.env.REACT_APP_URL_BE + "/users/" + id,
    {
      roleId: role,
      email,
      fullName: name,
      areaId: area,
      password,
      passwordConfirmation,
      avatar: image,
    },

    {
      headers: header,
    }
  );
};

export const updateUserData = (token, id, role, area) => {
  const header = {
    "x-access-token": token,
    "Content-Type": "multipart/form-data",
  };

  return axios.put(
    process.env.REACT_APP_URL_BE + "/users/" + id,
    {
      roleId: role,
      areaId: area,
    },

    {
      headers: header,
    }
  );
};
